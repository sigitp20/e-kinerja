-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 22, 2020 at 06:31 AM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 5.6.40-7+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sirsak_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `hospital_survey_indicator_variable`
--

CREATE TABLE `hospital_survey_indicator_variable` (
  `variable_id` int(11) NOT NULL,
  `variable_indicator_id` int(11) NOT NULL,
  `variable_name` text NOT NULL,
  `variable_type` char(1) NOT NULL COMMENT 'N=Numerator;D=Denumerator',
  `variable_unit_name` char(10) NOT NULL,
  `variable_record_status` char(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hospital_survey_indicator_variable`
--

INSERT INTO `hospital_survey_indicator_variable` (`variable_id`, `variable_indicator_id`, `variable_name`, `variable_type`, `variable_unit_name`, `variable_record_status`) VALUES
(1, 1, 'Jumlah asesmen  lengkap yang dilakukan oleh tenaga medis dalam waktu 24 jam setelah pasien masuk  rawat inap dalam waktu 1 bulan', 'N', 'Orang', 'A'),
(2, 1, 'Jumlah total pasien yang masuk rawat inap dalam waktu 24 jam, dalam waktu 1 bulan', 'D', 'Orang', 'A'),
(3, 2, 'Jumlah status pasien yang tidak ada dokumentasi  asesmen nyeri awal maupun ulang sesuai panduan manajemen nyeri per bulan', 'N', 'orang', 'A'),
(4, 2, 'Jumlah seluruh  pasien dengan keluhan nyeri dalam bulan tersebut', 'D', 'Orang', 'A'),
(5, 3, 'Jumlah kerusakan sample darah per bulan', 'N', 'sample', 'A'),
(6, 3, 'Jumlah sample darah pada bulan tersebut', 'D', 'sampel', 'A'),
(7, 4, 'Jumlah jenis pemeriksaan dengan hasil kritis yang tidak terlaporkan per bulan', 'N', 'jenis', 'A'),
(8, 4, 'Jumlah jenis pemeriksaan dengan hasil kritis pada bulan tersebut', 'D', 'jenis', 'A'),
(9, 5, 'Jumlah penolakan expertise per bulan', 'N', 'pelayanan', 'A'),
(10, 5, 'Jumlah seluruh pelayanan di radiologi pada bulan tersebut ', 'D', 'pelayanan', 'A'),
(11, 6, 'Jumlah pasien rawat jalan yang menerima hasil foto beserta bacaan lebih dari 3 jam per bulan', 'N', 'pasien', 'A'),
(12, 6, 'Jumlah seluruh pasien rawat jalan yang dilakukan tindakan foto rontgen pada bulan tersebut ', 'D', 'pasien', 'A'),
(13, 7, 'Jumlah pemeriksaan ulang radiologi per bulan ', 'N', 'pasien', 'A'),
(14, 7, 'Jumlah pasien yang dilakukan pemeriksaan radiologi pada bulan tersebut', 'D', 'pasien', 'A'),
(15, 8, 'Jumlah tindakan radiologi yang tidak dilakukan asesmen awal radiologi per bulan', 'N', 'tindakan', 'A'),
(16, 8, 'Jumlah seluruh tindakan radiologi dalam bulan tersebut', 'D', 'tindakan', 'A'),
(17, 9, 'Jumlah kejadian tidak dilakukannya penandaan lokasi operasi pada semua kasus operasi yang harus dilakukan penandaan lokasi operasi per bulan.', 'N', 'kejadian', 'A'),
(18, 9, 'Jumlah semua kasus operasi yang harus dilakukan penandaan lokasi operasi dalam bulan tersebut.', 'D', 'kejadian', 'A'),
(19, 10, 'Jumlah operasi ulang dengan diagnosa sama / komplikasinya per bulan.', 'N', 'operasi', 'A'),
(20, 10, 'Jumlah seluruh operasi dalam bulan tersebut', 'D', 'operasi', 'A'),
(21, 11, 'Jumlah insiden salah prosedur operasi pada pasien', 'N', 'insiden', 'A'),
(22, 11, 'Jumlah insiden salah prosedur operasi dalam bulan tersebut', 'D', 'insiden', 'A'),
(23, 12, 'Jumlah insiden kesalahan lokasi operasi pada pasien pembedahan', 'N', 'insiden', 'A'),
(24, 12, 'Jumlah insiden kesalahan lokasi operasi dalam bulan tersebut', 'D', 'insiden', 'A'),
(25, 13, 'Insiden kesalahan diagnosis pra operasi', 'N', 'kesalahan', 'A'),
(26, 13, 'Insiden kesalahan diagnosis pra operasi dalam bulan tersebut', 'D', 'kesalahan', 'A'),
(27, 14, 'Jumlah kesalahan penyerahan perbekalan per bulan', 'N', 'kesalahan', 'A'),
(28, 14, 'Jumlah permintaan perbekalan dalam bentuk resep dan formulir permintaan pada bulan tersebut', 'D', 'kesalahan', 'A'),
(29, 15, 'Jumlah kesalahan / ketidaktepatan pemberian obat  (5 Benar)', 'N', 'kesalahan', 'A'),
(30, 15, 'Jumlah total kesalahan/ketidaktepatan pemberian obat dalam bulan tersebut', 'D', 'kesalahan', 'A'),
(31, 16, 'Jumlah pasien yang tidak dilakukan asesmen pre anestesi secara lengkap per bulan', 'N', 'pasien', 'A'),
(32, 16, 'Jumlah pasien operasi dengan anestesi dalam bulan tersebut', 'D', 'pasien', 'A'),
(33, 17, 'Jumlah kasus reaksi  transfusi darah per hari', 'N', 'kasus', 'A'),
(34, 17, 'Jumlah total kasus pemasangan transfusi darah (kantong darah) di hari tersebut', 'D', 'kasus', 'A'),
(35, 18, 'Informed consent yang tidak lengkap per bulan', 'N', 'kasus', 'A'),
(36, 18, 'Jumlah tindakan kedokteran dari seluruh pasien dalam bulan tersebut', 'D', 'kasus', 'A'),
(37, 19, 'Jumlah catatan rekam medis yang belum lengkap dan benar dalam 14 hari per bulan', 'N', 'catatan', 'A'),
(38, 19, 'Jumlah catatan rekam medis dalam bulan tersebut', 'D', 'catatan', 'A'),
(39, 20, 'Jumlah pasien yang mengalami infeksi pasca operasi dalam satu bulan', 'N', 'pasien', 'A'),
(40, 20, 'Jumlah seluruh pasien yang dioperasi di rumah sakit dalam bulan tersebut', 'D', 'pasien', 'A'),
(41, 21, 'Jumlah pasien sepsis per bulan', 'N', 'pasien', 'A'),
(42, 21, 'Jumlah pasien rawat inap pada  bulan tersebut', 'D', 'pasien', 'A'),
(43, 22, 'Jumlah kasus infeksi luka infus per bulan', 'N', 'kasus', 'A'),
(44, 22, 'Jumlah kasus pemasangan infus dalam bulan tersebut', 'D', 'kasus', 'A'),
(45, 23, 'Jumlah kasus infeksi aliran darah primer karena pemasangan intravaskular kateter  perbulan', 'N', 'kasus', 'A'),
(46, 23, 'Jumlah kasus pemasangan intravaskular kateter  dalam bulan tersebut', 'D', 'kasus', 'A'),
(47, 24, 'Jumlah kasus infeksi karena pemasangan kateter per bulan', 'N', 'kasus', 'A'),
(48, 24, 'Jumlah hari pemasangan kateter dalam bulan tersebut', 'D', 'hari', 'A'),
(49, 25, 'Jumlah VAP  atau pneumonia yang terjadi akibat pemasangan ventilator per bulan', 'N', 'VAP', 'A'),
(50, 25, 'Jumlah hari pemakaian Endotracheal Tube (ETT) pada bulan tersebut', 'D', 'hari', 'A'),
(51, 26, 'Jumlah kasus luka dekubitus per bulan', 'N', 'pasien', 'A'),
(52, 26, 'Jumlah pasien tirah baring pada bulan tersebut', 'D', 'pasien', 'A'),
(53, 27, 'Jumlah insiden ketidaktepatan identifikasi pasien', 'N', 'insiden', 'A'),
(54, 27, 'Jumlah total insiden ketidaktepatan identifikasi pasien dalam bulan tersebut', 'D', 'insiden', 'A'),
(55, 28, 'Insiden kejadian/kesalahan yang terkait dengan keamanan obat-obatan yang perlu diwaspadai', 'N', 'insiden', 'A'),
(56, 28, 'Jumlah total insiden/kejadian kesalahan yang terkait dengan keamanan obat-obatan yang perlu diwaspadai dalam bulan tersebut', 'D', 'insiden', 'A'),
(57, 29, 'Jumlah insiden pasien jatuh dalam bulan tersebut', 'N', 'insiden', 'A'),
(58, 29, 'Jumlah total insiden pasien jatuh dalam bulan tersebut', 'D', 'insiden', 'A'),
(59, 30, 'Jumlah pasien stroke ischemic yang tidak mendapatkan terapi anti trombotik pada saat KRS perbulan ', 'N', 'pasien', 'A'),
(60, 30, 'Jumlah seluruh pasien stroke ischemic yang KRS dalam bulan tersebut', 'D', 'pasien', 'A'),
(61, 31, 'Jumlah pasien stroke yang tidak mendapatkan edukasi tentang stroke selama menjalani rawat inap selama satu bulan', 'N', 'pasien', 'A'),
(62, 31, 'Jumlah seluruh pasien stroke yang dirawat inap dalam bulan tersebut', 'D', 'pasien', 'A'),
(63, 32, 'Jumlah pasien IMA yang tidak  mendapatkan terapi aspirin/anti trombotik  dalam waktu 24 jam sejak datang ke RS per bulan ', 'N', 'pasien', 'A'),
(64, 32, 'Jumlah seluruh pasien IMA dalam bulan tersebut', 'D', 'pasien', 'A'),
(65, 33, 'Jumlah bayi baru lahir yang tidak mendapat ASI eksklusif selama masa rawat inap dalam satu bulan', 'N', 'pasien', 'A'),
(66, 33, 'Jumlah seluruh bayi baru lahir dalam bulan tersebut', 'D', 'pasien', 'A'),
(67, 34, 'Jumlah pasien asma anak yang  tidak menerima bronkodilator selama masa rawat inap per bulan', 'N', 'pasien', 'A'),
(68, 34, 'Jumlah seluruh pasien asma anak dalam bulan tersebut', 'D', 'pasien', 'A'),
(69, 35, 'Jumlah semua pasien tuberkulosis yang tidak ditangani sesuai dengan strategi DOTS per bulan', 'N', 'pasien', 'A'),
(70, 35, 'Jumlah seluruh pasien tuberculosis yang ditangani dalam bulan tersebut.', 'D', 'pasien', 'A'),
(71, 36, 'Jumlah seluruh pasien terdiagnosa TB terkonfirmasi bakteriologis pada triwulan tersebut.', 'N', 'pasien', 'A'),
(72, 36, 'Jumlah seluruh pasien terduga TB yang melakukan pemeriksaan dahak mikroskopis pada bulan tersebut.', 'D', 'pasien', 'A'),
(73, 37, 'Jumlah seluruh pasien  TB paru terkonfirmasi bakteriologis dengan hasil pemeriksaan BTA akhir tahap awal negatif', 'N', 'pasien', 'A'),
(74, 37, 'Jumlah seluruh  pasien TB paru terkonfirmasi bakteriologis yang diobati.', 'D', 'pasien', 'A'),
(75, 38, 'Jumlah seluruh pasien TB paru terkonfirmasi bakteriologis yang sembuh.', 'N', 'pasien', 'A'),
(76, 38, 'Jumlah seluruh  pasien TB paru terkonfirmasi bakteriologis yang diobati', 'D', 'pasien', 'A'),
(77, 39, 'Jumlah ibu meninggal karena eklampsia per bulan', 'N', 'pasien', 'A'),
(78, 39, 'Jumlah ibu dengan eklampsia pada bulan tersebut', 'D', 'pasien', 'A'),
(79, 40, 'Jumlah ibu melahirkan yang meninggal karena perdarahan perbulan', 'N', 'pasien', 'A'),
(80, 40, 'Jumlah ibu melahirkan dengan perdarahan pada bulan tersebut', 'D', 'pasien', 'A'),
(81, 41, 'Jumlah BBLR 1500 – 2500 gr dengan usia kehamilan &ge; 32 minggu  yang tidak berhasil ditangani  per bulan', 'N', 'BBLR', 'A'),
(82, 41, 'Jumlah seluruh BBLR 1500 – 2500 gr dengan usia kehamilan &ge; 32 minggu yang ditangani dalam bulan tersebut', 'D', 'BBLR', 'A'),
(83, 42, 'Jumlah ibu yang mengalami keterlambatan sectio caesarea pada bulan tersebut', 'N', 'pasien', 'A'),
(84, 42, 'Jumlah ibu yang mengalami sectio caesarea pada bulan tersebut', 'D', 'pasien', 'A'),
(85, 43, 'Jumlah ibu hamil / bersalin / nifas yang mengalami keterlambatan penyediaan darah pada bulan tersebut', 'N', 'pasien', 'A'),
(86, 43, 'Jumlah ibu hamil / bersalin / nifas yang membutuhkan transfusi darah pada bulan tersebut.', 'D', 'pasien', 'A'),
(87, 44, 'Jumlah bayi baru lahir yang tidak dilakukan IMD pada bulan tersebut', 'N', 'pasien', 'A'),
(88, 44, 'Jumlah seluruh bayi baru lahir yang dapat dilakukan IMD pada bulan tersebut', 'D', 'pasien', 'A'),
(89, 45, 'Jumlah pasien rawat ulang per bulan tersebut', 'N', 'pasien', 'A'),
(90, 45, 'Jumlah pasien masuk rumah sakit dalam  bulan tersebut', 'D', 'pasien', 'A'),
(91, 46, 'Jumlah pasien yang kembali ke Instalasi Pelayanan Intensif dengan kasus yang sama dalam waktu &lt; 72 jam pada per bulan', 'N', 'pasien', 'A'),
(92, 46, 'Jumlah seluruh  pasien yang dirawat di Instalasi Pelayanan Intensif dalam bulan tersebut', 'D', 'pasien', 'A'),
(93, 47, 'Jumlah pasien pulang atas permintaan sendiri  per bulan ', 'N', 'pasien', 'A'),
(94, 47, 'Jumlah seluruh pasien yang dirawat dalam bulan tersebut', 'D', 'pasien', 'A'),
(95, 48, 'Jumlah pasien yang mengalami kesalahan tindakan rehabilitasi medis per bulan ', 'N', 'pasien', 'A'),
(96, 48, 'Jumlah seluruh pasien yang diprogram rehabilitasi medis dalam bulan tersebut', 'D', 'pasien', 'A'),
(97, 49, 'Jumlah resume rawat jalan yang tidak terisi dengan lengkap per bulan ', 'N', 'pasien', 'A'),
(98, 49, 'Jumlah kunjungan yang membutuhkan resume rawat jalan dalam bulan tersebut.', 'D', 'pasien', 'A'),
(99, 50, 'Jumlah pasien non diit yang tidak menghabiskan makan siangnya &ge; &frac12; porsi  per bulan ', 'N', 'pasien', 'A'),
(100, 50, 'Jumlah pasien non diit rawat inap yang makan siang dalam bulan tersebut', 'D', 'pasien', 'A'),
(101, 51, 'Jumlah kejadian kesalahan jenis diit makanan pasien per bulan ', 'N', 'insiden', 'A'),
(102, 51, 'Jumlah porsi makanan diit dalam bulan tersebut', 'D', 'kasus', 'A'),
(103, 52, 'Jumlah  ketidaklengkapan laporan operasi per bulan ', 'N', 'kasus', 'A'),
(104, 52, 'Jumlah pasien operasi pada bulan tersebut (sesuai data dari IKO)', 'D', 'kasus', 'A'),
(105, 53, 'Jumlah  ketidaklengkapan laporan anestesi per bulan ', 'N', 'kasus', 'A'),
(106, 53, 'Jumlah total pasien operasi dengan anestesi pada bulan tersebut (sesuai data dari IKO)', 'D', 'kasus', 'A'),
(107, 54, 'Jumlah tertinggalnya instrumen/kasa/benda lain saat operasi per bulan ', 'N', 'kasus', 'A'),
(108, 54, 'Total kejadian tertinggalnya instrumen/kasa/benda lain saat operasi per bulan ', 'D', 'kasus', 'A'),
(109, 55, 'Jumlah pasien yang operasinya tertunda 30 menit per bulan ', 'N', 'pasien', 'A'),
(110, 55, 'Jumlah pasien operasi dalam bulan tersebut ', 'D', 'pasien', 'A'),
(111, 56, 'Jumlah status pasien di ICU dengan keluhan nyeri yang tidak lengkap dokumentasi  asesmen nyerinya selama 1 bulan', 'N', 'pasien', 'A'),
(112, 56, 'Jumlah seluruh pasien yang dirawat di  ICU dengan keluhan nyeri dalam bulan tersebut', 'D', 'pasien', 'A'),
(113, 57, 'Insiden Kesalahan Setting Ventilator ', 'N', 'kasus', 'A'),
(114, 57, 'Jumlah total Insiden Kesalahan Setting Ventilator dalam bulan tersebut', 'D', 'kasus', 'A'),
(115, 58, 'Jumlah Insiden  Vagal Reflek Pada Pemasangan ET', 'N', 'kasus', 'A'),
(116, 58, 'Jumlah total Insiden  Vagal Reflek Pada Pemasangan ET pada bulan tersebut', 'D', 'pasien', 'A'),
(117, 59, 'Jumlah pasien rawat jalan yang menerima obat racikan &ge; 60 menit per bulan', 'N', 'pasien', 'A'),
(118, 59, 'Jumlah pasien rawat jalan yang menerima resep obat racikan dalam bulan tersebut', 'D', 'pasien', 'A'),
(119, 60, 'Jumlah pasien rawat jalan yang menerima obat non racikan > 20 menit ', 'N', 'pasien', 'A'),
(120, 60, 'Jumlah pasien rawat jalan yang menerima obat non racikan dalam bulan tersebut', 'D', 'pasien', 'A'),
(121, 61, 'Jumlah kesalahan penulisan jenis komponen pada label darah per bulan', 'N', 'kasus', 'A'),
(122, 61, 'Jumlah permintaan darah pada bulan tersebut', 'D', 'kasus', 'A'),
(123, 62, 'Jumlah kesalahan pemeriksaan golongan darah per bulan', 'N', 'kasus', 'A'),
(124, 62, 'Jumlah semua pemeriksaan golongan darah dalam bulan tersebut', 'D', 'kasus', 'A'),
(125, 63, 'Jumlah kesalahan jenis komponen darah yang diberikan perbulan', 'N', 'kasus', 'A'),
(126, 63, 'Jumlah permintaan darah pada bulan tersebut', 'D', 'kasus', 'A'),
(127, 64, 'Jumlah seluruh pasien yang drop out per bulan', 'N', 'pasien', 'A'),
(128, 64, 'Jumlah seluruh pasien yang diprogram rehabilitasi medis dalam bulan tersebut', 'D', 'pasien', 'A'),
(129, 65, 'Jumlah keterlambatan waktu tindakan hemodialisa perbulan', 'N', 'kasus', 'A'),
(130, 65, 'Jumlah seluruh pasien hemodialisa dalam bulan tersebut', 'D', 'kasus', 'A'),
(131, 66, 'Jumlah kesalahan setting program hemodialisa perbulan ', 'N', 'kasus', 'A'),
(132, 66, 'Jumlah total kesalahan setting program hemodialisa dalam bulan tersebut', 'D', 'kasus', 'A'),
(133, 67, 'Jumlah ketidaktepatan insersi vena dan arteri perbulan', 'N', 'kasus', 'A'),
(134, 67, 'Jumlah total ketidaktepatan insersi vena dan arteri pada bulan tersebut', 'D', 'kasus', 'A'),
(135, 68, 'Angka kegagalan uji Bowie Dick pada mesin sterilisasi autoclave per bulan', 'N', 'kasus', 'A'),
(136, 68, 'Jumlah seluruh hasil uji Bowie Dick pada mesin sterilisasi autoclave pada bulan tersebut', 'D', 'kasus', 'A'),
(137, 69, 'Jumlah tindakan endoskopi yang tertunda lebih dari 60 menit perbulan', 'N', 'pasien', 'A'),
(138, 69, 'Jumlah pasien yang dilakukan endoskopi dalam bulan tersebut ', 'D', 'pasien', 'A'),
(139, 70, 'Insiden vagal reflex pada tindakan endoskopi perbulan', 'N', 'kasus', 'A'),
(140, 70, 'Jumlah total insiden vagal reflex pada tindakan endoskopi pada bulan tersebut', 'D', 'kasus', 'A'),
(141, 71, 'Insiden ruptur colon pada tindakan kolonoskopi per bulan', 'N', 'kasus', 'A'),
(142, 71, 'Jumlah total insiden ruptur colon pada tindakan kolonoskopi dalam bulan tersebut', 'D', 'kasus', 'A'),
(143, 72, 'Jumlah  kesalahan posisi pasien dalam pemeriksaan radiologi perbulan', 'N', 'kasus', 'A'),
(144, 72, 'Jumlah seluruh pemeriksaan radiologi dalam bulan tersebut', 'D', 'kasus', 'A'),
(145, 73, 'Jumlah kesalahan cetak film  pemeriksaan radiologi perbulan', 'N', 'kasus', 'A'),
(146, 73, 'Jumlah seluruh pemeriksaan radiologi dalam bulan tersebut', 'D', 'kasus', 'A'),
(147, 74, 'Penomeran rekam medis ganda/dobel per bulan', 'N', 'kasus', 'A'),
(148, 74, 'Jumlah pasien MRS dan jumlah pasien rawat jalan dalam bulan tersebut', 'D', 'kasus', 'A'),
(149, 75, 'Kehilangan dokumen rekam medis pasien rawat jalan perbulan', 'N', 'kasus', 'A'),
(150, 75, 'Jumlah pasien rawat jalan dalam bulan teersebut', 'D', 'kasus', 'A'),
(151, 76, 'Jumlah pasien pembedahan di ruang operasi yang telah diisi lengkap checklist keselamatan pasiennya sesuai tahapan oleh petugas tertentu disertai tandatangan dan penulisan jam pengisian', 'N', 'pasien', 'A'),
(152, 76, 'Jumlah pasien pembedahan  di ruang operasi', 'D', 'pasien', 'A'),
(153, 77, 'Jumlah item resep (R/) yang sesuai Fornas ', 'N', 'kasus', 'A'),
(154, 77, 'Jumlah total item resep (R/)', 'D', 'kasus', 'A'),
(155, 78, 'jumlah pasien meninggal di IGD &le; 8 jam sejak datang ', 'N', 'pasien', 'A'),
(156, 78, 'jumlah seluruh pasien yang ditangani di IGD ', 'D', 'pasien', 'A'),
(157, 79, 'KKM + KKK + KKH (%)', 'N', '%', 'A'),
(158, 79, 'Nilai Denominator = 3', 'D', '%', 'A'),
(159, 80, 'Jumlah pasien IGD yang tidak dirawat yang mengalami kesalahan billing resep perbulan', 'N', 'pasien', 'A'),
(160, 80, 'Jumlah seluruh pasien IGD yang tidak dirawat yang dilakukan billing resep dalam bulan tersebut', 'D', 'pasien', 'A'),
(161, 81, 'Jumlah keterlambatan waktu menangani kerusakan alat perbulan', 'N', 'kasus', 'A'),
(162, 81, 'Jumlah seluruh laporan kerusakan alat dalam bulan tersebut', 'D', 'kasus', 'A'),
(163, 82, 'Jumlah kejadian genset menyala dalam waktu &gt; 10 detik pada saat listrik padam perbulan', 'N', 'kasus', 'A'),
(164, 82, 'Jumlah seluruh kejadian pemadaman listrik dalam bulan tersebut', 'D', 'kasus', 'A'),
(165, 83, 'Jumlah linen yang hilang perbulan', 'N', 'kasus', 'A'),
(166, 83, 'Jumlah seluruh linen dalam bulan tersebut', 'D', 'kasus', 'A'),
(167, 84, 'Jumlah ketidaktepatan administrasi keuangan laboratorium perbulan', 'N', 'kasus', 'A'),
(168, 84, 'Jumlah pelayanan administrasi keuangan dalam bulan tersebut', 'D', 'kasus', 'A'),
(169, 85, 'Jumlah ketidaklengkapan dokumen pendukung penagihan perbulan', 'N', 'kasus', 'A'),
(170, 85, 'Jumlah seluruh tagihan atas pelayanan rumah sakit yang terkirim dalm bulan tersebut', 'D', 'kasus', 'A'),
(171, 86, 'Jumlah angket kepuasan yang tidak kembali atau tidak terisi selama satu bulan', 'N', 'kasus', 'A'),
(172, 86, 'Jumlah angket kepuasan yang dibagikan dalam bulan tersebut', 'D', 'kasus', 'A'),
(173, 87, 'Jumlah keterlambatan respon time petugas EDP dalam menanggapi laporan kerusakan hardware / jaringan pada bulan tersebut', 'N', 'kasus', 'A'),
(174, 87, 'Jumlah seluruh laporan kerusakan hardware / jaringan pada bulan tersebut', 'D', 'kasus', 'A'),
(175, 88, 'Ketidaksesuaian surat pesanan (SP) dg fisik barang/ bahan yg diterima per bulan', 'N', 'kasus', 'A'),
(176, 88, 'Jumlah surat pesanan barang yang dikirim dalam bulan tersebut', 'D', 'kasus', 'A'),
(177, 89, 'Jumlah keterlambatan pelayanan ambulans perbulan', 'N', 'kasus', 'A'),
(178, 89, 'Jumlah seluruh permintaan ambulans dalam bulan tersebut', 'D', 'kasus', 'A'),
(179, 90, 'Jumlah proses yang telah dilakukan identifikasi secara benar', 'N', 'proses', 'A'),
(180, 90, 'Jumlah proses pelayanan yang di observasi ', 'D', 'proses', 'A'),
(181, 91, 'Jumlah pasien gawat, darurat, dan gawat-darurat yang mendapatkan pelayanan kegawatdaruratannya dalam waktu ? 5 menit.', 'N', 'pasien', 'A'),
(182, 91, 'Jumlah seluruh pasien pasien gawat, darurat, dan gawat-darurat yang mendapatkan pelayanan kegawatdaruratan di rumah sakit tersebut', 'D', 'pasien', 'A'),
(183, 92, 'Jumlah kumulatif waktu tunggu pasien rawat jalan yang disurvey', 'N', 'pasien', 'A'),
(184, 92, 'Jumlah seluruh pasien rawat jalan yang disurvey', 'D', 'pasien', 'A'),
(185, 93, 'Jumlah pasien yang waktu jadwal operasinya berubah', 'N', 'pasien', 'A'),
(186, 93, 'Jumlah pasien operasi elektif', 'D', 'pasien', 'A'),
(187, 94, 'Jumlah visite dokter spesialis sebelum jam 14:00 pada hari berjalan', 'N', 'visitasi', 'A'),
(188, 94, 'Jumlah visite dokter spesialis pada hari berjalan', 'D', 'visitasi', 'A'),
(189, 95, 'Jumlah pemeriksaan laboratorium kritis yang dilaporkan < 30 menit', 'N', 'pemeriksaa', 'A'),
(190, 95, 'Jumlah seluruh pemeriksaan laboratorium kritis', 'D', 'pemeriksaa', 'A'),
(191, 96, 'Jumlah R/ yang patuh dengan formularium nasional', 'N', 'resep', 'A'),
(192, 96, 'Jumlah seluruh R/', 'D', 'resep', 'A'),
(193, 97, 'Jumlah R/ yang patuh dengan formularium RS', 'N', 'resep', 'A'),
(194, 97, 'Jumlah seluruh R/', 'D', 'resep', 'A'),
(195, 98, 'Total kebersihan tangan yang dilakukan', 'N', 'catatan', 'A'),
(196, 98, 'Peluang kebersihan tangan', 'D', 'catatan', 'A'),
(197, 99, 'Jumlah kasus yang mendapatkan ketiga upaya pencegahan pasien jatuh', 'N', 'kasus', 'A'),
(198, 99, 'Jumlah kasus semua pasien yang berisiko jatuh', 'D', 'kasus', 'A'),
(199, 100, 'Jumlah kasus yang penanganannya patuh dengan kriteria 5 clinical pathways', 'N', 'kasus', 'A'),
(200, 100, 'Jumlah total kasus yang  masuk dalam kriteria 5 clinical pathways yang ditetapkan', 'D', 'kasus', 'A'),
(201, 101, 'Hasil Penilaian IKM.  Jumlah kumulatif hasil penilaian kepuasan dari pasien yang disurvei (dalam prosen)', 'N', 'kasus', 'A'),
(202, 101, 'Skala Maksimal Penilaian IKM.  Jumlah total pasien yang disurvei (n minimal 50)', 'D', 'kasus', 'A'),
(203, 102, 'Jumlah KKM, KKK dan KKH yang sudah ditanggapi dan ditindaklanjuti', 'N', 'komplain', 'A'),
(204, 102, 'Jumlah seluruh KKM, KKK dan KKH', 'D', 'komplain', 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hospital_survey_indicator_variable`
--
ALTER TABLE `hospital_survey_indicator_variable`
  ADD PRIMARY KEY (`variable_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hospital_survey_indicator_variable`
--
ALTER TABLE `hospital_survey_indicator_variable`
  MODIFY `variable_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
