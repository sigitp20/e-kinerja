-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 03, 2020 at 09:08 AM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 5.6.40-7+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sirsak_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `hospital_survey_indicator_variable_for_hospital`
--

CREATE TABLE `hospital_survey_indicator_variable_for_hospital` (
  `variable_id` int(11) NOT NULL,
  `variable_indicator_id` int(11) NOT NULL,
  `variable_name` text NOT NULL,
  `variable_type` char(1) NOT NULL COMMENT 'N=Numerator;D=Denumerator',
  `variable_unit_name` char(10) NOT NULL,
  `variable_record_status` char(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hospital_survey_indicator_variable_for_hospital`
--

INSERT INTO `hospital_survey_indicator_variable_for_hospital` (`variable_id`, `variable_indicator_id`, `variable_name`, `variable_type`, `variable_unit_name`, `variable_record_status`) VALUES
(3, 1, 'Jumlah seluruh sampel yang diperiksa', 'D', 'Sampel', 'A'),
(8, 9, 'semua rekam medik pasien baru yang dirawat 1x24 jam', 'D', 'pasien', 'A'),
(9, 9, 'assasment awal yang diisi lengkap 1x24jam', 'N', 'pasien', 'A'),
(10, 11, 'Capaian Indikator Medik', 'N', 'Rekap Medi', 'A'),
(11, 11, 'Capaian Indikator Medik', 'D', 'Rekap Medi', 'A'),
(12, 13, 'test1', 'N', 'orang', 'A'),
(13, 13, 'test 2', 'D', 'orang', 'A'),
(14, 1, 'Jumlah sampel tumpah selama pemeriksaan', 'N', 'sampel', 'A'),
(15, 7, 'Jumlah waktu tunggu pemeriksaan laboratorium maksimal 120 menit', 'N', 'pasien', 'A'),
(17, 6, 'Jumlah kesalahan penyediaan sampel pasien rawat inap yang akan diperiksa sesuai dengan pemeriksaan', 'N', 'sampel', 'A'),
(18, 6, 'Jumlah seluruh sampel yang masuk ke laboratorium', 'D', 'sampel', 'A'),
(19, 5, 'Jumlah tidak ada kesalahan penyampaian hasil pemeriksaan laboratorium', 'N', 'pemeriksaa', 'A'),
(20, 5, 'Jumlah seluruh hasil pemeriksaan pasien laboratorium', 'D', 'pemeriksaa', 'A'),
(21, 4, 'Jumlah kesalahan pengambilan sampel kepada pasien', 'N', 'sampel', 'A'),
(22, 4, 'Jumlah seluruh pasien yang diambil sampelnya', 'D', 'sampel', 'A'),
(23, 3, 'Angka kejadian alat trouble selama pemeriksaan', 'N', 'pemeriksaa', 'A'),
(24, 3, 'Jumlah seluruh pemeriksaan', 'D', 'pemeriksaa', 'A'),
(25, 2, 'Jumlah seluruh sampling selama satu bulan', 'D', 'pasien', 'A'),
(26, 2, 'Jumlah kejadian tertusuk jarum suntik setelah sampling', 'N', 'pasien', 'A'),
(27, 15, 'Jumlah Ketetapan Waktu Pemberian Makanan Pada Pasien Rawat Inap setiap kali waktu makan', 'N', 'waktu maka', 'A'),
(28, 15, 'Jumlah waktu pemberian makanan pada pasien rawat inap setiap bulan', 'D', 'waktu maka', 'A'),
(29, 16, 'Pasien yang mendapatkan konsultasi gizi di ruang rawat inap', 'N', 'pasien', 'A'),
(30, 16, 'Semua pasien yang ada di ruang rawat inap', 'D', 'pasien', 'A'),
(31, 17, 'Semua pasien rawat jalan yang mendapatkan rujukan ke poli gizi', 'D', 'pasien', 'A'),
(32, 17, 'Pasien yang mendapatkan konsultasi gizi di poli rawat jalan', 'N', 'pasien', 'A'),
(33, 18, 'Semua penulisan pemesanan diet pasien', 'D', 'waktu maka', 'A'),
(34, 18, 'Penulisan pemesanan diet yang tepat', 'N', 'waktu maka', 'A'),
(35, 19, 'Semua karyawan yang terlibat dalam proses penyediaan makanan', 'D', 'orang', 'A'),
(36, 19, 'Karyawan yang memakai APD lengkap sesuai ketentuan instalasi gizi', 'N', 'orang', 'A'),
(37, 20, 'Pasien yang complain mengenai makanan yang disajikan', 'N', 'insiden', 'A'),
(38, 20, 'Semua pasien yang diberikan pelayanan makanan di ruang rawat inap setiap waktu makan', 'D', 'waktu maka', 'A'),
(39, 21, 'Pegawai yang mengalami kecelakaan kerja di penyelenggaraan makanan', 'N', 'insiden', 'A'),
(40, 21, 'Semua pegawai yang bekerja yang  bekerja sesuai shift pada penyelenggaraan makanan dalam 1 bulan', 'D', 'orang', 'A'),
(41, 22, 'Pasien yang terkaji dan dilakukan asuhan gizi', 'N', 'pasien', 'A'),
(42, 22, 'Semua pasien yang yang ada di rawat inap setiap ruangan', 'D', 'pasien', 'A'),
(43, 23, 'Pasien yang diberikan etiket diet lengkap rata-rata perbulan dalam 3 kali makan', 'N', 'pasien', 'A'),
(44, 23, 'Semua pasien rata-rata perbulan yang diberikan makanan pada waktu makan tersebut', 'D', 'pasien', 'A'),
(45, 25, 'Jumlah semua pasien operasi yang form aseassment pra bedah telah diisi oleh operator', 'N', 'pasien', 'A'),
(46, 25, 'Jumlah pasien yang mendapat prosedur bedah di kamar operasi', 'D', 'pasien', 'A'),
(48, 28, 'seluruh perbaikan yang harus di perbaiki', 'D', 'alat', 'A'),
(49, 29, 'jumlah WTP dan tandon', 'D', 'kasus', 'A'),
(50, 29, 'jumlah kejadian kegagalan pasokan sumber air ', 'N', 'kasus', 'A'),
(51, 30, 'jumlah trouble listrik', 'N', 'sumber', 'A'),
(52, 30, 'jumlah sumber listrik genset atau penyulang', 'D', 'sumber', 'A'),
(53, 31, 'Jumlah pasien yang dilakukan monitoring proses pemulihan anestesi dan sedasi dalam secara lengkap di kamar operasi', 'N', 'pasien', 'A'),
(54, 31, 'Jumlah operasi dalam satu bulan ', 'D', 'pasien', 'A'),
(55, 32, 'Jumlah pasien yang dilakukan evaluasi ulang terjadi konversi tindakan dari local/regional ke general di kamar operasi', 'N', 'pasien', 'A'),
(56, 32, 'jumlah pasien yang terjadi konversi tindakan dari local/regional ke general. operasi dalam satu bulan', 'D', 'pasien', 'A'),
(57, 7, 'Jumlah seluruh pemeriksaan laboratorium', 'D', 'pasien', 'A'),
(58, 33, 'Jumlah seluruh pasien operasi yang di visite oleh dokter anastesi di RSUD R.A. Basoeni', 'N', 'pasien', 'A'),
(59, 33, 'Jumlah seluruh pasien operasi yang di visite dan tidak di visite', 'D', 'pasien', 'A'),
(64, 36, 'Jumlah pasien yang pemulihan gangren di instalasi rawat inap dalam satu bulan', 'N', 'pasien', 'A'),
(65, 36, 'Seluruh pasien gangren di rawat inap', 'D', 'pasien', 'A'),
(66, 37, 'Jumlah pasien yang mengalami salah identifikasi', 'N', 'pasien', 'A'),
(67, 37, 'Semua pasien operasi ', 'D', 'pasien', 'A'),
(68, 12, 'Hasil penilaian IKM', 'N', '%', 'A'),
(69, 12, 'Skala maksimal nilai IKM', 'D', '%', 'A'),
(70, 27, 'Jumlah pasien yang mendapatkan terapi ultrasound wound treatmen pada luka ganggraen/sellulitis', 'N', 'pasien', 'A'),
(71, 27, 'Jumlah semua pasien ganggraen/sellulitis yang mendapat prosedur bedah di kamar operasi', 'D', 'pasien', 'A'),
(72, 35, 'Jumlah resep yang tidak terlayani dikarenakan obat kosong', 'N', 'Resep', 'A'),
(73, 35, 'Jumlah seluruh resep dalam bulan tersebut', 'D', 'Resep', 'A'),
(74, 39, 'Semua alat dalam inklusi yang terpasang internal indikator', 'N', 'pasien', 'A'),
(75, 39, 'Semua alat dalam inklusi yang wajib menggunakan\nindikator', 'D', 'pasien', 'A'),
(79, 41, 'Total Semua proses sterilisasi yang dilakukan', 'D', '', 'A'),
(80, 42, 'Kemasan alat yang dalam keadaan baik\n', 'N', '', 'A'),
(81, 42, 'Semua kemasan yang keluaar daari mesin sterilisator\n', 'D', '', 'A'),
(82, 43, 'Semua alat atau bahan yang ext indikatornya berubah warna hitam\n', 'N', '', 'A'),
(83, 43, 'Semua alat atau bahan yang terpasang ext indikator\n', 'D', '', 'A'),
(84, 44, 'jumlah seluruh pasien operasi yang di visite oleh dokter anastesi di RSUD RA BASOENI', 'N', '', 'A'),
(85, 44, 'jumlah seluruh pasien operasi yang di visite dan tidak di visite', 'D', '', 'A'),
(88, 47, 'Frekuensi kerusakan sterilisator per hari\n', 'N', 'hari', 'A'),
(90, 48, 'Jumlahseluruh pemeriksaan USG dalam waktu 1 bulan', 'N', '', 'A'),
(91, 48, 'Target pemeriksaan USG dalam 1 bulan sebanyak 70 pasien', 'D', '', 'A'),
(92, 49, 'Jumlah pasien yang mengalami reaksi alergi akibat dari  pemeriksaan kontras yang dilakukan diradiologi', 'N', '', 'A'),
(93, 49, 'Jumlah pasien yang melakukan pemeriksaan kontras', 'D', '', 'A'),
(94, 50, 'Jumlah hasil foto yang dibaca dan diverifikasi oleh dokter spesialis radiologi dalam waktu 1 bulan', 'N', '', 'A'),
(95, 50, 'Jumlah seluruh pemeriksaan foto  dalam waktu 1 bulan', 'D', '', 'A'),
(96, 54, 'jumlah seluruh pasien yang di tangani IGD', 'D', '', 'A'),
(97, 54, 'jumlah insident false triase di IGD', 'N', '', 'A'),
(98, 55, 'jumlah seluruh dokter yang dinas di IGD', 'D', '', 'A'),
(99, 55, 'jumlah dokter IGD yang memiliki sertifikat pelatihan', 'N', '', 'A'),
(100, 51, 'jumlah ketepatan dosis antibiotik pada pasien', 'N', 'pasien', 'A'),
(101, 51, 'jumlah seluruh pasien yang menggunakan antibiotik', 'D', 'pasien', 'A'),
(102, 10, 'jumlah penggunaan SBAR dalam setiap komunikasi antar petugas', 'N', 'kasus', 'A'),
(103, 10, 'jumlah seluruh tindakan yang harus menggunakan SBAR direkam medik pasien', 'D', 'kasus', 'A'),
(104, 56, 'jumlah hari buka klinik spesialis yang dilayani oleh dokter spesialis dalam waktu satu bulan', 'N', 'hari', 'A'),
(105, 56, 'jumlah seluruh hari buka klinik spesialis dalam satu bulan', 'D', 'hari', 'A'),
(106, 57, 'jumlah hari pelayanan rawat jalan spesialis yang buka sesuai dengan ketentuan dalam satu bulan', 'N', 'hari', 'A'),
(107, 57, 'jumlah seluru hari pelayanan rawat jalan spesialis dalan satu bulan', 'D', 'hari', 'A'),
(108, 58, 'Jumlah pasien yang dilakukan penandaan area operasi dalam 1 bulan', 'N', 'pasien', 'A'),
(109, 58, 'Jumlah pasien yang memerlukan penandaan area operasi', 'D', 'pasien', 'A'),
(110, 59, 'Jumlah pasien MRS dari rawat jalan yang diberikan edukasi pemasangan gelang', 'N', 'pasien', 'A'),
(111, 59, 'Jumlah pasien MRS dari rawat jalan', 'D', 'pasien', 'A'),
(112, 60, 'Jumlah pasien jatuh di area poli fisioterapi dalam 1 bulan', 'N', 'pasien', 'A'),
(113, 60, 'Jumlah pasien yang berkunjung di poli fisioterapi', 'D', 'pasien', 'A'),
(114, 47, 'Jumlah hari dalam satu bulan', 'D', 'hari', 'A'),
(115, 62, 'Jumlah resep pasien bedah yang patuh sesuai Fornas', 'N', 'Resep', 'A'),
(116, 62, 'Jumlah seluruh resep pasien bedah', 'D', 'Resep', 'A'),
(117, 63, 'Jumlah seluruh kesalahan pemberian obat pada pasien bedah', 'N', 'Resep', 'A'),
(118, 63, 'Jumlah seluruh resep pasien bedah', 'D', 'Resep', 'A'),
(119, 64, 'jumlah pasien di ICU yang di tangani dokter spesialis sesuai dengan kasus yang ditangani', 'N', '', 'A'),
(120, 64, 'jumlah seluruh pasien di ICU yang ditangani dokter spesialis', 'D', '', 'A'),
(121, 65, 'jumlah pasien anak dan dewasa yang mendapatkan tindakan life saving selama di rawat di ICU', 'N', '', 'A'),
(122, 65, 'jumlah seluruh pasien anak dan dewasa yang harus mendapatkan tindakan life saving selama di rawat di ICU', 'D', '', 'A'),
(123, 66, 'angka kejadian pasien jatuh selama di ICU', 'N', '', 'A'),
(124, 66, 'jumlah seluruh pasien yang di rawat di ICU', 'D', '', 'A'),
(125, 67, 'Jumlah perawat dengan sertifikat PPGD dan PL ICU', 'N', 'orang', 'A'),
(126, 67, 'Jumlah seluruh perawat yang bertugas di ICU', 'D', 'orang', 'A'),
(129, 69, 'Jumlah Visite dokter spesialis pada hari kerja di ICU per pasien', 'N', '', 'A'),
(130, 69, 'Jumlah seluruh hari rawat inap pada hari kerja per pasien', 'D', '', 'A'),
(131, 41, 'Jumlah sterilisasi yang dilakukan test bowie dick', 'N', '', 'A'),
(132, 28, 'yang sudah diperbaiki atau mendapatkan repson', 'N', 'alat', 'A'),
(133, 40, 'Semua proses pencucian linen infeksius yang sesuai spo', 'N', 'buah', 'A'),
(134, 40, 'Total semua proses pencucian linen infeksius', 'D', 'buah', 'A'),
(135, 70, 'jumlah tenaga dokter spog , dokter umum terlatih (APN)  dan bidan yg memberikan pertolongan persalinan normal', 'N', '', 'A'),
(136, 70, 'jumlahseluruh tenaga yang memberi pertolongan persalinan normal', 'D', '', 'A'),
(137, 71, 'jumlah tenaga dokter dan perawat yang memberi pelayanan di ruang rawat inap yang sesuai dengan ketentuan (min D3)', 'N', '', 'A'),
(138, 71, 'jumlah seluruh tenaga dokter dan perawat yang bertugas di rawat inap ', 'D', '', 'A'),
(139, 72, 'Jumlah tenaga dokter SPOG, dokter spesialis anak, dokter spesialis anaestesi yang memberikan pertolongan persalinan dengan tindakan operasi', 'N', '', 'A'),
(140, 72, 'jumlah seluruh tenaga dokter yang melayani persalinan dengan tindakan operasi', 'D', '', 'A'),
(141, 73, 'Jumlah BBLR 1500g -2500 g yang berhasil di tangani', 'N', '', 'A'),
(142, 73, 'Jumlah seluruh  BBLR 1500g -2500 g yang  di tangani', 'D', '', 'A'),
(143, 74, 'jumlah persalinan dengan secsio cesaria dalam 1 bln ', 'N', '', 'A'),
(144, 74, 'jumlah seluruh persalinan dalam 1 bln', 'D', '', 'A'),
(145, 75, 'Jumlah konseling layanan KB mantap', 'N', '', 'A'),
(146, 75, 'Jumlahpeserta  konseling KB mantap', 'D', '', 'A'),
(147, 76, 'jumlah kumulatif hasil penilaian kepuasan dari pasien yang di survey (%)', 'N', '', 'A'),
(148, 76, 'jumlah total pasien yang di survey(n minimal 50)', 'D', '', 'A'),
(151, 45, '	\njumlah seluruh petugas yang patuh dan tidak patuh memakai APD dalam pertolongan persalinan dan tindakan lain secara adekuat di VK', 'D', '', 'A'),
(152, 45, 'jumlah petugas yang patuh dan tidak patuh memakai APD dalam pertolongan persalinan dan tindakan lain secara adekuat di VK', 'N', '', 'A'),
(153, 77, 'Jumlah Kegagalan Gas Medis', 'N', 'Hari', 'A'),
(154, 77, 'Jumlah Hari Kejadian Kegagalan Gas Medis', 'D', 'Hari', 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hospital_survey_indicator_variable_for_hospital`
--
ALTER TABLE `hospital_survey_indicator_variable_for_hospital`
  ADD PRIMARY KEY (`variable_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hospital_survey_indicator_variable_for_hospital`
--
ALTER TABLE `hospital_survey_indicator_variable_for_hospital`
  MODIFY `variable_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
