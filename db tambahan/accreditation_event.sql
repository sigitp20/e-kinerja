-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 14, 2020 at 08:31 AM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 5.6.40-7+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sirsak_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `accreditation_event`
--

CREATE TABLE `accreditation_event` (
  `event_id` int(11) NOT NULL,
  `event_category_id` int(11) NOT NULL,
  `event_barcode` varchar(7) DEFAULT NULL,
  `event_title` text NOT NULL,
  `event_location_id` int(11) NOT NULL DEFAULT '0',
  `event_province_code` varchar(3) DEFAULT NULL,
  `event_city_id` int(5) DEFAULT NULL,
  `event_address_other` varchar(150) DEFAULT NULL,
  `event_start_date` date NOT NULL,
  `event_end_date` date NOT NULL,
  `event_note` text,
  `event_is_webinar` int(1) DEFAULT NULL,
  `event_price` varchar(20) DEFAULT NULL,
  `event_price_personal` decimal(12,2) DEFAULT NULL,
  `event_is_internal` int(1) DEFAULT NULL,
  `event_is_approved` int(11) DEFAULT NULL,
  `event_approval_date` datetime DEFAULT NULL,
  `event_surveyor_code` varchar(50) DEFAULT NULL,
  `event_surveyor_joborder_date` date DEFAULT NULL,
  `event_counselor_code` varchar(50) DEFAULT NULL,
  `event_counselor_joborder_date` date DEFAULT NULL,
  `event_kars_replyletter_code` varchar(30) DEFAULT NULL,
  `event_kars_replyletter_date` date NOT NULL DEFAULT '0000-00-00',
  `event_billing_pic` varchar(50) DEFAULT NULL,
  `event_billing_pic_phone` varchar(15) DEFAULT NULL,
  `event_billing_hospital_letter_code` varchar(30) DEFAULT NULL,
  `event_billing_hospital_letter_date` date DEFAULT NULL,
  `event_billing_kars_letter_code` varchar(25) DEFAULT NULL,
  `event_billing_kars_letter_date` date DEFAULT NULL,
  `event_billing_kadin_name` varchar(30) DEFAULT NULL,
  `event_billing_price` decimal(12,2) DEFAULT NULL,
  `event_billing_ticket` decimal(12,2) DEFAULT NULL,
  `event_billing_transport` decimal(12,2) DEFAULT NULL,
  `event_billing_close_status` int(1) DEFAULT '0',
  `event_billing_close_date` date DEFAULT NULL,
  `event_billing_code_transaction` varchar(13) DEFAULT NULL,
  `event_accreditation_id_reference` int(11) DEFAULT NULL COMMENT 'Nomor Accreditation_ID yang akan remedial',
  `event_id_reference` int(11) DEFAULT NULL,
  `event_hospital_pic` int(6) DEFAULT NULL,
  `event_reply_letter01_number` varchar(30) DEFAULT NULL,
  `event_reply_letter01_date` date NOT NULL DEFAULT '0000-00-00',
  `event_reply_letter02_number` varchar(30) DEFAULT NULL,
  `event_reply_letter02_date` date NOT NULL DEFAULT '0000-00-00',
  `event_reply_letter03_number` varchar(30) DEFAULT NULL,
  `event_reply_letter03_date` date NOT NULL DEFAULT '0000-00-00',
  `event_reply_letter04_number` varchar(30) DEFAULT NULL,
  `event_reply_letter04_date` date NOT NULL DEFAULT '0000-00-00',
  `event_reply_letter05_number` varchar(30) DEFAULT NULL,
  `event_reply_letter05_date` date NOT NULL DEFAULT '0000-00-00',
  `event_reply_letter06_number` varchar(30) DEFAULT NULL,
  `event_reply_letter06_date` date NOT NULL DEFAULT '0000-00-00',
  `event_reply_letter07_number` varchar(30) DEFAULT NULL,
  `event_reply_letter07_date` date NOT NULL DEFAULT '0000-00-00',
  `event_post_date` datetime NOT NULL,
  `event_user_id` int(11) NOT NULL,
  `event_version` varchar(4) NOT NULL,
  `event_id_kars` varchar(10) NOT NULL,
  `event_record_status` char(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accreditation_event`
--

INSERT INTO `accreditation_event` (`event_id`, `event_category_id`, `event_barcode`, `event_title`, `event_location_id`, `event_province_code`, `event_city_id`, `event_address_other`, `event_start_date`, `event_end_date`, `event_note`, `event_is_webinar`, `event_price`, `event_price_personal`, `event_is_internal`, `event_is_approved`, `event_approval_date`, `event_surveyor_code`, `event_surveyor_joborder_date`, `event_counselor_code`, `event_counselor_joborder_date`, `event_kars_replyletter_code`, `event_kars_replyletter_date`, `event_billing_pic`, `event_billing_pic_phone`, `event_billing_hospital_letter_code`, `event_billing_hospital_letter_date`, `event_billing_kars_letter_code`, `event_billing_kars_letter_date`, `event_billing_kadin_name`, `event_billing_price`, `event_billing_ticket`, `event_billing_transport`, `event_billing_close_status`, `event_billing_close_date`, `event_billing_code_transaction`, `event_accreditation_id_reference`, `event_id_reference`, `event_hospital_pic`, `event_reply_letter01_number`, `event_reply_letter01_date`, `event_reply_letter02_number`, `event_reply_letter02_date`, `event_reply_letter03_number`, `event_reply_letter03_date`, `event_reply_letter04_number`, `event_reply_letter04_date`, `event_reply_letter05_number`, `event_reply_letter05_date`, `event_reply_letter06_number`, `event_reply_letter06_date`, `event_reply_letter07_number`, `event_reply_letter07_date`, `event_post_date`, `event_user_id`, `event_version`, `event_id_kars`, `event_record_status`) VALUES
(1, 4, NULL, 'Kegiatan Survei 2019', 0, NULL, NULL, NULL, '2019-03-04', '2019-03-31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '0000-00-00', NULL, '0000-00-00', NULL, '0000-00-00', NULL, '0000-00-00', NULL, '0000-00-00', NULL, '0000-00-00', '2019-03-04 08:11:33', 1, '2018', '2052', 'A'),
(2, 4, NULL, 'Kegiatan Survei 2019', 0, NULL, NULL, NULL, '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '0000-00-00', NULL, '0000-00-00', NULL, '0000-00-00', NULL, '0000-00-00', NULL, '0000-00-00', NULL, '0000-00-00', NULL, '0000-00-00', '2019-05-07 09:43:40', 20, '2012', '', 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accreditation_event`
--
ALTER TABLE `accreditation_event`
  ADD PRIMARY KEY (`event_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accreditation_event`
--
ALTER TABLE `accreditation_event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
