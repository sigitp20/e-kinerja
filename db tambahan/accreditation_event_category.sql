-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 14, 2020 at 09:25 AM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 5.6.40-7+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sirsak_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `accreditation_event_category`
--

CREATE TABLE `accreditation_event_category` (
  `category_id` int(11) NOT NULL,
  `category_code` varchar(5) DEFAULT NULL,
  `category_name` varchar(50) NOT NULL,
  `category_price` varchar(20) DEFAULT NULL,
  `category_version` year(4) NOT NULL,
  `category_post_date` datetime NOT NULL,
  `category_user_id` int(11) NOT NULL,
  `category_record_status` char(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accreditation_event_category`
--

INSERT INTO `accreditation_event_category` (`category_id`, `category_code`, `category_name`, `category_price`, `category_version`, `category_post_date`, `category_user_id`, `category_record_status`) VALUES
(1, 'WN', 'Workshop KARS Nasional', '43.000.000', 2012, '2013-09-26 20:34:04', 1, 'A'),
(2, 'BA', 'Bimbingan Reguler', '12.250.000', 2012, '2013-09-26 20:34:04', 1, 'A'),
(3, 'SSA', 'Simulasi Survei Reguler', '', 2012, '2013-09-26 20:34:04', 1, 'A'),
(4, 'SA', 'Survei Akreditasi', '', 2012, '2013-09-26 20:34:04', 1, 'A'),
(5, 'SR', 'Remedial (Re-Survei)', '', 2012, '2013-11-13 23:11:52', 1, 'A'),
(6, 'SV', 'Survei Verifikasi', '', 2012, '2013-12-18 16:31:21', 1, 'A'),
(7, 'SK', 'Survei Akreditasi Program Khusus', NULL, 2012, '2015-03-10 16:27:34', 1, 'A'),
(8, 'BK2', 'Bimbingan Program Khusus 2 Hari', NULL, 2012, '2015-06-25 12:57:14', 1, 'A'),
(9, 'BK4', 'Bimbingan Program Khusus 4 Hari', NULL, 2012, '2015-06-25 12:57:11', 1, 'A'),
(10, 'SSK', 'Simulasi Survei Program Khusus', NULL, 2012, '2015-06-25 12:59:54', 1, 'A'),
(11, 'WK', 'Workshop Program Khusus', NULL, 2012, '2015-06-25 13:01:23', 1, 'A'),
(12, 'WKS', 'Workshop Kerjasama', NULL, 2012, '2015-06-25 13:02:07', 1, 'A'),
(13, 'WA', 'Workshop Reguler', NULL, 2012, '2015-06-25 13:02:55', 1, 'A'),
(14, 'SRP', 'Survei Remedial Progsus', NULL, 2012, '2015-12-04 03:25:36', 1, 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accreditation_event_category`
--
ALTER TABLE `accreditation_event_category`
  ADD PRIMARY KEY (`category_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accreditation_event_category`
--
ALTER TABLE `accreditation_event_category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
