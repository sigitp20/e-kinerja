-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 17, 2020 at 07:21 AM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 5.6.40-7+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sirsak_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `accreditation_instrument_chapter`
--

CREATE TABLE `accreditation_instrument_chapter` (
  `chapter_id` int(11) NOT NULL,
  `chapter_class_id` int(5) NOT NULL,
  `chapter_code` varchar(10) NOT NULL,
  `chapter_name` varchar(75) NOT NULL,
  `chapter_maximum_score` int(11) NOT NULL,
  `chapter_type` varchar(15) DEFAULT NULL,
  `chapter_package` int(5) DEFAULT NULL,
  `chapter_progsus` int(2) DEFAULT NULL,
  `chapter_package_order` int(5) DEFAULT NULL,
  `chapter_version` year(4) NOT NULL,
  `chapter_post_date` datetime NOT NULL,
  `chapter_user_id` int(3) NOT NULL,
  `chapter_record_status` char(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accreditation_instrument_chapter`
--

INSERT INTO `accreditation_instrument_chapter` (`chapter_id`, `chapter_class_id`, `chapter_code`, `chapter_name`, `chapter_maximum_score`, `chapter_type`, `chapter_package`, `chapter_progsus`, `chapter_package_order`, `chapter_version`, `chapter_post_date`, `chapter_user_id`, `chapter_record_status`) VALUES
(1, 3, 'SKP', 'Sasaran Keselamatan Pasien (SKP)', 240, 'Dasar', 3, 1, 1, 2012, '2013-08-02 07:00:00', 1, 'A'),
(2, 1, 'HPK', 'Hak Pasien dan Keluarga (HPK)', 1000, 'Dasar', 3, 2, 2, 2012, '2013-08-02 07:01:00', 1, 'A'),
(3, 1, 'PPK', 'Pendidikan Pasien dan Keluarga (PPK)', 280, 'Dasar', 4, NULL, 2, 2012, '2013-08-02 07:02:00', 1, 'A'),
(4, 2, 'PMKP', 'Peningkatan Mutu dan Keselamatan Pasien (PMKP)', 880, 'Dasar', 2, NULL, 1, 2012, '2013-08-02 07:03:00', 1, 'A'),
(5, 4, 'MDGs', 'Millenium Development Goals (MDGs)', 190, 'Lainnya', 4, NULL, 1, 2012, '2013-08-02 07:04:00', 1, 'A'),
(6, 1, 'APK', 'Akses Pelayanan dan Kontinuitas Pelayanan  (APK)', 1050, 'Lainnya', 1, NULL, 1, 2012, '2013-08-02 07:05:00', 1, 'A'),
(7, 1, 'AP', 'Asesmen Pasien  (AP)', 1840, 'Lainnya', 1, NULL, 2, 2012, '2013-08-02 07:05:55', 1, 'A'),
(8, 1, 'PP', 'Pelayanan Pasien  (PP)', 740, 'Lainnya', 1, NULL, 3, 2012, '2013-08-02 07:07:00', 1, 'A'),
(9, 1, 'PAB', 'Pelayanan Anestesi dan Bedah  (PAB)', 510, 'Lainnya', 1, NULL, 4, 2012, '2013-08-02 07:08:00', 1, 'A'),
(10, 1, 'MPO', 'Manajemen Penggunaan Obat  (MPO)', 840, 'Lainnya', 3, NULL, 3, 2012, '2013-08-02 07:09:00', 1, 'A'),
(11, 2, 'MKI', 'Manajemen Komunikasi dan Informasi (MKI)', 1090, 'Lainnya', 4, NULL, 3, 2012, '2013-08-02 07:10:00', 1, 'A'),
(12, 2, 'KPS', 'Kualifikasi dan Pendidikan  Staff  (KPS)', 990, 'Lainnya', 3, 12, 4, 2012, '2013-08-02 07:11:00', 1, 'A'),
(13, 2, 'PPI', 'Pencegahan dan Pengendalian Infeksi (PPI)', 830, 'Lainnya', 2, 13, 2, 2012, '2013-08-02 07:12:00', 1, 'A'),
(14, 2, 'TKP', 'Tata Kelola, Kepemimpinan dan Pengarahan  (TKP)', 980, 'Lainnya', 2, NULL, 3, 2012, '2013-08-02 07:13:21', 1, 'A'),
(15, 2, 'MFK', 'Manajemen Fasilitas dan Keselamatan (MFK)', 910, 'Lainnya', 2, NULL, 4, 2012, '2013-08-02 07:14:11', 1, 'A'),
(16, 0, 'DOK.AKRED', 'Penyusunan Dokumen Akreditasi (DOK AKRED)', 0, 'Bimbingan', 4, NULL, 4, 0000, '2015-06-30 16:37:12', 1, 'A'),
(17, 7, 'SKP', 'Sasaran Keselamatan Pasien (SKP)', 360, NULL, 3, 17, NULL, 2018, '2017-09-23 00:00:00', 1, 'A'),
(18, 5, 'ARK', 'Akses ke Rumah Sakit dan Kontinuitas (ARK)', 1020, NULL, 1, NULL, NULL, 2018, '2017-09-23 00:00:00', 1, 'A'),
(19, 5, 'HPK', 'Hak Pasien dan Keluarga (HPK)', 990, NULL, 3, 19, NULL, 2018, '2017-09-23 00:00:00', 1, 'A'),
(20, 5, 'AP', 'Asesmen Pasien (AP)', 1650, NULL, 1, NULL, NULL, 2018, '2017-09-23 00:00:00', 1, 'A'),
(21, 5, 'PAP', 'Pelayanan Asuhan Pasien (PAP)', 810, NULL, 1, NULL, NULL, 2018, '2017-09-23 00:00:00', 1, 'A'),
(22, 5, 'PAB', 'Pelayanan Anestesi dan Bedah (PAB)', 690, NULL, 1, NULL, NULL, 2018, '2017-09-23 00:00:00', 1, 'A'),
(23, 5, 'PKPO', 'Pelayanan Kefarmasian dan Penggunaan Obat (PKPO)', 800, NULL, 3, NULL, NULL, 2018, '2017-09-23 00:00:00', 1, 'A'),
(24, 5, 'MKE', 'Manajemen Komunikasi dan Edukasi (MKE)', 490, NULL, 4, NULL, NULL, 2018, '2017-09-23 00:00:00', 1, 'A'),
(25, 6, 'PMKP', 'Peningkatan Mutu dan Keselamatan Pasien (PMKP)', 800, NULL, 2, NULL, NULL, 2018, '2017-09-23 00:00:00', 1, 'A'),
(26, 6, 'PPI', 'Pencegahan dan Pengendalian Infeksi (PPI)', 1070, NULL, 2, 26, NULL, 2018, '2017-09-23 00:00:00', 1, 'A'),
(27, 6, 'TKRS', 'Tata Kelola Rumah Sakit (TKRS)', 1290, NULL, 2, NULL, NULL, 2018, '2017-09-23 00:00:00', 1, 'A'),
(28, 6, 'MFK', 'Manajemen Fasilitas dan Keselamatan (MFK)', 1040, NULL, 2, NULL, NULL, 2018, '2017-09-23 00:00:00', 1, 'A'),
(29, 6, 'KKS', 'Kompetensi &  Kewenangan Staf (KKS)', 960, NULL, 3, 29, NULL, 2018, '2017-09-23 00:00:00', 1, 'A'),
(30, 6, 'MIRM', 'Manajemen Informasi dan Rekam Medis  (MIRM) ', 750, NULL, NULL, NULL, NULL, 2018, '2017-09-23 00:00:00', 1, 'A'),
(31, 8, 'PN', 'Program Nasional', 580, NULL, NULL, NULL, NULL, 2018, '2017-09-23 00:00:00', 1, 'A'),
(32, 9, 'IPKP', 'Integrasi Pendidikan Kesehatan  dalam Pelayanan Rumah Sakit (IPKP)', 210, NULL, NULL, NULL, NULL, 2018, '2017-09-23 00:00:00', 1, 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accreditation_instrument_chapter`
--
ALTER TABLE `accreditation_instrument_chapter`
  ADD PRIMARY KEY (`chapter_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accreditation_instrument_chapter`
--
ALTER TABLE `accreditation_instrument_chapter`
  MODIFY `chapter_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
