-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 29, 2020 at 02:25 AM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 5.6.40-7+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sirsak_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `hospital_survey_indicator_for_hospital`
--

CREATE TABLE `hospital_survey_indicator_for_hospital` (
  `indicator_id` int(3) NOT NULL,
  `indicator_definition` text NOT NULL,
  `indicator_criteria_inclusive` text,
  `indicator_criteria_exclusive` text,
  `indicator_element` text NOT NULL,
  `indicator_source_of_data` varchar(100) NOT NULL,
  `indicator_type` varchar(50) NOT NULL,
  `indicator_value_standard` int(3) DEFAULT NULL,
  `indicator_monitoring_area` varchar(200) NOT NULL,
  `indicator_frequency` char(1) NOT NULL,
  `indicator_target` char(5) DEFAULT '0',
  `indicator_category_id` int(2) NOT NULL DEFAULT '2',
  `indicator_iscomplete` int(1) NOT NULL DEFAULT '0',
  `indicator_record_status` char(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hospital_survey_indicator_for_hospital`
--

INSERT INTO `hospital_survey_indicator_for_hospital` (`indicator_id`, `indicator_definition`, `indicator_criteria_inclusive`, `indicator_criteria_exclusive`, `indicator_element`, `indicator_source_of_data`, `indicator_type`, `indicator_value_standard`, `indicator_monitoring_area`, `indicator_frequency`, `indicator_target`, `indicator_category_id`, `indicator_iscomplete`, `indicator_record_status`) VALUES
(1, 'Penilaian angka sampel tumpah selama pemeriksaan', '', '', 'Kejadian Sampel tumpah', 'Laboratorium', 'Input', NULL, 'Laboratorium', 'M', '3%', 1, 0, 'A'),
(2, 'Penilaian angka kejadian tertusuknya jarum suntik setelah sampling', '', '', 'Kejadian tertusuk jarum', 'Laboratorium', 'Input', NULL, 'Laboratorium', 'M', '0%', 1, 0, 'A'),
(3, 'Penilaian angka kejadian alat trouble saat melakukan pemeriksaan', '', '', 'Alat Trouble', 'Laboratorium', 'Input', NULL, 'Laboratorium', 'M', '5%', 1, 0, 'A'),
(4, 'Penilaian terhadap kesalahan dalam pengambilan sampel kepada pasien', '', '', 'Insiden kesalahan pengambilan sampel', 'Laboratorium', 'Input', NULL, 'Laboratorium', 'M', '2%', 1, 0, 'A'),
(5, 'Penilaian persentase kesalahan penyampaian hasil pasien dalam setiap pemeriksaan laboratorium', '', '', 'Tidak ada kesalahan penyampaian hasil pemeriksaan laboratorium', 'Laboratorium', 'Input', NULL, 'Laboratorium', 'M', '90%', 1, 0, 'A'),
(6, 'Jumlah kesalahan penyediaan sampel yang akan diperiksa, sesuai dengan pemeriksaan yang akan dilakukan', '', '', 'Angka kesalahan penyediaan sampel', 'Laboratorium', 'Input', NULL, 'Laboratorium', 'M', '2%', 1, 0, 'A'),
(7, 'Penilaian waktu tunggu pemeriksaan', '', '', 'Waktu tunggu pemeriksaan', 'Laboratorium', 'Input', NULL, 'Laboratorium', 'M', '5%', 1, 0, 'A'),
(8, 'Yang dimaksud assesmen medis lengkap dalam waktu 24 jam setelah pasien masuk adalah : proses kegiatan mengevaluasi pasien oleh tenaga medis\n paling lambat 24 jam setelah pasien masuk rawat inap meliputi mengumpulkan informasi, enganalisa informasi, membuatrencana pelayanan untuk memenuhi semua kebutuhan pasien yang telah diidentifikasi\n', '', 'Pasien yang meninggal dalam waktu 24 Jam setelah pasien masuk Rawat Inap atau pasien APS atau dirujuk sebelum waktu 24 Jam', 'Kelengkapan Assesmen Medis Dalam Waktu 24 Jam Setelah Pasien Masuk Rawat INAP', 'Rekam Medis', 'Proses', NULL, 'Ruang Rawat Inap', 'W', '100', 1, 0, 'A'),
(9, 'Assesemen awal merupakan langkah awal dalm mengenali informasi tentang data pasien', '', '', 'Kelengkapan Pengisian Assesemen Awal Bagi pasien rawat inap 1X24 jam', 'Ruang Perawatan 2', 'Input', NULL, 'Ruang Perawatan 2', 'M', '100%', 1, 0, 'A'),
(10, 'Merupakan manajemen komunikasi efektif yang di lakukan antar petugas agar tidak terjadi kesalahan dalam komunikasi', '', '', 'Kepatuhan penulisan SBAR', 'Ruang Perawatan 2', 'Input', NULL, 'Ruang Perawatan 2', 'M', '80%', 1, 0, 'A'),
(11, 'Waktu pemulihan pasien gangren di istalasi rawat inap dan di tata laksanakan oleh dokter DPJP sejak masuk kedalam ruang rawat inap.\nYAng di maksud dengan gangren adalah kondisi yang terjadi ketika jaringan tubuh mati. Sirkulasi yang terhambat menjadi penyebab  utama pada gengren. Gangren adalah kondisi darurat medis yang dapat menyebabkan amputasi bahkan kematian', 'Seluruh pasien gangren di rawat inap', '', 'Capaian indikkator medik', 'Rekam Medik', 'Input', NULL, 'Rekam Medik', 'M', '100%', 1, 0, 'A'),
(12, 'Kepuasan pelanggan (pasien khusus bedah) adalah pernyataan tentang persepsi pelanggan (pasien khusu bedah) terhadap jasa pelayanan kesehatan yang di berikan oleh RS.\nKepuasan pelanggan(pasien khusus bedah) dapat di capai apabila pelayanan yang di berikan sesuai atau melampaui harapan pelanggan.Hal ini dapat di ketahui dengan melakukan survey kepuasan  pelanggan (pasien khusus bedah) untuk mengetahui tingkat kepuasan pelanggan dengan mengacu pada kepuasan pelanggan berdasarkan Indeks Kepuasan Masyarakat (IKM).\nPengukuran IKM (pasien khusus bedah) Dilaksanakan di lokasi layanan sesuai dengan metode dan ketentuan sebagai mana di atur dalam pedoman umum penyusunan Indeks Kepuasan Masyarakat unit layanan instansi pemerintah (KepMenPan nomor KEP-25/M.PAN/2/2004) dan survey kepuasan masyarakat  (Permenpan No. 14 tahun 2017).', 'Pasien rawat inap yang telah di rawat inap 2X24 jam\nPasien rawat jalan yang telah berkunjung lebih dari 1 kali', 'Pasien dengan gangguan jiwa , tidak sadar dan pasien anak yang belum menerti survey', 'kepuasan pasien', 'Hasil survey kepuasan pasien di rawat inap dan rawat jalan', 'Input', NULL, 'Hasil survey kepuasan pasien di rawat inap dan rawat jalan', 'M', '85%', 1, 0, 'A'),
(13, 'Kepatuhan penulisan Tulbakon pada pasien bedah adalah di lakukannya prosedur komunikasi efektif pada pasien bedah di rawat', '', '', 'Keselamatan Pasien', 'Rekam Medis, observasi', 'Input', NULL, 'Rekam Medis, observasi', 'M', '0%', 1, 0, 'A'),
(14, 'Kepatuhan Penandaan area operasi pada operasi  Elektif adalah kepatuhan operator dalam menandai area operasi pada pasien elektif.Sehingga di kamar operasi dapat di klarifikasi ketepatan insisi dan ketepatan prosedur yang di pilih pada pasien yang tepat', '', '', 'Keselamatan Pasien', 'Rekam Medis, observasi', 'Input', NULL, 'Rekam Medis, observasi', 'M', '100%', 1, 0, 'A'),
(15, 'Ketepatan Waktu Pemberian Makanan Pada Pasien Rawat Inap', '', '', 'Ketepatan Waktu Distribusi Makan Pasien Rawat Inap', 'Instalasi Gizi', 'Input', NULL, 'Gizi', 'M', '90%', 1, 0, 'A'),
(16, 'Perlunya Pengetahuan Tentang Makanan Berkaitan dengan Penyakit  Yang di Derita Pasien di ruang Rawat Inap', '', '', 'Pemberian Konsultasi Gizi Pada Pasien Rawat Inap ', 'Instalasi Gizi', 'Input', NULL, 'Instalasi Gizi', 'M', '100%', 1, 0, 'A'),
(17, 'Perlunya Pengetahuan Tentang Makanan Berkaitan DEngan Penyakit yang di Derita Pasien Tertentu di Poli Rawat Jalan', '', '', 'Pemberian Konsultasi Gizi Pada Pasien Rawat Jalan', 'Instalasi Gizi', 'Input', NULL, 'Instalasi Gizi', 'M', '100%', 1, 0, 'A'),
(18, 'Ketetapan Penulisan Pemesanan Diet Sesuai Dengan PEnyakitnya', '', '', 'Ketetapan Penulisan Pemesanan Makanan Pasien', 'Instalasi Gizi', 'Input', NULL, 'Instalasi Gizi', 'M', '100%', 1, 0, 'A'),
(19, 'Kepatuhan Karyawan Dalam Pemakaian APD Di instalasi Gizi', '', '', 'Kepatuhan Pemakaian APD di Instilasi Gizi', 'Intalasi Gizi', 'Input', NULL, 'Intalasi Gizi', 'M', '90%', 1, 0, 'A'),
(20, 'Indikator Keamanan Makanan yang Disajikan', '', '', 'Adanya Benda Asing Dalam Makanan', 'Intalasi Gizi', 'Input', NULL, 'Intalasi Gizi', 'M', '0%', 1, 0, 'A'),
(21, 'Indikator Keselamatan Pegawai', '', '', 'Kecelakaan Kerja Pada Penyelenggaraan Makanan', 'Intalasi Gizi', 'Input', NULL, 'Intalasi Gizi', 'M', '0%', 1, 0, 'A'),
(22, 'Indikator Keamanan Makanan Yang Disajikan', '', '', 'Pengkajian Dan Asuhan Gizi', 'Intalasi Gizi', 'Input', NULL, 'Intalasi Gizi', 'M', '100%', 1, 0, 'A'),
(23, 'Terjadinya Kelengkapan Etiket Diet Untuk Semua Pasien Rawat Inap', '', '', 'Kelengkapan Etiket Diet', 'Intalasi Gizi', 'Input', NULL, 'Intalasi Gizi', 'M', '100%', 1, 0, 'A'),
(24, 'Utilisasi alat Ultra Wound Treatment adalah Pemanfaatan alat Ultra Wound Treatment yang Memiliki harga Beli Diatas 500 Juta Rupiah yang Akan Diginakan untuk Mendukung Penatalaksanaan Prosedur Bedah Khususnya pada Penanganan Luka Tanpa Merusak Jaringan Yang Sehat dalam Upaya Meningkatkan Mutu Pelayanan Pasien Bedah', '', '', 'Tingkat Kehandalan Sumber Daya', 'Laporan Pemeriksaan Menggunakan Alat Ultrasound Wound Treatment', 'Input', NULL, 'Laporan Pemeriksaan Menggunakan Alat Ultrasound Wound Treatment', 'M', '80%', 1, 0, 'A'),
(25, 'Kelengkapan Pengisian From Aseassement pra Bedah Sebelum Tindakan Operatif Dilakukan', 'Operasi yang Dilakukan di Ruang OK', '', 'pengisian form assesment prabedah', 'Catatan Data Pasien Operasi', 'Input', NULL, 'kamar oprasi', 'M', '100%', 1, 0, 'A'),
(26, 'Merupakan manajemen komunikasi efektif yang dilakukan antar petugas agar tidak terjadi kesalahan dalam komunikasi.', 'Jumlah penggunaan SBAR dalam setiap komunikasi antar petugas', 'Jumlah seluruh tindakan yang seharusnya menggunakan SBAR di Rekam Medik Pasien', 'kepatuhan penulisan SBAR', 'RUANG PERAWATAN 2', 'Input', NULL, 'B3', 'M', '80%', 1, 0, 'A'),
(27, 'utilisasi alat alat ultrasound wound treatment adalah pemnfaatan alat ultrasound wound treatment yang memiliki harga beli di atas 500 juta rupiah yang akan di gunakan untuk mendukung penata laksanaan prosedur bedah khususnya pada penanganan luka ganggrent atau selulitis tanpa merusak jaringan yang sehat dalam upaya meningkatkan mutu pelayanan pasien bedah', 'oprasi yang dilakukan diruang kamar oprasi', '', 'utilisasi alat ultrasound wound treatment', 'laporan register kamar oprasi', 'Input', NULL, 'kamar oprasi', 'M', '>80%', 2, 0, 'A'),
(28, '1. Melihat data rekapitulasi pengajuan perbaikan dan tindak lanjut Instalasi Pemeliharaan Sarana Prasarana\n2. Jumlah kejadian keterlambatan perbaikan alat umum yang tidak dapat diperbaiki sesuai waktu yang ditentukan \na. kerusakan ringan > 1 x 24 jam \nb. kerusakan sedang > 2 x 24 jam\nc. kerusakan berat > 7 x 24  jam ', '', '', 'Kecepatan terhadap respon komplain', 'IPSRS Form pengajuan perbaikan dan tindak lanjut ', 'Outcome and Process', NULL, 'Seluruh Area RSUD RA Basoeni ', 'M', '80%', 2, 0, 'A'),
(29, 'kejadian kekurangan air karena kerusakan sistem ataupun kekurangan supplay air selama 1 bulan \na. air bersih tidak ada karena stock habis\nb. air panas tidak sedia\nc. kerusakan alur pipa atau kerusakan mekanik & elektrikal ', '', '', 'angka kegagalan ketersedian air bersih di Rumah Sakit', 'IPS From perbaikan gedung dan perpipaan', 'Outcome and Process', NULL, 'seluruh area RSUD RA Basoeni', 'M', '100%', 2, 0, 'A'),
(30, 'jumlah ganguan listrik utama meliputi :\na. kubikel PLN Trip\nb. kegagalan sistem ATS\nc. genset gagal start\nd. genset overload', '', '', 'angka gangguan listrik gedung supplay dari PLN dan Genset', 'IPSRS', 'Input', NULL, 'Ruang Genset, Panel dan Ruang Kubikel', 'M', '', 1, 0, 'A'),
(31, 'Monitoring proses pemulihan anestesi dan sedasi dalam adalah pelayanan pada pasien pasca anesthesia sampai pasien pulih dari tindakan anestesia', '', '', 'Kelengkapan monitoring proses pemulihan anestesi dan sedasi dalam', 'Kamar Operasi', 'Input', NULL, 'Kamar Operasi', 'M', '100%', 1, 0, 'A'),
(32, 'Anestesi regional adalah tindakan pemberian anestetik untuk memblok saraf regional sehingga tercapai anesthesia di lokasi operasi sesuai yang diharapkan. Anestesi general adalah hilang kesadaran yang bersifat reversible yang disebabkan oleh agen anestetik dengan kehilangan sensasi nyeri di seluruh tubuh.', '', '', 'Kelengkapan evaluasi ulang bila terjadi konversi tindakan dari local/regional ke general', 'Kamar Operasi', 'Input', NULL, 'Kamar Operasi', 'M', '100%', 1, 0, 'A'),
(35, 'Angka kejadian obat kosong adalah banyaknya kejadian obat kosong (tidak terlayani) di Instalasi Farmasi baik rawat jalan maupun rawat inap', 'Seluruh obat essensial maupun obat setara yang kosong', 'Obat yang diresepkan di luar formularium', 'Angka Kejadian Obat Kosong', 'Resep', 'Input', NULL, 'Instalasi Farmasi', 'M', '0 %', 2, 0, 'A'),
(36, 'Waktu pemulihan pasien gangren di instalasi rawat inap dan ditatalaksana oleh dokter dpjp sejak masuk kedalam ruang rawat inap', '', '', 'Kesembuhan pasien gangren', 'Rekam Medik', 'Input', NULL, 'Perawatan 2', 'M', '100%', 1, 0, 'A'),
(37, 'Kepatuhan penulisan tulbakon pada pasien bedah yaitu dilakukan prosedur komunikasi efektif pada pasien bedah', '', '', 'Ketidak patuhan penulisan tulbakon', 'Rekam Medik', 'Input', NULL, 'Perawatan 2', 'M', '0%', 1, 0, 'A'),
(39, 'Internal indikator adalah kontrol mutu hasil sterilisasi yang digunakan khusus set alat dan linen kemasan besar.', '', '', 'Kepatuhan Pemakaian internal indikator pada proses sterilisasi', 'Catatan Register CSSD', 'Input', NULL, 'Ka. CSSD', 'M', '100%', 1, 0, 'A'),
(40, 'Pencucian linen infeksius dilakukan sesuai dengan SPO pencucian linen infeksius dalam setiap circle.', '', '', 'Prosedur pencucian linen infeksius', 'Catatan Register CSSD', 'Input', NULL, 'Ka. CSSD', 'M', '100%', 1, 0, 'A'),
(41, 'Memastikan bahwa mesin sterilisator dalam keadaan baik dan layak digunakan', '', '', 'Pemakaian test bowie dick pada proses sterilisasi', 'Catatan Register CSSD', 'Input', NULL, 'Ka. CSSD', 'M', '100%', 1, 0, 'A'),
(42, 'Semua kemasan yang keluar setelah proses sterilisasi dalam keadaan baik, utuh, tidak basah dan tidak sobek', '', '', 'Tidak adanya kerusakan kemasan setelah proses sterilisasi', 'Catatan Register CSSD', 'Input', NULL, 'Ka. CSSD', 'M', '100%', 1, 0, 'A'),
(43, 'Hasil indikator yang berubah warna menjadi hitam setelah proses sterilisasi\n', '', '', 'Kualitas hasil proses sterilisasi\n', 'Catatan Register CSSD', 'Input', NULL, 'Ka. CSSD', 'M', '100%', 1, 0, 'A'),
(44, 'penilaian dokter anastesi rumah sakit dalam kepatuhan melakukan visite pada pre op ke IRNA secara adekuat unit IRNA di RSUD RA BASOENI', '', '', 'KEPATUHAN DR ANASTESI MELAKUKAN VISITE PADA PASIEN PRE OP', 'perawatan 3', 'Input', NULL, 'perawatan p3', 'M', '100%', 1, 0, 'A'),
(45, 'penilaian kepatuhan petugas melakukan APD dalam pertolongan persalinan dan tindakan lain secara adekuat di VK di RSUD RA BASOENI', '', '', 'kepatuhan memakai APD dalam pertolongan persalinan', 'IRNA P3', 'Input', NULL, 'perawatan 3', 'M', '100%', 1, 0, 'A'),
(46, 'Semua instrumen alat rawat luka dan tindakan invasive lainnya harus disterilkan di CSSD, unit yang dimaksud adalah unit rawat jalan, IGD, Kamar operasi, Kamar Bersalin, HCU dan Ruang Perawatan.', '', '', 'Kepatuhan unit untuk mengirim bahan yang disterilisasi ke CSSD', 'Catatan Register CSSD', 'Input', NULL, 'Ka. CSSD', 'M', '100%', 1, 0, 'A'),
(47, 'Frekuensi kerusakan sterilisator sehingga gagal melakukan sterilisasi pada waktu itu, kerusakan air, listrik maupun komponen.\n', '', '', 'Angka kerusakan Sterilisator', 'Catatan Register CSSD', 'Input', NULL, 'Ka. CSSD', 'M', '100%', 1, 0, 'A'),
(48, 'Jumlah penggunaan fasilitas USG dalam waktu 1 bulan', '', '', 'ANGKA PEMANFAATAN USG', 'Radiologi', 'Input', NULL, 'Radiologi', 'M', '100%', 1, 0, 'A'),
(49, 'Indicator mutu untuk mengetahui kejadian / insiden reaksi obat kontras pada setiap pemeriksaan radiography dan upaya preventif yang harus dilakukan bila terjadi.', '', '', 'INSIDEN REAKSI KONTRAS OBAT', 'Radiologi', 'Input', NULL, 'Radiologi', 'Y', '0%', 1, 0, 'A'),
(50, 'Dokter spesialis radiologi yang mempunyai kewenangan untuk melakukan pembacaan foto rontgen/ hasil pemeriksaan radiologi. Bukti pembacaan dan verifikasi adalah dicantumkannya tandatangan dokter spesialis radiologi pada lembar hasil pemeriksaan yang dikirimkan kepada dokter yang  meminta', '', '', 'PELAKSANA EKSPERTISE  HASIL PEMERIKSAAN  RADIOLOGI', 'Radiologi', 'Input', NULL, 'Radiologi', 'M', '100%', 1, 0, 'A'),
(51, '', '', '', 'ketepatan dosis antibiotik', 'ruang interna kelas', 'Input', NULL, 'ruang interna kelas', 'M', '100%', 1, 0, 'A'),
(52, '', '', '', 'kelengkapan asessmen nyeri dan jatuh', 'ruang interna kelas', 'Input', NULL, 'ruang interna kelas', 'M', '100%', 1, 0, 'A'),
(54, 'False emergency (tidak gawat darurat)adalah pasien yang tidak dalam keadaan gawat dan darurat yang berkunjung ke IGD untuk mendapatkan pelayanan pengobatan', '', '', 'false triase di IGD', 'IGD', 'Input', NULL, 'IGD', 'M', '< 25%', 1, 0, 'A'),
(55, 'dokter IGD di tuntut dengan tanggung jawab yang besar dan mempunyai sertifikat serta pelatihan', '', '', 'dokter IGD yang memiliki sertifikat', 'IGD', 'Input', NULL, 'IGD', 'M', '100 %', 1, 0, 'A'),
(56, 'klinik spesialis adalah klinik pelayanan rawat jalan di rumah sakit yang dilayani oleh dokter spesialis', '', '', 'Pemberian pelayanan di klinik spesialis', 'registrasi rawat jalan poliklinik spesialis', 'Input', NULL, 'instalasi rawat jalan', 'M', '100%', 1, 0, 'A'),
(57, 'jam buka pelayanan adalah jam di mulainya pelayanan rawat jalan oleh tenaga spesialis jam buka 08.00 s/d 14.00', '', '', 'Buka pelayanan sesuai ketentuan', 'register rawat jalan poliklinik spesialis', 'Input', NULL, 'instalasi rawat jalan', 'Y', '100%', 1, 0, 'A'),
(58, 'Dilakukannya pemberian tanda area operasi untuk mengklarifikasi daerah yang akan di incise oleh operator', '', '', 'Penandaan area operasi di rawat jalan', 'Rekam medis, observasi', 'Input', NULL, 'Poli', 'M', '100%', 1, 0, 'A'),
(59, 'Informasi yang diberikan perawat saat pemasangan gelang mengenai maksud dan tujuan pemasangan gelang', '', '', 'Pelaksanaan edukasi mengenai pemasangan gelang identitas', 'Register rawat jalan, observasi', 'Input', NULL, 'Poli', 'M', '100%', 1, 0, 'A'),
(60, 'kejadian pasien jatuh di area poli fisioterapi', '', '', 'Insiden pasien jatuh di poli fisioterapi', 'Register rawat jalan, observasi', 'Input', NULL, 'Poli Fisioterapi', 'M', '0%', 1, 0, 'A'),
(61, '', '', '', 'ketepatan dosis', 'ruang interna kelas', 'Input', NULL, 'ruang interna kelas', 'M', '100%', 1, 0, 'A'),
(62, 'Formularium Nasional adalah daftar obat terpilih yang dibutuhkan dan harus tersedia di fasilitas pelayanan kesehatan sebagai acuan dalam pelaksanaan JKN-KIS.', 'Semua resep baik rawat inap maupun rawat jalan pada kasus bedah', 'Semua resep di luar kasus bedah', 'Kepatuhan Penulisan Resep terhadap Formularium Nasional (Fornas) pada Pasien Bedah', 'Resep', 'Input', NULL, 'Farmasi', 'M', '100 %', 1, 0, 'A'),
(63, 'Kesalahan pemberian obat adalah suatu kesalahan dalam proses pengobatan, dalam hal ini yang tidak sesuai dengan prinsip 7 benar (salah pasien, salah obat, salah dosis,salah cara pemberian,salahwaktu pemberian,salah informasi,salah dokumentasi)', 'Jumlah Seluruh Kesalahan Pemberian obat ( medication error ) yang terjadi di farmasi rawat jalan dan rawat inap pada pasien bedah', 'Jumlah Seluruh Kesalahan Pemberian obat ( medication error ) yang terjadi di farmasi rawat jalan dan rawat inap selain pasien bedah', 'Kesalahan Pemberian Obat pada Pasien Bedah', 'Resep', 'Input', NULL, 'Farmasi', 'M', '0 %', 1, 0, 'A'),
(64, 'kualitas pelayanan medis yang adekuat dan akurat', '', '', 'dokter spesialis pemberi pelayanan sesuai dengan kasus yang di tangani', 'ICU', 'Input', NULL, 'ICU', 'M', '100%', 1, 0, 'A'),
(65, 'penilaian pemberian tindakan life saving pada pasien anak dan dewasa sbagai tindakan resusitasi untuk menyelamatkan nyawa pasien', '', '', 'kemampuan melakukan tindakan life saving pada pasien anak dan dewasa', 'ICU', 'Input', NULL, 'ICU', 'Y', '100%', 1, 0, 'A'),
(66, 'penialaian angka kejadian pasien jatuh selama perawatan di ICU', '', '', 'kejadian pasien jatuh yang berakibat cacat atau kematian ', 'ICU', 'Input', NULL, 'ICU', 'M', '0%', 1, 0, 'A'),
(67, 'Penilaian perawat sebagai pemberi pelayanan di ICU sesuai dengan kompetensinya ', '', '', 'Perawatan pemberi pelayanan minimal bersertifikat minimal kegawat daruratan dan PL ICU', 'ICU', 'Input', NULL, 'ICU', 'M', '80%', 1, 0, 'A'),
(68, 'Penilaian persentase visite dokter spesialis di ICU selama hari kerja', '', '', 'Persentase visite dokter spesialis di ICU', 'ICU', 'Input', NULL, 'ICU', 'M', '100%', 1, 0, 'A'),
(69, 'Penilaian presentase visite dokter spesialis di ICU selama hari kerja', '', '', 'Kepatuhan Visite Dokter Spesialis di ICU', 'ICU', 'Input', NULL, 'ICU', 'M', '100%', 1, 0, 'A'),
(70, 'Pemberi pelayanan persalinan normal adalah dokter Sp. OG, dokter umum terlatih (asuhan persalinan normal) dan bidan', '', '', 'Pemberi Pelayanan Persalinan Normal (PONEK)', 'PONEK', 'Input', NULL, 'PONEK', 'M', '100%', 1, 0, 'A'),
(71, 'Pemberi pelayanan persalinan dengan penyulit adalah tim PONEK yang terdiri dari dokter Sp.OG, dokter umum, bidan dan perawat yang terlatih.\nPenyulit dalam persalinan antara lain meliputi partus lama, ketuban pecah dini, kelainan letak, berat badan janin diperkirakan kurang dari 2500 gr, kelainana panggul, perdarahan ante partum, eklamsia dan preemklamsia berat, tali pusat menumbung.\n', '', '', 'Pemberi Pelayanan Persalinan Dengan Penyulit (PONEK)', 'PONEK', 'Input', NULL, 'PONEK', 'M', '100%', 1, 0, 'A'),
(72, 'Pemberi pelayanan persalinan dengan tindakan operasi addalah dokter Sp.OG, dokter spesialis anak, dokter spesialis anastesi', '', '', 'Pemberi Pelayanan Persalinan Dengan Tindakan Operasi (PONEK)', 'PONEK', 'Input', NULL, 'PONEK', 'M', '100%', 1, 0, 'A'),
(73, 'BBLR adalah bayi yang lahir dengan berat badan 1500gr-2500gr', '', '', '4.	Kemampuan Menangani BBLR 1500Gr-2500Gr (PONEK)', 'PONEK', 'Input', NULL, 'PONEK', 'M', '100%', 1, 0, 'A'),
(74, 'Seksio Cesaria adalah tindakan persalinan melalui pembedahan abdominal baik elektif maupun emergensi', '', '', 'Pertolongan Persalinan Melalui Seksio Cesaria (PONEK)', 'PONEK', 'Input', NULL, 'PONEK', 'M', '55%', 1, 0, 'A'),
(75, 'Kontrasepsi mantap adalah vasektomi dan tubektomi. Konseling dilakukan minimal oleh tenaga bidan terlatih', '', '', 'Pelayanan Konseling pada Akseptor Kontrasepsi Mantap (PONEK)', 'PONEK', 'Input', NULL, 'PONEK', 'M', '100%', 1, 0, 'A'),
(76, 'Kepuasan pelanggan adalah pernyataan puas oleh pelanggan terhadap pelayanan persalinan', '', '', 'Kepuasan Pelanggan (PONEK)', 'PONEK', 'Input', NULL, 'PONEK', 'M', '90%', 1, 0, 'A'),
(77, 'Kompetensi Teknis', '', '', 'Kegagalan Pasokan Gas Medis', 'IPSRS', 'Input', NULL, '', 'M', '0%', 1, 0, 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hospital_survey_indicator_for_hospital`
--
ALTER TABLE `hospital_survey_indicator_for_hospital`
  ADD PRIMARY KEY (`indicator_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hospital_survey_indicator_for_hospital`
--
ALTER TABLE `hospital_survey_indicator_for_hospital`
  MODIFY `indicator_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
