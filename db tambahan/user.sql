-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 20, 2020 at 02:45 AM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 5.6.40-7+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sirsak_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_coder_nik` varchar(10) DEFAULT NULL,
  `user_profile_id` varchar(6) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_password` varchar(50) DEFAULT NULL,
  `user_assignment` varchar(20) NOT NULL,
  `user_fullname` varchar(100) DEFAULT NULL,
  `user_angkatan` varchar(11) DEFAULT NULL,
  `user_specialize_id` int(11) DEFAULT NULL,
  `user_instansi` varchar(250) DEFAULT NULL,
  `user_handphone` varchar(50) DEFAULT NULL,
  `user_telephone` varchar(50) DEFAULT NULL,
  `user_fax` varchar(50) DEFAULT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_bb_pin` varchar(10) DEFAULT NULL,
  `user_address` text,
  `user_city_id` int(4) DEFAULT NULL,
  `user_province_code` int(3) DEFAULT NULL,
  `user_domisili` varchar(35) DEFAULT NULL,
  `user_picture` varchar(50) DEFAULT NULL,
  `user_birth_place` varchar(50) DEFAULT NULL,
  `user_birth_date` date DEFAULT NULL,
  `user_gender` char(1) DEFAULT NULL,
  `user_npwp` varchar(50) DEFAULT NULL,
  `user_fullname_npwp` varchar(50) DEFAULT NULL,
  `user_bank` varchar(50) DEFAULT NULL,
  `user_bank_account` varchar(50) DEFAULT NULL,
  `user_bank_account_holder` varchar(30) DEFAULT NULL,
  `user_national_id` varchar(20) DEFAULT NULL,
  `user_fullname_national_id` varchar(50) DEFAULT NULL,
  `user_access_chapter_id` varchar(100) NOT NULL DEFAULT '0',
  `user_accreditation_version` char(4) DEFAULT NULL,
  `user_insert_by` char(6) NOT NULL,
  `user_insert_date` datetime NOT NULL,
  `user_update_by` char(6) DEFAULT NULL,
  `user_update_date` datetime DEFAULT NULL,
  `user_delete_by` char(6) DEFAULT NULL,
  `user_delete_date` datetime DEFAULT NULL,
  `user_room_id` char(4) DEFAULT NULL,
  `user_level_id` int(11) DEFAULT '2',
  `user_group_id` varchar(50) DEFAULT '4',
  `user_department_id` varchar(20) DEFAULT NULL,
  `user_poly_id` varchar(3) DEFAULT NULL,
  `user_last_login` datetime DEFAULT NULL,
  `user_session` char(32) DEFAULT NULL,
  `user_amount_login` char(5) DEFAULT NULL,
  `user_last_activity` char(10) DEFAULT NULL,
  `user_online_status` char(1) DEFAULT NULL,
  `user_tarrif_mapping` char(1) NOT NULL DEFAULT '0',
  `user_record_status` char(1) NOT NULL,
  `user_hospital_id` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_coder_nik`, `user_profile_id`, `user_name`, `user_password`, `user_assignment`, `user_fullname`, `user_angkatan`, `user_specialize_id`, `user_instansi`, `user_handphone`, `user_telephone`, `user_fax`, `user_email`, `user_bb_pin`, `user_address`, `user_city_id`, `user_province_code`, `user_domisili`, `user_picture`, `user_birth_place`, `user_birth_date`, `user_gender`, `user_npwp`, `user_fullname_npwp`, `user_bank`, `user_bank_account`, `user_bank_account_holder`, `user_national_id`, `user_fullname_national_id`, `user_access_chapter_id`, `user_accreditation_version`, `user_insert_by`, `user_insert_date`, `user_update_by`, `user_update_date`, `user_delete_by`, `user_delete_date`, `user_room_id`, `user_level_id`, `user_group_id`, `user_department_id`, `user_poly_id`, `user_last_login`, `user_session`, `user_amount_login`, `user_last_activity`, `user_online_status`, `user_tarrif_mapping`, `user_record_status`, `user_hospital_id`) VALUES
(1, NULL, '1', 'admin@kars.or.id', '7e805b44db82acf0c71e65b786363da4', '1234', 'Administrator', NULL, NULL, NULL, '081218161603', NULL, NULL, 'admin@kars.or.id', NULL, NULL, NULL, NULL, NULL, NULL, 'Jakarta', '2018-04-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '17,18,19', NULL, '', '2018-04-18 10:53:10', NULL, NULL, NULL, NULL, NULL, 1, '1', '1', NULL, NULL, NULL, NULL, NULL, NULL, '1', 'A', 160),
(2, NULL, '4', 'skp@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'Dwi Yulianto', NULL, NULL, NULL, '0897654324152', NULL, NULL, 'skp@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'Mojokerto', '1980-03-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '17', NULL, '1', '2019-03-04 08:03:36', '1', '2019-03-04 08:14:34', NULL, NULL, '', 2, '20', '10', '29', NULL, NULL, NULL, NULL, NULL, '0', 'A', 3516073),
(3, NULL, '5', 'it@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'Mochamad Efendi', NULL, NULL, NULL, '085706729805', NULL, NULL, 'it@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'Jombang', '2019-03-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '30', NULL, '1', '2019-03-04 08:47:38', '1', '2019-03-06 07:39:25', NULL, NULL, '', 2, '21', '16', '48', NULL, NULL, NULL, NULL, NULL, '0', 'A', 3516073),
(4, NULL, '2', 'haji@baso.eni', '00f5c5291b67079e6f5df5b5af479976', '', 'Haji', NULL, NULL, NULL, '08783389720192', NULL, NULL, 'haji@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'Mojokerto', '2019-02-28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-04 11:07:20', '1', '2019-03-26 09:25:05', NULL, NULL, '', 2, '22', '', '48', NULL, NULL, NULL, NULL, NULL, '0', 'A', 3516073),
(5, NULL, '7', 'ophil.valkyrie@gmail.com', 'bdc601c4b963b45eea83ed3624203528', '', 'ASYROFIL AFIKA FATTA', NULL, NULL, NULL, '085648786828', NULL, NULL, 'ophil.valkyrie@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'Jombang', '1991-07-16', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '30', NULL, '1', '2019-03-04 22:36:09', '1', '2019-03-05 08:22:23', NULL, NULL, '', 2, '21', '16', '', NULL, NULL, NULL, NULL, NULL, '0', 'A', 3516073),
(6, NULL, '8', 'zaki@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'Abdul Hamid Muzaki', NULL, NULL, NULL, '087685968374', NULL, NULL, 'zaki@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'Mojokerto', '2000-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-05 08:32:04', '1', '2019-03-29 11:51:15', NULL, NULL, '', 2, '21', '', '48', NULL, NULL, NULL, NULL, NULL, '0', 'A', 3516073),
(7, NULL, '9', 'lab@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'Dodit', NULL, NULL, NULL, '085850542164', NULL, NULL, 'lab@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'Mojokerto', '1974-09-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20', NULL, '1', '2019-03-05 10:48:51', '1', '2019-04-06 10:02:07', NULL, NULL, '', 2, '21', '67', '47', NULL, NULL, NULL, NULL, NULL, '0', 'A', 3516073),
(8, NULL, '10', 'mirm@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'MIRM', NULL, NULL, NULL, '085706729805', NULL, NULL, 'mirm@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'MOJOKERTO', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '30', NULL, '1', '2019-03-05 17:39:48', '1', '2019-03-23 22:05:15', NULL, NULL, '', 2, '21', '52', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(9, NULL, '11', 'pmkp@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'PMKP', NULL, NULL, NULL, '08524152', NULL, NULL, 'pmkp@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '25', NULL, '1', '2019-03-05 17:44:25', NULL, NULL, NULL, NULL, '', 2, '20', '', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(10, NULL, '13', 'pn@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'Program Nasional', NULL, NULL, NULL, '02814364', NULL, NULL, 'pn@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '31', NULL, '1', '2019-03-05 17:51:37', NULL, NULL, NULL, NULL, '', 2, '20', '', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(11, NULL, '12', 'hpk@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'HPK', NULL, NULL, NULL, '08512541', NULL, NULL, 'hpk@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'MOJOKERTO', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '19', NULL, '1', '2019-03-05 17:51:39', NULL, NULL, NULL, NULL, '', 2, '20', '', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(12, NULL, '14', 'ppi@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'PPI', NULL, NULL, NULL, '0852125456', NULL, NULL, 'ppi@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'MOJOKERTO', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '26', NULL, '1', '2019-03-05 17:55:36', '1', '2019-03-15 08:13:36', NULL, NULL, '', 2, '21', '', '67', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(13, NULL, '15', 'kks@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'KKS', NULL, NULL, NULL, '0857435578', NULL, NULL, 'kks@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'Mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '29', NULL, '1', '2019-03-05 17:58:36', NULL, NULL, NULL, NULL, '', 2, '20', '', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(14, NULL, '16', 'skp1@baso.eni', '42af716f5ffa4cfab496b42a11df4788', '', 'SKP', NULL, NULL, NULL, '085212541', NULL, NULL, 'skp1@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-05 17:59:37', '1', '2019-03-24 11:49:07', NULL, NULL, '', 2, '20', '52', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(15, NULL, '17', 'ark@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'ARK', NULL, NULL, NULL, '085213642', NULL, NULL, 'ark@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'MOJOKERTO', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '18', NULL, '1', '2019-03-05 18:00:05', NULL, NULL, NULL, NULL, '', 2, '20', '', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(16, NULL, '19', 'ap@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'Yunit', NULL, NULL, NULL, '082245893245', NULL, NULL, 'ap@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'Kediri', '1983-07-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '20', NULL, '1', '2019-03-05 18:02:57', '1', '2019-03-08 09:15:24', NULL, NULL, '1', 2, '20', '18', '48', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(17, NULL, '18', 'mfk@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'MFK', NULL, NULL, NULL, '0857335697', NULL, NULL, 'mfk@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'Mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '28', NULL, '1', '2019-03-05 18:03:13', NULL, NULL, NULL, NULL, '', 2, '20', '', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(18, NULL, '20', 'pap@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'PAP', NULL, NULL, NULL, '085212541', NULL, NULL, 'pap@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '21', NULL, '1', '2019-03-05 18:03:15', NULL, NULL, NULL, NULL, '', 2, '20', '', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(19, NULL, '22', 'tkrs@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'TKRS', NULL, NULL, NULL, '0857664595', NULL, NULL, 'tkrs@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'Mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27', NULL, '1', '2019-03-05 18:05:43', NULL, NULL, NULL, NULL, '', 2, '20', '', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(20, NULL, '21', 'pab@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'PAB', NULL, NULL, NULL, '0852125412', NULL, NULL, 'pab@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '22', NULL, '1', '2019-03-05 18:05:51', NULL, NULL, NULL, NULL, '', 2, '20', '', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(21, NULL, '23', 'pkpo@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'PKPO', NULL, NULL, NULL, '0185412541', NULL, NULL, 'pkpo@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '23', NULL, '1', '2019-03-05 18:06:14', NULL, NULL, NULL, NULL, '', 2, '20', '', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(22, NULL, '24', 'mke@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'MKE', NULL, NULL, NULL, '0821435568', NULL, NULL, 'mke@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'Mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '24', NULL, '1', '2019-03-05 18:08:31', NULL, NULL, NULL, NULL, '', 2, '20', '', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(23, NULL, '5', 'admin@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'Mochamad Efendi', NULL, NULL, NULL, '085706729805', NULL, NULL, 'admin@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'Jombang', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-15 08:23:20', '1', '2019-03-15 08:28:15', '1', '2019-03-16 07:49:10', '', 2, '19', '', '34', NULL, NULL, NULL, NULL, NULL, '1', 'D', 3516073),
(24, NULL, '26', 'Surveior@kars.or.id', '827ccb0eea8a706c4c34a16891f84e7b', '', 'Surveior', NULL, NULL, NULL, '0851672682', NULL, NULL, 'Surveior@kars.or.id', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-23 22:16:36', '1', '2019-05-08 09:18:25', NULL, NULL, '', 2, '22', '', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(25, NULL, '27', 'ipkp@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'IPKP', NULL, NULL, NULL, '085421456677', NULL, NULL, 'ipkp@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'MOJOKERTO', '2019-03-27', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '32', NULL, '1', '2019-03-27 12:30:55', NULL, NULL, NULL, NULL, '', 2, '20', '', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(26, NULL, '7', 'test@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b', '', 'ASYROFIL AFIKA FATTA', NULL, NULL, NULL, '085648786828', NULL, NULL, 'test@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 'Jombang', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-28 17:05:59', '1', '2019-03-29 13:59:00', NULL, NULL, '', 2, '19', '52', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(27, NULL, '28', 'p1@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'KEPERAWATAN 1', NULL, NULL, NULL, '5416541841', NULL, NULL, 'p1@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-29 14:21:41', NULL, NULL, NULL, NULL, '', 2, '19', '53', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(28, NULL, '29', 'p2int@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'INT', NULL, NULL, NULL, '0812', NULL, NULL, 'p2int@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'MOJOKERTO', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 08:19:50', NULL, NULL, NULL, NULL, '', 2, '19', '54', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(29, NULL, '30', 'p3@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'VK', NULL, NULL, NULL, '085', NULL, NULL, 'p3@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'MOJOKERTO', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 08:20:12', NULL, NULL, NULL, NULL, '', 2, '19', '55', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(30, NULL, '31', 'p4@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'neo', NULL, NULL, NULL, '0875', NULL, NULL, 'p4@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 08:20:42', NULL, NULL, NULL, NULL, '', 2, '19', '56', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(31, NULL, '34', 'p2bdh@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'p2bdh', NULL, NULL, NULL, '0214', NULL, NULL, 'p2bdh@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'jombang', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 08:21:16', '1', '2019-04-06 12:36:51', NULL, NULL, '', 2, '19', '59', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(32, NULL, '35', 'ok@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'ok', NULL, NULL, NULL, '021587', NULL, NULL, 'ok@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 08:21:54', NULL, NULL, NULL, NULL, '', 2, '19', '60', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(33, NULL, '36', 'icu@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'icu', NULL, NULL, NULL, '0854', NULL, NULL, 'icu@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'jombang', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 08:22:09', NULL, NULL, NULL, NULL, '', 2, '19', '61', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(34, NULL, '37', 'poli@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'poli', NULL, NULL, NULL, '02158', NULL, NULL, 'poli@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'jombang', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 08:22:23', NULL, NULL, NULL, NULL, '', 2, '19', '62', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(35, NULL, '38', 'igd@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'igd', NULL, NULL, NULL, '02', NULL, NULL, 'igd@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 08:22:39', NULL, NULL, NULL, NULL, '', 2, '19', '63', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(36, NULL, '39', 'rm@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'rm', NULL, NULL, NULL, '021', NULL, NULL, 'rm@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'jombang', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 08:22:52', NULL, NULL, NULL, NULL, '', 2, '19', '64', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(37, NULL, '40', 'cssd@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'cssd', NULL, NULL, NULL, '021', NULL, NULL, 'cssd@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'jombang', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 08:23:08', NULL, NULL, NULL, NULL, '', 2, '19', '65', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(38, NULL, '41', 'iprs@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'iprs', NULL, NULL, NULL, '021', NULL, NULL, 'iprs@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'jombang', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 08:23:25', NULL, NULL, NULL, NULL, '', 2, '19', '66', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(39, NULL, '43', 'rad@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'radiologi', NULL, NULL, NULL, '087', NULL, NULL, 'rad@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 08:24:04', NULL, NULL, NULL, NULL, '', 2, '19', '68', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(40, NULL, '44', 'tu@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'tu', NULL, NULL, NULL, '021', NULL, NULL, 'tu@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 08:24:20', NULL, NULL, NULL, NULL, '', 2, '19', '69', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(41, NULL, '45', 'keu@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'keuangan', NULL, NULL, NULL, '081', NULL, NULL, 'keu@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 08:24:37', NULL, NULL, NULL, NULL, '', 2, '19', '70', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(42, NULL, '32', 'b3@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'b3', NULL, NULL, NULL, '02147', NULL, NULL, 'b3@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'surabaya', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 08:31:36', NULL, NULL, NULL, NULL, '', 2, '19', '57', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(43, NULL, '33', 'b4@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'b4', NULL, NULL, NULL, '0214', NULL, NULL, 'b4@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'surabaya', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 08:36:15', NULL, NULL, NULL, NULL, '', 2, '19', '58', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(44, NULL, '46', 'farmasi@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'farmasi', NULL, NULL, NULL, '545481', NULL, NULL, 'farmasi@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-30 12:17:35', '1', '2019-03-30 12:18:49', NULL, NULL, '', 2, '19', '71', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(45, NULL, '47', 'gizi@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'gizi', NULL, NULL, NULL, '5415125151', NULL, NULL, 'gizi@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-31 20:55:39', NULL, NULL, NULL, NULL, '', 2, '19', '72', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(46, NULL, '48', 'master@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'master', NULL, NULL, NULL, '6545125151', NULL, NULL, 'master@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-31 20:57:37', NULL, NULL, NULL, NULL, '', 2, '22', '', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(47, NULL, '49', 'komiteppi@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'Anyta', NULL, NULL, NULL, '081515347732', NULL, NULL, 'komiteppi@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'Mojokerto', '1982-11-02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-04-09 10:36:19', NULL, NULL, NULL, NULL, '', 2, '22', '', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(48, NULL, '50', 'kasir@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'Kasir', NULL, NULL, NULL, '08521365123', NULL, NULL, 'kasir@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'Mojokerto', '2010-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-04-09 10:57:19', '1', '2019-04-09 10:57:38', NULL, NULL, '', 2, '19', '74', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(49, NULL, '52', 'surveior1@kars.or.id', '827ccb0eea8a706c4c34a16891f84e7b', '', 'Surveior1', NULL, NULL, NULL, '0', NULL, NULL, 'surveior1@kars.or.id', NULL, NULL, NULL, NULL, NULL, NULL, 'Mojokerto', '2019-05-08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-05-08 09:45:24', NULL, NULL, NULL, NULL, '', 2, '20', '', '', NULL, NULL, NULL, NULL, NULL, '1', 'A', 3516073),
(50, NULL, '53', 'labpa@baso.eni', '827ccb0eea8a706c4c34a16891f84e7b', '', 'Lab PA', NULL, NULL, NULL, '085850542164', NULL, NULL, 'labpa@baso.eni', NULL, NULL, NULL, NULL, NULL, NULL, 'Mojokerto', '1974-09-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '1', '2019-03-05 10:48:51', '1', '2020-02-20 08:31:18', NULL, NULL, '', 2, '19', '76', '47', NULL, NULL, NULL, NULL, NULL, '0', 'A', 3516073);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
