-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 20, 2020 at 02:45 AM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 5.6.40-7+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sirsak_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `master_hospital_department`
--

CREATE TABLE `master_hospital_department` (
  `department_id` int(3) NOT NULL,
  `department_name` varchar(50) NOT NULL,
  `department_description` text NOT NULL,
  `department_hospital_id` int(5) NOT NULL,
  `department_record_status` char(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `master_hospital_department`
--

INSERT INTO `master_hospital_department` (`department_id`, `department_name`, `department_description`, `department_hospital_id`, `department_record_status`) VALUES
(1, 'Unit 1', 'Unit 1 - Bagian Instalasi', 123, 'D'),
(2, 'Unit 2', 'Unit 2 - Bagian Farmasi', 772, 'D'),
(3, 'unit 3', 'instalasi rekam medis ', 772, 'D'),
(4, 'unit 3', 'bidang pelayanan', 772, 'D'),
(5, 'Komite Mutu dan Manajemen Risiko', 'KMMR', 772, 'D'),
(6, 'Komite Pencegahan dan Pengendalian Infeksi', 'KPPI', 772, 'D'),
(7, 'Rekam Medis', 'Unit yang melayani pendataan rekam medis', 3495, 'D'),
(8, 'Gudang', 'Gudang', 695, 'D'),
(9, 'Unit Praktek I', 'Unit', 695, 'D'),
(10, 'Unit Pokja I', 'Pokja', 695, 'D'),
(11, 'Keuangan', 'Bagian Keuangan', 3516073, 'D'),
(12, 'Tata Usaha', 'Bagian Tata Usaha', 3516073, 'D'),
(13, 'Pelayanan', 'Bagian Pelayanan', 3516073, 'D'),
(14, 'Perawatan', 'Bagian Perawatan', 3516073, 'D'),
(15, 'Administrasi', 'Bagian Admisi', 3516073, 'D'),
(16, 'IT', 'Bagian IT', 3516073, 'D'),
(17, 'MIRM', 'TIM POKJA', 3516073, 'D'),
(18, 'Rawat Inap', 'Rawat Inap', 3516073, 'D'),
(19, 'Laboratorium', 'Laboratorium', 3516073, 'D'),
(20, 'Poli Gigi', 'Gigi', 3516073, 'D'),
(21, 'Poli Mata', 'Mata', 3516073, 'D'),
(22, 'ARK', 'TIM POKJA', 3516073, 'D'),
(23, 'Poli Anak', 'Anak', 3516073, 'D'),
(24, 'Poli Dalam', 'Dalam', 3516073, 'D'),
(25, 'Poli Fisioterapi', 'Fisioterapi', 3516073, 'D'),
(26, 'Poli Kulit', 'Kulit', 3516073, 'D'),
(27, 'Poli DOTS', 'DOTS', 3516073, 'D'),
(28, 'TKRS', 'TIM POKJA', 3516073, 'D'),
(29, 'Poli Obgyn', 'Obgyn', 3516073, 'D'),
(30, 'Poli Bedah', 'Bedah', 3516073, 'D'),
(31, 'Poli Syaraf', 'Syaraf', 3516073, 'D'),
(32, 'ICU', 'Unit ICU', 3516073, 'D'),
(33, 'Poli THT', 'THT', 3516073, 'D'),
(34, 'Poli Jiwa', 'Jiwa', 3516073, 'D'),
(35, 'Poli Orthopedi', 'Orthopedi', 3516073, 'D'),
(36, 'Poli Paru', 'Paru', 3516073, 'D'),
(37, 'Poli Jantung', 'Jantung', 3516073, 'D'),
(38, 'Poli Umum', 'Umum', 3516073, 'D'),
(39, 'NICU', 'NICU', 3516073, 'D'),
(40, 'Apotek', 'Apotek', 3516073, 'D'),
(41, 'Gudang ', 'Gudang', 3516073, 'D'),
(42, 'CSSD', 'CSSD', 3516073, 'D'),
(43, 'Radiologi', 'Radiologi', 3516073, 'D'),
(44, 'Kamar Operasi', 'OK', 3516073, 'D'),
(45, 'Kamar Bersalin', 'Bersalin', 3516073, 'D'),
(46, 'Gizi', 'Gizi', 3516073, 'D'),
(47, 'IPRS', 'IPRS', 3516073, 'D'),
(48, 'IGD', 'IGD', 3516073, 'D'),
(49, 'it', 'it', 3516073, 'D'),
(50, 'igd', 'igd', 3516073, 'D'),
(51, 'radiologi', 'radiologi', 3516073, 'D'),
(52, 'IMUT area manajemen', 'IMUT area manajemen', 3516073, 'A'),
(53, 'P1', 'P1', 3516073, 'A'),
(54, 'P2 INTERNA', 'P2 INTERNA', 3516073, 'A'),
(55, 'P3', 'P3 VK', 3516073, 'A'),
(56, 'P4', 'NEO', 3516073, 'A'),
(57, 'B3', 'B3', 3516073, 'A'),
(58, 'B4', 'B4', 3516073, 'A'),
(59, 'P2', 'BEDAH', 3516073, 'A'),
(60, 'OK', 'OK', 3516073, 'A'),
(61, 'ICU', 'ICU', 3516073, 'A'),
(62, 'POLI', 'POLI', 3516073, 'A'),
(63, 'IGD', 'IGD', 3516073, 'A'),
(64, 'RM', 'RM', 3516073, 'A'),
(65, 'CSSD', 'CSSD', 3516073, 'A'),
(66, 'IPRS', 'IPRS', 3516073, 'A'),
(67, 'LAB', 'LAB', 3516073, 'A'),
(68, 'RADIOLOGI', 'RADIOLOGI', 3516073, 'A'),
(69, 'TU', 'TU', 3516073, 'A'),
(70, 'KEUANGAN', 'KEUANGAN', 3516073, 'A'),
(71, 'farmasi', 'farmasi', 3516073, 'A'),
(72, 'gizi', 'gizi', 3516073, 'A'),
(73, 'IT', 'IT', 3516073, 'A'),
(74, 'Kasir', 'Kasir', 3516073, 'A'),
(75, 'LABPA', 'laboratorium patologi anatomi', 3516073, 'D'),
(76, 'Lab PA', 'Lab PA', 3516073, 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `master_hospital_department`
--
ALTER TABLE `master_hospital_department`
  ADD PRIMARY KEY (`department_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `master_hospital_department`
--
ALTER TABLE `master_hospital_department`
  MODIFY `department_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
