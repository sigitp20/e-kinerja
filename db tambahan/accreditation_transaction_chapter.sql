-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 17, 2020 at 08:05 AM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 5.6.40-7+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sirsak_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `accreditation_transaction_chapter`
--

CREATE TABLE `accreditation_transaction_chapter` (
  `trans_id` int(11) NOT NULL,
  `trans_accreditation_id` int(11) NOT NULL,
  `trans_event_id` varchar(10) NOT NULL,
  `trans_hospital_id` int(11) NOT NULL,
  `trans_chapter_id` int(11) NOT NULL,
  `trans_hospital_score` float DEFAULT NULL,
  `trans_hospital_date` datetime DEFAULT NULL,
  `trans_surveyor_id` int(11) DEFAULT NULL,
  `trans_remedial_surveyor_score` float DEFAULT NULL,
  `trans_surveyor_score` float DEFAULT NULL,
  `trans_remedial_surveyor_date` datetime DEFAULT NULL,
  `trans_surveyor_date` datetime DEFAULT NULL,
  `trans_counselor_id` int(11) DEFAULT NULL,
  `trans_counselor_score` float DEFAULT NULL,
  `trans_counselor_date` datetime DEFAULT NULL,
  `trans_remedial_surveyor_id` int(11) DEFAULT NULL,
  `trans_final_score` float DEFAULT NULL,
  `trans_post_date` datetime NOT NULL,
  `trans_user_id` int(11) NOT NULL,
  `trans_record_status` enum('A','D') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accreditation_transaction_chapter`
--

INSERT INTO `accreditation_transaction_chapter` (`trans_id`, `trans_accreditation_id`, `trans_event_id`, `trans_hospital_id`, `trans_chapter_id`, `trans_hospital_score`, `trans_hospital_date`, `trans_surveyor_id`, `trans_remedial_surveyor_score`, `trans_surveyor_score`, `trans_remedial_surveyor_date`, `trans_surveyor_date`, `trans_counselor_id`, `trans_counselor_score`, `trans_counselor_date`, `trans_remedial_surveyor_id`, `trans_final_score`, `trans_post_date`, `trans_user_id`, `trans_record_status`) VALUES
(1, 1, '1', 3516073, 17, 87.84, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-04 08:11:33', 1, 'A'),
(2, 1, '1', 3516073, 18, 90.4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-04 08:11:33', 1, 'A'),
(3, 1, '1', 3516073, 19, 85.48, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-04 08:11:33', 1, 'A'),
(4, 1, '1', 3516073, 20, 90.8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-04 08:11:33', 1, 'A'),
(5, 1, '1', 3516073, 21, 86, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-04 08:11:33', 1, 'A'),
(6, 1, '1', 3516073, 22, 90, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-04 08:11:33', 1, 'A'),
(7, 1, '1', 3516073, 23, 86.88, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-04 08:11:33', 1, 'A'),
(8, 1, '1', 3516073, 24, 97.96, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-04 08:11:33', 1, 'A'),
(9, 1, '1', 3516073, 25, 84.38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-04 08:11:33', 1, 'A'),
(10, 1, '1', 3516073, 26, 88.83, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-04 08:11:33', 1, 'A'),
(11, 1, '1', 3516073, 27, 81.75, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-04 08:11:33', 1, 'A'),
(12, 1, '1', 3516073, 28, 87.5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-04 08:11:33', 1, 'A'),
(13, 1, '1', 3516073, 29, 81.25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-04 08:11:33', 1, 'A'),
(14, 1, '1', 3516073, 30, 81.82, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-04 08:11:33', 1, 'A'),
(15, 1, '1', 3516073, 31, 92.24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-04 08:11:33', 1, 'A'),
(16, 1, '1', 3516073, 32, 85.71, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-03-04 08:11:33', 1, 'A'),
(17, 2, '2', 3516073, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-07 09:43:40', 20, 'A'),
(18, 2, '2', 3516073, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-07 09:43:40', 20, 'A'),
(19, 2, '2', 3516073, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-07 09:43:40', 20, 'A'),
(20, 2, '2', 3516073, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-07 09:43:40', 20, 'A'),
(21, 2, '2', 3516073, 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-07 09:43:40', 20, 'A'),
(22, 2, '2', 3516073, 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-07 09:43:40', 20, 'A'),
(23, 2, '2', 3516073, 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-07 09:43:40', 20, 'A'),
(24, 2, '2', 3516073, 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-07 09:43:40', 20, 'A'),
(25, 2, '2', 3516073, 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-07 09:43:40', 20, 'A'),
(26, 2, '2', 3516073, 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-07 09:43:40', 20, 'A'),
(27, 2, '2', 3516073, 11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-07 09:43:40', 20, 'A'),
(28, 2, '2', 3516073, 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-07 09:43:40', 20, 'A'),
(29, 2, '2', 3516073, 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-07 09:43:40', 20, 'A'),
(30, 2, '2', 3516073, 14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-07 09:43:40', 20, 'A'),
(31, 2, '2', 3516073, 15, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-05-07 09:43:40', 20, 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accreditation_transaction_chapter`
--
ALTER TABLE `accreditation_transaction_chapter`
  ADD PRIMARY KEY (`trans_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accreditation_transaction_chapter`
--
ALTER TABLE `accreditation_transaction_chapter`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
