<?php
class imut extends CI_Controller
{

    var $folder =   "imut";
    var $tables =   "accreditation_instrument_chapter";
    var $tables2 =   "accreditation_transaction_chapter";
    var $pk     =   "chapter_id";
    var $title  =   "Indikator Mutu Harian";
    function __construct()
    {
        parent::__construct();
        $this->load->model('imut_m');
        $this->load->model('mcrud');
        date_default_timezone_set('Asia/Jakarta');
        check_not_login();
    }

    function index()
    {
        $this->manajemen();
    }

    function manajemen($tglP = null)
    {

        if (isset($_POST['submit'])) {
            $tglP  =   $this->input->post('tglP');
        }

        if ($tglP == null) {
            $bulan = (date('m'));
            $tahun = (date('Y'));
        } else {
            $bulan = substr($tglP, 5, 2);
            $tahun = explode('-', $tglP)[0];
        }

        // $data['id'] =  $event_version;
        $bul = array(1 => "Januari", 2 => "Februari", 3 => "Maret", 4 => "April", 5 => "Mei", 6 => "Juni", 7 => "Juli", 8 => "Agustus", 9 => "September", 10 => "Oktober", 11 => "November", 12 => "Desember");
        if ($bulan <= 9) {
            $bl = substr($bulan, 1, 1);
        } else {
            $bl = $bulan;
        }
        $data['title'] =  "Indikator Mutu Harian Bulan " . $bul[$bl] . " " . $tahun;

        $indicator_id_user =  $this->session->userdata('hospital_survey_indicator');

        if ($indicator_id_user != null) {
            $data['record'] = $this->imut_m->imutManajemen("(indicator_category_id = 2 and indicator_id in ($indicator_id_user)) or (indicator_category_id = 2 and (MONTH(result_period) = $bulan and YEAR(result_period) = $tahun) and indicator_id in ($indicator_id_user))");
        } else if ($indicator_id_user == null) {
            $data['record'] = $this->imut_m->imutManajemen("indicator_category_id = 2 or (indicator_category_id = 2 and (MONTH(result_period) = $bulan and YEAR(result_period) = $tahun))");
        }

        $indicator_element2 = array(); //$nama2
        $indicator_id2 = array(); //$id2

        //mengambil indicator_element & indicator_id yang dobel
        for ($i = 1; $i <= count($data['record']); $i++) {
            if ($i == 1) {
                $indicator_element2[1] = $data['record'][0]->indicator_element;
                $indicator_id2[1] = $data['record'][0]->indicator_id;
                $k = 2;
            } else {
                if ($data['record'][$i - 1]->indicator_element != $indicator_element2[$k - 1]) {
                    $indicator_element2[$k] = $data['record'][$i - 1]->indicator_element;
                    $indicator_id2[$k] = $data['record'][$i - 1]->indicator_id;
                    $k++;
                }
            }
        }

        //get last day of month
        $lastDate = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

        //looping tgl menyammping
        $record = array();
        for ($c = 1; $c <= count($indicator_element2); $c++) {
            for ($d = 1; $d <= $lastDate; $d++) { //(31) $data['sheet']
                $record[$indicator_element2[$c]][$d]['nilai'] = $this->getNilai($d, $bulan, $tahun, $indicator_element2[$c],  $data);
                if ($record[$indicator_element2[$c]][$d]['nilai'] == null) {
                    $record[$indicator_element2[$c]][$d]['nilai'] = 0;
                }
                $record[$indicator_element2[$c]][$d]['tgl'] = $this->getTgl($d, $bulan, $tahun, $indicator_element2[$c],  $data);
                if ($record[$indicator_element2[$c]][$d]['tgl'] == null) {
                    if ($d <= 9) {
                        $record[$indicator_element2[$c]][$d]['tgl'] = $tahun . "-" . $bulan . "-0" . $d;
                    } else {
                        $record[$indicator_element2[$c]][$d]['tgl'] = $tahun . "-" . $bulan . "-" . $d;
                    }
                }
                $record[$indicator_element2[$c]][$d]['nama'] = $indicator_element2[$c];
                $record[$indicator_element2[$c]][$d]['id'] = $indicator_id2[$c];

                // $absen[$indicator_element2[$c]][$d]['out'] = $this->getTgl($d, 'Out', $indicator_element2[$c],  $data);
            }
        }

        $data['record'] = $record;
        $data['lastDate'] = $lastDate;

        $this->template->load('template', $this->folder . '/viewManajemen2', $data);
    }

    function klinik($tglP = null)
    {
        if (isset($_POST['submit'])) {
            $tglP  =   $this->input->post('tglP');
        }

        if ($tglP == null) {
            $bulan = (date('m'));
            $tahun = (date('Y'));
        } else {
            $bulan = substr($tglP, 5, 2);
            $tahun = explode('-', $tglP)[0];
        }

        $bul = array(1 => "Januari", 2 => "Februari", 3 => "Maret", 4 => "April", 5 => "Mei", 6 => "Juni", 7 => "Juli", 8 => "Agustus", 9 => "September", 10 => "Oktober", 11 => "November", 12 => "Desember");
        if ($bulan <= 9) {
            $bl = substr($bulan, 1, 1);
        } else {
            $bl = $bulan;
        }
        $data['title'] =  "Indikator Mutu Harian Bulan " . $bul[$bl] . " " . $tahun;

        $indicator_id_user =  $this->session->userdata('hospital_survey_indicator');

        if ($indicator_id_user != null) {
            $data['record'] = $this->imut_m->imutManajemen("(indicator_category_id = 1 and indicator_id in ($indicator_id_user)) or (indicator_category_id = 1 and (MONTH(result_period) = $bulan and YEAR(result_period) = $tahun) and indicator_id in ($indicator_id_user))");

            // $data['record'] = $this->imut_m->imutManajemen("indicator_category_id = 1 or (indicator_category_id = 1 and (MONTH(result_period) = $bulan and YEAR(result_period) = $tahun))");
        } else if ($indicator_id_user == null) {
            $data['record'] = $this->imut_m->imutManajemen("indicator_category_id = 1 or (indicator_category_id = 1 and (MONTH(result_period) = $bulan and YEAR(result_period) = $tahun))");
        }

        $indicator_element2 = array(); //$nama2
        $indicator_id2 = array(); //$id2

        //mengambil indicator_element & indicator_id yang dobel
        for ($i = 1; $i <= count($data['record']); $i++) {
            if ($i == 1) {
                $indicator_element2[1] = $data['record'][0]->indicator_element;
                $indicator_id2[1] = $data['record'][0]->indicator_id;
                $k = 2;
            } else {
                if ($data['record'][$i - 1]->indicator_element != $indicator_element2[$k - 1]) {
                    $indicator_element2[$k] = $data['record'][$i - 1]->indicator_element;
                    $indicator_id2[$k] = $data['record'][$i - 1]->indicator_id;
                    $k++;
                }
            }
        }

        //get last day of month
        $lastDate = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
        if ($indicator_id_user == null) {
            $batas = count($indicator_element2);
        } else {
            $batas = $lastDate;
        }

        //looping tgl menyammping
        $record = array();
        for ($c = 1; $c <= count($indicator_element2); $c++) {
            for ($d = 1; $d <= $batas; $d++) { //(31) $data['sheet'] //count($indicator_element2)
                $record[$indicator_element2[$c]][$d]['nilai'] = $this->getNilai($d, $bulan, $tahun, $indicator_element2[$c],  $data);
                if ($record[$indicator_element2[$c]][$d]['nilai'] == null) {
                    $record[$indicator_element2[$c]][$d]['nilai'] = 0;
                }
                $record[$indicator_element2[$c]][$d]['tgl'] = $this->getTgl($d, $bulan, $tahun, $indicator_element2[$c],  $data);
                if ($record[$indicator_element2[$c]][$d]['tgl'] == null) {
                    if ($d <= 9) {
                        $record[$indicator_element2[$c]][$d]['tgl'] = $tahun . "-" . $bulan . "-0" . $d;
                    } else {
                        $record[$indicator_element2[$c]][$d]['tgl'] = $tahun . "-" . $bulan . "-" . $d;
                    }
                }
                $record[$indicator_element2[$c]][$d]['nama'] = $indicator_element2[$c];
                $record[$indicator_element2[$c]][$d]['id'] = $indicator_id2[$c];
            }
        }

        $data['record'] = $record;
        $data['lastDate'] = $lastDate;

        $this->template->load('template', $this->folder . '/viewKlinik2', $data);
    }

    function wajib($tglP = null)
    {
        if (isset($_POST['submit'])) {
            $tglP  =   $this->input->post('tglP');
        }

        if ($tglP == null) {
            $bulan = (date('m'));
            $tahun = (date('Y'));
        } else {
            $bulan = substr($tglP, 5, 2);
            $tahun = explode('-', $tglP)[0];
        }

        $bul = array(1 => "Januari", 2 => "Februari", 3 => "Maret", 4 => "April", 5 => "Mei", 6 => "Juni", 7 => "Juli", 8 => "Agustus", 9 => "September", 10 => "Oktober", 11 => "November", 12 => "Desember");
        if ($bulan <= 9) {
            $bl = substr($bulan, 1, 1);
        } else {
            $bl = $bulan;
        }
        $data['title'] =  "Indikator Mutu Harian Bulan " . $bul[$bl] . " " . $tahun;

        $indicator_id_user =  $this->session->userdata('hospital_survey_indicator');

        if ($indicator_id_user != null) {
            $data['record'] = $this->imut_m->imutManajemen("(indicator_category_id = 4 and indicator_id in ($indicator_id_user)) or (indicator_category_id = 4 and (MONTH(result_period) = $bulan and YEAR(result_period) = $tahun) and indicator_id in ($indicator_id_user))");
        } else if ($indicator_id_user == null) {
            $data['record'] = $this->imut_m->imutManajemen("indicator_category_id = 4 or (indicator_category_id = 4 and (MONTH(result_period) = $bulan and YEAR(result_period) = $tahun))");
        }

        // $data['record'] = $this->imut_m->imutManajemen("indicator_category_id = 4 or (indicator_category_id = 4 and (MONTH(result_period) = $bulan and YEAR(result_period) = $tahun))");

        $indicator_element2 = array(); //$nama2
        $indicator_id2 = array(); //$id2

        //mengambil indicator_element & indicator_id yang dobel
        for ($i = 1; $i <= count($data['record']); $i++) {
            if ($i == 1) {
                $indicator_element2[1] = $data['record'][0]->indicator_element;
                $indicator_id2[1] = $data['record'][0]->indicator_id;
                $k = 2;
            } else {
                if ($data['record'][$i - 1]->indicator_element != $indicator_element2[$k - 1]) {
                    $indicator_element2[$k] = $data['record'][$i - 1]->indicator_element;
                    $indicator_id2[$k] = $data['record'][$i - 1]->indicator_id;
                    $k++;
                }
            }
        }

        //get last day of month
        $lastDate = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

        //looping tgl menyammping
        $record = array();
        for ($c = 1; $c <= count($indicator_element2); $c++) {
            for ($d = 1; $d <= $lastDate; $d++) { //(31) $data['sheet']
                $record[$indicator_element2[$c]][$d]['nilai'] = $this->getNilai($d, $bulan, $tahun, $indicator_element2[$c],  $data);
                if ($record[$indicator_element2[$c]][$d]['nilai'] == null) {
                    $record[$indicator_element2[$c]][$d]['nilai'] = 0;
                }
                $record[$indicator_element2[$c]][$d]['tgl'] = $this->getTgl($d, $bulan, $tahun, $indicator_element2[$c],  $data);
                if ($record[$indicator_element2[$c]][$d]['tgl'] == null) {
                    if ($d <= 9) {
                        $record[$indicator_element2[$c]][$d]['tgl'] = $tahun . "-" . $bulan . "-0" . $d;
                    } else {
                        $record[$indicator_element2[$c]][$d]['tgl'] = $tahun . "-" . $bulan . "-" . $d;
                    }
                }
                $record[$indicator_element2[$c]][$d]['nama'] = $indicator_element2[$c];
                $record[$indicator_element2[$c]][$d]['id'] = $indicator_id2[$c];
            }
        }

        $data['record'] = $record;
        $data['lastDate'] = $lastDate;

        $this->template->load('template', $this->folder . '/viewWajib2', $data);
    }

    public function grafik_bulanan($result_indicator_id)
    {

        $year = $this->input->get('tahun');
        if ($year == null) {
            $year = date('Y');
        }
        $bulan = array('January', 'februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        $data['bulan'] = json_encode($bulan);
        //$year=date('Y');

        $data_rj = $this->imut_m->get_mutu_tahun($year, $result_indicator_id);
        // print_r($data_rj);
        // $data_rj = $this->rm_model->get_rj_tahun($year);
        // $data_ri = $this->rm_model->get_ri_tahun($year);
        $rj = array();
        // $ri = array();
        $warna = array();
        $border = array();
        for ($a = 0; $a < 12; $a++) {
            $rj[$a] = 0;
            //   $ri[$a] = 0;
            $warna[$a] = 'rgba(153, 102, 255, 0.2)';
            $border[$a] = 'rgba(153, 102, 255, 1)';
        }
        $total_rj = 0;
        // $total_ri = 0;
        foreach ($data_rj as $d) {
            $rj[($d->bulan) - 1] = $d->total;
            $total_rj += $d->total;
        }
        // foreach ($data_ri as $e) {
        //   $ri[($e->bulan) - 1] = $e->total;
        //   $total_ri += $e->total;
        // }

        $data['rj'] = json_encode($rj);
        // $data['ri'] = json_encode($ri);
        $data['warna'] = json_encode($warna);
        $data['border'] = json_encode($border);
        $data['total_rj'] = $total_rj;
        // $data['total_ri'] = $total_ri;
        $data['tahun'] = $year;
        $this->template->load('grafik_bulanan', $data);
    }

    function rekapNas($tglP = null)
    {
        if (isset($_POST['submit'])) {
            $tglP  =   $this->input->post('tglP');
        }

        if ($tglP == null) {
            // $bulan = (date('m'));
            $tahun = (date('Y'));
        } else {
            // $bulan = substr($tglP, 5, 2);
            $tahun = explode('-', $tglP)[0];
        }

        $data['title'] =  "Rekap IMUT Nasional :: Periode " . $tahun;

        //11
        $data['record'] = $this->imut_m->imutNas("(indicator_category_id in (1, 2, 4) and YEAR(result_period) = $tahun)");

        //sg
        $data['indicator_element11'] = $this->imut_m->imutNasSg("indicator_category_id in (1, 2, 4)  or (indicator_category_id in (1, 2, 4) and YEAR(result_period) = $tahun)");

        $temp = $data['indicator_element11'];
        $indicator_element11 = array();
        $indicator_id11 = array();

        for ($i = 0; $i < count($temp); $i++) {
            $indicator_element11[$i] = $temp[$i]->indicator_element;
            $indicator_id11[$i] = $temp[$i]->indicator_id;
        }


        //get last day of month
        // $lastDate = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

        //looping tgl menyammping 11
        $record = array();
        for ($c = 1; $c <= count($indicator_element11); $c++) {
            for ($d = 1; $d <= count($indicator_element11); $d++) { //count($indicator_element2)  12    (31) $data['sheet']
                $record[$indicator_element11[$c - 1]][$d]['nilai'] = $this->getNilaiRkp($d,  $tahun, $indicator_element11[$c - 1],  $data); //$bulan,
                if ($record[$indicator_element11[$c - 1]][$d]['nilai'] == null) {
                    $record[$indicator_element11[$c - 1]][$d]['nilai'] = 0;
                }

                $record[$indicator_element11[$c - 1]][$d]['tgl'] = $this->getTglRkp($d, $tahun, $indicator_element11[$c - 1],  $data); //$bulan,
                if ($record[$indicator_element11[$c - 1]][$d]['tgl'] == null) {

                    $record[$indicator_element11[$c - 1]][$d]['tgl'] = $tahun . "-" . $d;
                }
                $record[$indicator_element11[$c - 1]][$d]['nama'] = $indicator_element11[$c - 1];
                $record[$indicator_element11[$c - 1]][$d]['id'] = $indicator_id11[$c - 1];
            }
        }

        $data['record'] = $record;
        // $data['lastDate'] = $lastDate;

        $this->template->load('template', $this->folder . '/viewRkpNas2', $data);
    }

    function rekapLokal($tglP = null)
    {
        if (isset($_POST['submit'])) {
            $tglP  =   $this->input->post('tglP');
        }

        if ($tglP == null) {
            // $bulan = (date('m'));
            $tahun = (date('Y'));
        } else {
            // $bulan = substr($tglP, 5, 2);
            $tahun = explode('-', $tglP)[0];
        }

        $data['title'] =  "Rekap IMUT Lokal :: Periode " . $tahun;

        //11
        $data['record'] = $this->imut_m->imutLokal("(indicator_category_id in (1, 2, 4) and YEAR(result_period) = $tahun)");

        //sg
        $data['indicator_element11'] = $this->imut_m->imutLokalSg("indicator_category_id in (1, 2, 4)  or (indicator_category_id in (1, 2, 4) and YEAR(result_period) = $tahun)");

        $temp = $data['indicator_element11'];
        $indicator_element11 = array();
        $indicator_id11 = array();

        for ($i = 0; $i < count($temp); $i++) {
            $indicator_element11[$i] = $temp[$i]->indicator_element;
            $indicator_id11[$i] = $temp[$i]->indicator_id;
        }


        //get last day of month
        // $lastDate = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);

        //looping tgl menyammping 11
        $record = array();
        for ($c = 1; $c <= count($indicator_element11); $c++) {
            for ($d = 1; $d <= count($indicator_element11); $d++) { //count($indicator_element2)  12    (31) $data['sheet']
                $record[$indicator_element11[$c - 1]][$d]['nilai'] = $this->getNilaiRkp($d,  $tahun, $indicator_element11[$c - 1],  $data); //$bulan,
                if ($record[$indicator_element11[$c - 1]][$d]['nilai'] == null) {
                    $record[$indicator_element11[$c - 1]][$d]['nilai'] = 0;
                }

                $record[$indicator_element11[$c - 1]][$d]['tgl'] = $this->getTglRkp($d, $tahun, $indicator_element11[$c - 1],  $data); //$bulan,
                if ($record[$indicator_element11[$c - 1]][$d]['tgl'] == null) {
                    // if ($d <= 9) {
                    // $record[$indicator_element2[$c]][$d]['tgl'] = $tahun . "-0" . $d;
                    // } else {
                    $record[$indicator_element11[$c - 1]][$d]['tgl'] = $tahun . "-" . $d;
                    // $record[$indicator_element2[$c]][$d]['tgl'] = $tahun . "-" . $bulan . "-" . $d;
                    // }
                }
                $record[$indicator_element11[$c - 1]][$d]['nama'] = $indicator_element11[$c - 1];
                $record[$indicator_element11[$c - 1]][$d]['id'] = $indicator_id11[$c - 1];
            }
        }

        $data['record'] = $record;
        // $data['lastDate'] = $lastDate;

        // print_r($data['record']);
        $this->template->load('template', $this->folder . '/viewRkpLokal2', $data);
    }

    public function getNilai($d, $bulan, $tahun, $indicator_element2c, $data)
    {
        //semangat
        foreach ($data['record'] as $s) {
            // print_r("//samakan nama ");
            // print_r($indicator_element2c);
            // print_r(" == ");
            // print_r($s->indicator_element);
            // print_r("....
            // ");

            if ($indicator_element2c == $s->indicator_element) { //samakan nama //$data['sheet'][$c]['E']
                $thn =  explode('-', $s->result_period)[0]; //mengambil tahun
                $tgl =  substr($s->result_period, 8); // mengambil tgl
                $bln =  substr($s->result_period, 5, 2); // mengambil bulan
                // print_r("tgl ");
                // print_r(($tgl));
                // print_r(($bln));
                // print_r(($thn));

                // print_r("//samakan tgl ");
                // print_r($d);
                // print_r(" == ");
                // print_r($tgl);
                // print_r("//bln");
                // print_r($bln);
                // print_r("//thn");
                // print_r($thn);
                // print_r("****");

                if ($d == $tgl && $bulan == $bln && $tahun == $thn) { //samakan tgl	&& $bulan == $bln && $tahun == $thn 	 data['sheet'][$p]['E']	
                    // if ($tipe == $s['H']) { //samakan in out  $data['sheet'][$p]['H']	
                    // print_r($s['G']); //$data['sheet'][$p]['G']
                    $p = 1.00;
                    $num = $s->result_numerator_value;
                    $denum = $s->result_denumerator_value;
                    $p = number_format(100, 2);
                    $temp = 0;
                    if ($denum == 0 or $denum == null or $num == 0 or $num == null) {
                        $temp = 0 * $p;
                    } else {
                        $temp = (int) $num / (int) $denum * $p;
                    }

                    $nilai = number_format($temp, 2);

                    // print_r("(number_format((s->result_numerator_value) = ");
                    // print_r((number_format($num, 2)));
                    // print_r("(number_format(s->result_denumerator_value) = ");
                    // print_r((number_format($denum, 2)));
                    // print_r("
                    // ++");

                    // print_r("dd :");
                    // print_r($d);
                    if ($s->result_numerator_value != NULL or $s->result_denumerator_value != NULL) {
                        if ($s->result_numerator_value == 0 or $s->result_denumerator_value == 0) {
                            return "0.00%";
                        } else {
                            return ($nilai . '%');
                        }
                    } else {
                        return '0';
                    }
                }
            }
        }
    }

    public function getNilaiRkp($d, $tahun, $indicator_element2c, $data)
    {
        //semangat
        foreach ($data['record'] as $s) {
            // print_r("//samakan nama ");
            // print_r($indicator_element2c);
            // print_r(" == ");
            // print_r($s->indicator_element);
            // print_r("....
            // ");

            if ($indicator_element2c == $s->indicator_element) { //samakan nama //$data['sheet'][$c]['E']
                $thn =  explode('-', $s->result_period)[0]; //mengambil tahun
                // $tgl =  substr($s->result_period, 8); // mengambil tgl
                $bln =  substr($s->result_period, 5, 2); // mengambil bulan
                // print_r("tgl ");
                // print_r(($tgl));
                // print_r(($bln));
                // print_r(($thn));

                // print_r("//samakan tgl ");
                // print_r($d);
                // print_r(" == ");
                // print_r($tgl);
                // print_r("//bln");
                // print_r($bln);
                // print_r("//thn");
                // print_r($thn);
                // print_r("****");

                if ($d == $bln && $tahun == $thn) { //samakan tgl	$d == $tgl &&     && $bulan == $bln && $tahun == $thn 	 
                    $p = 1.00;
                    $num = $s->result_numerator_value;
                    $denum = $s->result_denumerator_value;
                    $p = number_format(100, 2);
                    $temp = 0;
                    if ($denum == 0 or $denum == null or $num == 0 or $num == null) {
                        $temp = 0 * $p;
                    } else {
                        $temp = (int) $num / (int) $denum * $p;
                    }

                    $nilai = number_format($temp, 2);

                    // print_r("(number_format((s->result_numerator_value) = ");
                    // print_r((number_format($num, 2)));
                    // print_r("(number_format(s->result_denumerator_value) = ");
                    // print_r((number_format($denum, 2)));
                    // print_r("
                    // ++");

                    // print_r("dd :");
                    // print_r($d);
                    if ($s->result_numerator_value != NULL or $s->result_denumerator_value != NULL) {
                        if ($s->result_numerator_value == 0 or $s->result_denumerator_value == 0) {
                            return "0.00%";
                        } else {
                            return ($nilai . '%');
                        }
                    } else {
                        return '0';
                    }
                }
            }
        }
    }

    public function getTglRkp($d, $tahun, $indicator_element2c, $data)
    {
        //semangat
        foreach ($data['record'] as $s) {
            // if ($indicator_element2c == $s->indicator_element) { //samakan nama //$data['sheet'][$c]['E']
            $thn =  explode('-', $s->result_period)[0]; //mengambil tahun
            // $tgl =  substr($s->result_period, 8); // mengambil tgl
            $bln =  substr($s->result_period, 5, 2); // mengambil bulan
            if ($d == $bln && $tahun == $thn) { //samakan tgl$d == $tgl &&  	&& $bulan == $bln && $tahun == $thn 	 data['sheet'][$p]['E']	
                // if ($d <= 9) {
                $temp = $thn . '-' . $bln; // . '-0' . $d;
                // } else {
                //     $temp = $thn . '-' . $bln . '-' . $d;
                // }
                return $temp;
            }
            // }
        }
    }

    public function getTgl($d, $bulan, $tahun, $indicator_element2c, $data)
    {
        //semangat
        foreach ($data['record'] as $s) {
            // if ($indicator_element2c == $s->indicator_element) { //samakan nama //$data['sheet'][$c]['E']
            $thn =  explode('-', $s->result_period)[0]; //mengambil tahun
            $tgl =  substr($s->result_period, 8); // mengambil tgl
            $bln =  substr($s->result_period, 5, 2); // mengambil bulan
            if ($d == $tgl && $bulan == $bln && $tahun == $thn) { //samakan tgl	&& $bulan == $bln && $tahun == $thn 	 data['sheet'][$p]['E']	
                if ($d <= 9) {
                    $temp = $thn . '-' . $bln . '-0' . $d;
                } else {
                    $temp = $thn . '-' . $bln . '-' . $d;
                }
                return $temp;
            }
            // }
        }
    }

    function tampilDetail($indikator_id, $typeImut, $tabel)
    {
        $data['id'] =  $indikator_id;
        $data['up'] =  $typeImut;
        $data['title'] =  "Daftar Indikator";
        $data['record'] = $this->imut_m->detailIndikator(array('indicator_id' => $indikator_id), $tabel); //bisa1
        $this->template->load('template', $this->folder . '/viewDetail', $data);
    }

    function tampilDetailLokal($indikator_id, $typeImut, $tabel)
    {
        $data['id'] =  $indikator_id;
        $data['up'] =  $typeImut;
        $data['title'] =  "Daftar Indikator";
        $data['record'] = $this->imut_m->detailIndikator(array('indicator_id' => $indikator_id), $tabel); //bisa1
        $this->template->load('template', $this->folder . '/viewDetailLokal', $data);
    }

    function daftarImutLokal()
    {
        $typeImut = 'imutLokal';
        $tabel = 'hospital_survey_indicator_for_hospital';

        // $data['id'] =  $indikator_id;
        $data['up'] =  $typeImut;
        $data['title'] =  "Daftar Indikator (Versi Lokal)";

        $indicator_id_user =  $this->session->userdata('hospital_survey_indicator_lkl');
        if ($indicator_id_user != null) {
            $data['record'] = $this->imut_m->detailIndikator("indicator_id in ($indicator_id_user)", $tabel);
        } else if ($indicator_id_user == null) {
            $data['record'] = $this->imut_m->detailIndikator('', $tabel);
        }

        // $data['record'] = $this->imut_m->detailIndikator('', $tabel); //bisa1
        $this->template->load('template', $this->folder . '/viewDetailLokal', $data);
    }

    function editDetailIndicator($indicator_id, $typeImut)
    {
        if (isset($_POST['submit'])) {
            $indicator_category_id  =   $this->input->post('indicator_category_id');
            $indicator_element  =   $this->input->post('indicator_element');
            $indicator_definition     =   $this->input->post('indicator_definition');
            $indicator_criteria_inclusive     =   $this->input->post('indicator_criteria_inclusive');
            $indicator_criteria_exclusive     =   $this->input->post('indicator_criteria_exclusive');
            $indicator_source_of_data     =   $this->input->post('indicator_source_of_data');
            $indicator_type     =   $this->input->post('indicator_type');
            $indicator_monitoring_area     =   $this->input->post('indicator_monitoring_area');
            $indicator_frequency     =   $this->input->post('indicator_frequency');
            $indicator_target     =   $this->input->post('indicator_target');


            $id     = $this->input->post('id');
            // $data   =   array('username'=>$username,'password'=>md5($password));
            $data   =   array(
                'indicator_category_id' => $indicator_category_id, 'indicator_element' => ($indicator_element), 'indicator_definition' => $indicator_definition, 'indicator_criteria_inclusive' => $indicator_criteria_inclusive, 'indicator_criteria_exclusive' => $indicator_criteria_exclusive,

                'indicator_source_of_data' => $indicator_source_of_data, 'indicator_type' => ($indicator_type), 'indicator_monitoring_area' => $indicator_monitoring_area, 'indicator_frequency' => $indicator_frequency, 'indicator_target' => $indicator_target
            );

            $this->mcrud->update('hospital_survey_indicator_for_hospital', $data, 'indicator_id', $id);
            // redirect($this->uri->segment(1));
        }
        // else
        // {
        $data['up'] =  $typeImut;
        $data['title'] =  "Edit Indikator";
        $id          =  $this->uri->segment(3);
        $data['r']   =  $this->mcrud->getByID('hospital_survey_indicator_for_hospital',  'indicator_id', $id)->row_array();
        $this->template->load('template', $this->folder . '/editDetailIndicator', $data);
        // }
    }

    function tambahImutLokal($typeImut = 'imutLokal')
    {
        if (isset($_POST['submit'])) {
            $indicator_category_id  =   $this->input->post('indicator_category_id');
            $indicator_element  =   $this->input->post('indicator_element');
            $indicator_definition     =   $this->input->post('indicator_definition');
            $indicator_criteria_inclusive     =   $this->input->post('indicator_criteria_inclusive');
            $indicator_criteria_exclusive     =   $this->input->post('indicator_criteria_exclusive');
            $indicator_source_of_data     =   $this->input->post('indicator_source_of_data');
            $indicator_type     =   $this->input->post('indicator_type');
            $indicator_monitoring_area     =   $this->input->post('indicator_monitoring_area');
            $indicator_frequency     =   $this->input->post('indicator_frequency');
            $indicator_target     =   $this->input->post('indicator_target');


            // $id     = $this->input->post('id');
            // $data   =   array('username'=>$username,'password'=>md5($password));
            $data   =   array(
                'indicator_category_id' => $indicator_category_id, 'indicator_element' => ($indicator_element), 'indicator_definition' => $indicator_definition, 'indicator_criteria_inclusive' => $indicator_criteria_inclusive, 'indicator_criteria_exclusive' => $indicator_criteria_exclusive,

                'indicator_source_of_data' => $indicator_source_of_data, 'indicator_type' => ($indicator_type), 'indicator_monitoring_area' => $indicator_monitoring_area, 'indicator_frequency' => $indicator_frequency, 'indicator_target' => $indicator_target
            );

            $this->mcrud->insert('hospital_survey_indicator_for_hospital', $data);
            // redirect($this->uri->segment(1));

            $typeImut = 'imutLokal';
            $tabel = 'hospital_survey_indicator_for_hospital';

            // $data['id'] =  $indikator_id;
            $data['up'] =  $typeImut;
            $data['title'] =  "Daftar Indikator (Versi Lokal)";
            $data['record'] = $this->imut_m->detailIndikator('', $tabel); //bisa1
            $this->template->load('template', $this->folder . '/viewDetailLokal', $data);
        } else {
            $data['up'] =  $typeImut;
            $data['title'] =  "Tambah Indikator Lokal";
            $this->template->load('template', $this->folder . '/tambahIndicator', $data);
        }
    }

    function deleteImutLokal($indicator_id)
    {
        print_r($indicator_id);
        // $id     =  $this->uri->segment(3);
        $chekid = $this->db->get_where('hospital_survey_indicator_for_hospital', array('indicator_id' => $indicator_id));
        print_r($chekid);
        if ($chekid->num_rows() > 0) {
            print_r('s , ');
            $this->mcrud->delete('hospital_survey_indicator_for_hospital',  'indicator_id',  $indicator_id);
            print_r('gg , ');
        }
        redirect(base_url('imut/daftarImutLokal'));
    }

    function inputMutu($variable_indicator_id, $result_period, $typeImut, $nol = null)
    {
        if (isset($_POST['submit'])) {

            $result_numerator_value  =   $this->input->post('result_value1');
            $result_denumerator_value  =   $this->input->post('result_value2');

            // if($level==2)
            // {
            $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"));
            // }
            // else
            // {
            //      $data   =   array('username'=>$username,'password'=>md5($password),'level'=>$level);
            // }

            $result_indicator_id     = $this->input->post('id');
            $result_period     = $this->input->post('id2');

            $cek = $this->imut_m->cekResult(array('result_indicator_id' => $result_indicator_id, 'result_period' => $result_period));

            if ($cek['jumlah'] <= 0 or $cek['jumlah'] == null) {
                $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"), 'result_indicator_id' => $result_indicator_id, 'result_period' => $result_period);

                $status = $this->mcrud->insert('hospital_survey_indicator_result', $data);
            } else if ($cek['jumlah'] == 1) {
                $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"));
                //update hospital_survey_indicator_result
                $status = $this->mcrud->update2('hospital_survey_indicator_result', $data, 'result_indicator_id', $result_indicator_id, 'result_period', $result_period);
            }



            // if ($nol != null) {

            //     $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"), 'result_indicator_id' => $result_indicator_id, 'result_period' => $result_period);

            //     $status = $this->mcrud->insert('hospital_survey_indicator_result', $data);
            // }

            // print_r($this->db->affected_rows());
            // if ($this->db->affected_rows() >= 1 and $nol != null) {
            $thn =  explode('-', $result_period)[0]; //mengambil tahun
            // $tgl =  substr($result_period, 8); // mengambil tgl
            $bln =  substr($result_period, 5, 2); // mengambil bulan
            $awalBulan = "'" . $thn . "-" . $bln . "-01'";
            $awalBulan2 =  $thn . "-" . $bln . "-01";
            $thnBulan = "'" . $thn . "-" . $bln . "-%'";
            // print_r($awalBulan);





            // print_r($this->db->affected_rows()); // $this->db->affected_rows() > 0
            // if ($nol != null) {
            $cekR = $this->imut_m->cekRekap(array('result_indicator_id' => $result_indicator_id, 'result_period' => $awalBulan2));

            print_r('cekR[OjumlahO] : ');
            print_r($cekR['jumlah']); //4

            // if ($cekR['jumlah'] <= 1 or $cekR['jumlah'] == null) {
            if ($cekR['jumlah'] > 1 or $cekR['jumlah'] == 0) {
                print_r('Sebelum deleteR,');
                $delR = $this->mcrud->delete2('hospital_survey_indicator_result_recapitulation', 'result_indicator_id', $result_indicator_id, 'result_period', $awalBulan2);
                print_r('setelah deleteR');

                print_r(' sebelum createR,');
                $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"), 'result_indicator_id' => $result_indicator_id, 'result_period' => $awalBulan2);
                $status = $this->mcrud->insert('hospital_survey_indicator_result_recapitulation', $data);
                print_r(' setelah createR,');

                $sumNum = $this->imut_m->getSumNum("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);
                $sumDenum = $this->imut_m->getSumDenum("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);

                print_r(' sebelum updateR,');
                $data   =   array('result_numerator_value' => $sumNum['jumlah'], 'result_denumerator_value' => $sumDenum['jumlah'], 'result_post_date' => (date("Y-m-d H:i:s")), 'result_period' => $awalBulan2);

                //update hospital_survey_indicator_result_recapitulation
                $this->mcrud->update2('hospital_survey_indicator_result_recapitulation', $data, 'result_indicator_id', $result_indicator_id, 'result_period', $awalBulan2);
                print_r(' setelah updateR,');
            } else if ($cekR['jumlah'] == 1) {
                $sumNum = $this->imut_m->getSumNum("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);
                $sumDenum = $this->imut_m->getSumDenum("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);

                print_r('sebelum updateR');
                $data   =   array('result_numerator_value' => $sumNum['jumlah'], 'result_denumerator_value' => $sumDenum['jumlah'], 'result_post_date' => (date("Y-m-d H:i:s")), 'result_period' => $awalBulan2);

                //update hospital_survey_indicator_result_recapitulation
                $this->mcrud->update2('hospital_survey_indicator_result_recapitulation', $data, 'result_indicator_id', $result_indicator_id, 'result_period', $awalBulan2);
                print_r('setelah updateR');
            }
            // // print_r('cekNol');
            // $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"), 'result_indicator_id' => $result_indicator_id, 'result_period' => $awalBulan);

            // $status = $this->mcrud->insert('hospital_survey_indicator_result_recapitulation', $data);
            // } else if ($cekR['jumlah'] >= 2) {



            // }
            // }
        }

        $data2['id'] =  $variable_indicator_id;
        $data2['result_period'] =  $result_period;
        $data2['up'] =  $typeImut;
        $data2['title'] =  "Pendataan Indikator";
        $data2['record'] = $this->imut_m->inputIndikator(
            array(
                'variable_indicator_id' => $variable_indicator_id,
                'result_period' => $result_period
            )
        );

        // print_r($data2['record']);
        $this->template->load('template', $this->folder . '/viewInputMutu', $data2);
    }

    function inputMutuLokal($variable_indicator_id, $result_period, $typeImut, $nol = null)
    {
        if (isset($_POST['submit'])) {

            $result_numerator_value  =   $this->input->post('result_value1');
            $result_denumerator_value  =   $this->input->post('result_value2');

            // if($level==2)
            // {
            $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"));
            // }
            // else
            // {
            //      $data   =   array('username'=>$username,'password'=>md5($password),'level'=>$level);
            // }

            $result_indicator_id     = $this->input->post('id');
            $result_period     = $this->input->post('id2');

            $cek = $this->imut_m->cekResultLokal(array('result_indicator_id' => $result_indicator_id, 'result_period' => $result_period));

            if ($cek['jumlah'] <= 0 or $cek['jumlah'] == null) {
                $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"), 'result_indicator_id' => $result_indicator_id, 'result_period' => $result_period);

                $status = $this->mcrud->insert('hospital_survey_indicator_result_for_hospital', $data);
            } else if ($cek['jumlah'] == 1) {
                $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"));
                //update hospital_survey_indicator_result
                $status = $this->mcrud->update2('hospital_survey_indicator_result_for_hospital', $data, 'result_indicator_id', $result_indicator_id, 'result_period', $result_period);
            }



            // if ($nol != null) {

            //     $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"), 'result_indicator_id' => $result_indicator_id, 'result_period' => $result_period);

            //     $status = $this->mcrud->insert('hospital_survey_indicator_result', $data);
            // }

            // print_r($this->db->affected_rows());
            // if ($this->db->affected_rows() >= 1 and $nol != null) {
            $thn =  explode('-', $result_period)[0]; //mengambil tahun
            // $tgl =  substr($result_period, 8); // mengambil tgl
            $bln =  substr($result_period, 5, 2); // mengambil bulan
            $awalBulan = "'" . $thn . "-" . $bln . "-01'";
            $awalBulan2 =  $thn . "-" . $bln . "-01";
            $thnBulan = "'" . $thn . "-" . $bln . "-%'";
            // print_r($awalBulan);

            $sumNum = $this->imut_m->getSumNumLokal("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);
            $sumDenum = $this->imut_m->getSumDenumLokal("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);



            // print_r($this->db->affected_rows()); // $this->db->affected_rows() > 0
            // if ($nol != null) {
            $cekR = $this->imut_m->cekRekapLokal(array('result_indicator_id' => $result_indicator_id, 'result_period' => $awalBulan2));

            // print_r('cekR[jumlah] : ');
            // print_r($cekR);

            // if ($cekR['jumlah'] <= 1 or $cekR['jumlah'] == null) {
            // // print_r('cekNol');
            // $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"), 'result_indicator_id' => $result_indicator_id, 'result_period' => $awalBulan);

            // $status = $this->mcrud->insert('hospital_survey_indicator_result_recapitulation', $data);
            // } else if ($cekR['jumlah'] >= 2) {
            $data   =   array('result_numerator_value' => $sumNum['jumlah'], 'result_denumerator_value' => $sumDenum['jumlah'], 'result_post_date' => (date("Y-m-d H:i:s")), 'result_period' => $awalBulan2);

            //update hospital_survey_indicator_result_recapitulation
            $this->mcrud->update2('hospital_survey_indicator_result_recapitulation_for_hospital', $data, 'result_indicator_id', $result_indicator_id, 'result_period', $awalBulan2);
            // }

            // }
            // }
        }

        $data2['id'] =  $variable_indicator_id;
        $data2['result_period'] =  $result_period;
        $data2['up'] =  $typeImut;
        $data2['title'] =  "Pendataan Imut (Versi Lokal) Tgl " . $result_period;
        $data2['record'] = $this->imut_m->inputIndikatorLokal(
            array(
                'variable_indicator_id' => $variable_indicator_id,
                'result_period' => $result_period
            )
        );

        // print_r($data2['record']);
        $this->template->load('template', $this->folder . '/viewInputMutuLokal', $data2);
    }

    function inputMutuNol($variable_indicator_id, $result_period, $typeImut)
    {
        $data['id'] =  $variable_indicator_id;
        $data['result_period'] =  $result_period;
        $data['up'] =  $typeImut;
        $data['title'] =  "Pendataan Indikator";
        $data['record'] = $this->imut_m->inputIndikatorNol(
            array(
                'variable_indicator_id' => $variable_indicator_id,
                // 'result_period' => $result_period
            )
        ); //bisa1

        // $data['record'] = $this->imut_m->inputIndikator(
        //     $variable_indicator_id, $result_period            
        // );

        // print_r($data['record']);
        $this->template->load('template', $this->folder . '/viewInputMutuNol', $data);
    }

    function inputMutuLokalNol($variable_indicator_id, $result_period, $typeImut)
    {
        $data['id'] =  $variable_indicator_id;
        $data['result_period'] =  $result_period;
        $data['up'] =  $typeImut;
        $data['title'] =  "Pendataan Imut (Versi Lokal) Tgl " . $result_period;
        $data['record'] = $this->imut_m->inputIndikatorNol(
            array(
                'variable_indicator_id' => $variable_indicator_id,
                // 'result_period' => $result_period
            )
        );
        // print_r($data['record']);
        $this->template->load('template', $this->folder . '/viewInputMutuLokalNol', $data);
    }

    function saveMutuNol($variable_indicator_id, $result_period, $typeImut)
    {

        if (isset($_POST['submit'])) {

            $result_numerator_value  =   $this->input->post('result_value1');
            $result_denumerator_value  =   $this->input->post('result_value2');

            // if($level==2)
            // {
            $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"));
            // }
            // else
            // {
            //      $data   =   array('username'=>$username,'password'=>md5($password),'level'=>$level);
            // }

            $result_indicator_id     = $this->input->post('id');
            $result_period     = $this->input->post('id2');

            $cek = $this->imut_m->cekResult(array('result_indicator_id' => $result_indicator_id, 'result_period' => $result_period));

            if ($cek['jumlah'] <= 0 or $cek['jumlah'] == null) {
                $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"), 'result_indicator_id' => $result_indicator_id, 'result_period' => $result_period);

                $status = $this->mcrud->insert('hospital_survey_indicator_result', $data);
            } else if ($cek['jumlah'] == 1) {
                $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"));
                //update hospital_survey_indicator_result
                $status = $this->mcrud->update2('hospital_survey_indicator_result', $data, 'result_indicator_id', $result_indicator_id, 'result_period', $result_period);
            }

            // print_r($this->db->affected_rows());
            // if ($this->db->affected_rows() >= 1 and $nol != null) {
            $thn =  explode('-', $result_period)[0]; //mengambil tahun
            // $tgl =  substr($result_period, 8); // mengambil tgl
            $bln =  substr($result_period, 5, 2); // mengambil bulan
            $awalBulan = "'" . $thn . "-" . $bln . "-01'";
            $awalBulan2 =  $thn . "-" . $bln . "-01";
            $thnBulan = "'" . $thn . "-" . $bln . "-%'";

            $sumNum = $this->imut_m->getSumNum("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);
            $sumDenum = $this->imut_m->getSumDenum("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);


            // print_r($this->db->affected_rows()); // $this->db->affected_rows() > 0
            // if ($nol != null) {
            $cekR = $this->imut_m->cekRekap(array('result_indicator_id' => $result_indicator_id, 'result_period' => $awalBulan2)); //$awalBulan2

            // print_r('cekR[jumlah] : ');
            // print_r($cekR);


            if ($cekR['jumlah'] > 1 or $cekR['jumlah'] == 0) {
                print_r('Sebelum deleteR0,');
                $delR = $this->mcrud->delete2('hospital_survey_indicator_result_recapitulation', 'result_indicator_id', $result_indicator_id, 'result_period', $awalBulan2);
                print_r('setelah deleteR0');

                print_r(' sebelum createR0,');
                $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"), 'result_indicator_id' => $result_indicator_id, 'result_period' => $awalBulan2);
                $status = $this->mcrud->insert('hospital_survey_indicator_result_recapitulation', $data);
                print_r(' setelah createR0,');

                $sumNum = $this->imut_m->getSumNum("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);
                $sumDenum = $this->imut_m->getSumDenum("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);

                print_r(' sebelum updateR0,');
                $data   =   array('result_numerator_value' => $sumNum['jumlah'], 'result_denumerator_value' => $sumDenum['jumlah'], 'result_post_date' => (date("Y-m-d H:i:s")), 'result_period' => $awalBulan2);

                //update hospital_survey_indicator_result_recapitulation
                $this->mcrud->update2('hospital_survey_indicator_result_recapitulation', $data, 'result_indicator_id', $result_indicator_id, 'result_period', $awalBulan2);
                print_r(' setelah updateR0,');
            } else if ($cekR['jumlah'] == 1) {
                $sumNum = $this->imut_m->getSumNum("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);
                $sumDenum = $this->imut_m->getSumDenum("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);

                print_r('sebelum updateR0');
                $data   =   array('result_numerator_value' => $sumNum['jumlah'], 'result_denumerator_value' => $sumDenum['jumlah'], 'result_post_date' => (date("Y-m-d H:i:s")), 'result_period' => $awalBulan2);

                //update hospital_survey_indicator_result_recapitulation
                $this->mcrud->update2('hospital_survey_indicator_result_recapitulation', $data, 'result_indicator_id', $result_indicator_id, 'result_period', $awalBulan2);
                print_r('setelah updateR0');
            }
        }

        $data['id'] =  $variable_indicator_id;
        $data['result_period'] =  $result_period;
        $data['up'] =  $typeImut;
        $data['title'] =  "Pendataan Indikator";
        $data['record'] = $this->imut_m->inputIndikator(
            array(
                'variable_indicator_id' => $variable_indicator_id, 'result_period' => $result_period
            )
        ); //bisa1

        $this->template->load('template', $this->folder . '/viewInputMutu', $data);
    }

    function saveMutuLokalNol($variable_indicator_id, $result_period, $typeImut)
    {

        if (isset($_POST['submit'])) {

            $result_numerator_value  =   $this->input->post('result_value1');
            $result_denumerator_value  =   $this->input->post('result_value2');

            // if($level==2)
            // {
            $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"));
            // }
            // else
            // {
            //      $data   =   array('username'=>$username,'password'=>md5($password),'level'=>$level);
            // }

            $result_indicator_id     = $this->input->post('id');
            $result_period     = $this->input->post('id2');

            $cek = $this->imut_m->cekResultLokal(array('result_indicator_id' => $result_indicator_id, 'result_period' => $result_period));

            if ($cek['jumlah'] <= 0 or $cek['jumlah'] == null) {
                $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"), 'result_indicator_id' => $result_indicator_id, 'result_period' => $result_period);

                $status = $this->mcrud->insert('hospital_survey_indicator_result_for_hospital', $data);
            } else if ($cek['jumlah'] == 1) {
                $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"));
                //update hospital_survey_indicator_result
                $status = $this->mcrud->update2('hospital_survey_indicator_result_for_hospital', $data, 'result_indicator_id', $result_indicator_id, 'result_period', $result_period);
            }

            // print_r($this->db->affected_rows());
            // if ($this->db->affected_rows() >= 1 and $nol != null) {
            $thn =  explode('-', $result_period)[0]; //mengambil tahun
            // $tgl =  substr($result_period, 8); // mengambil tgl
            $bln =  substr($result_period, 5, 2); // mengambil bulan
            $awalBulan = "'" . $thn . "-" . $bln . "-01'";
            $awalBulan2 =  $thn . "-" . $bln . "-01";
            $thnBulan = "'" . $thn . "-" . $bln . "-%'";

            $sumNum = $this->imut_m->getSumNumLokal("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);
            $sumDenum = $this->imut_m->getSumDenumLokal("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);


            // print_r($this->db->affected_rows()); // $this->db->affected_rows() > 0
            // if ($nol != null) {
            $cekR = $this->imut_m->cekRekapLokal(array('result_indicator_id' => $result_indicator_id, 'result_period' => $awalBulan2)); //$awalBulan2

            // print_r('cekR[jumlah] : ');
            // print_r($cekR);

            if ($cekR['jumlah'] > 1 or $cekR['jumlah'] == 0) {
                print_r('Sebelum deleteR0,');
                $delR = $this->mcrud->delete2('hospital_survey_indicator_result_recapitulation_for_hospital', 'result_indicator_id', $result_indicator_id, 'result_period', $awalBulan2);
                print_r('setelah deleteR0L');

                print_r(' sebelum createR0L,');
                $data   =   array('result_numerator_value' => $result_numerator_value, 'result_denumerator_value' => ($result_denumerator_value), 'result_post_date' => date("Y-m-d H:i:s"), 'result_indicator_id' => $result_indicator_id, 'result_period' => $awalBulan2);
                $status = $this->mcrud->insert('hospital_survey_indicator_result_recapitulation_for_hospital', $data);
                print_r(' setelah createR0L,');

                $sumNum = $this->imut_m->getSumNumLokal("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);
                $sumDenum = $this->imut_m->getSumDenumLokal("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);

                print_r(' sebelum updateR0L,');
                $data   =   array('result_numerator_value' => $sumNum['jumlah'], 'result_denumerator_value' => $sumDenum['jumlah'], 'result_post_date' => (date("Y-m-d H:i:s")), 'result_period' => $awalBulan2);

                //update hospital_survey_indicator_result_recapitulation
                $this->mcrud->update2('hospital_survey_indicator_result_recapitulation_for_hospital', $data, 'result_indicator_id', $result_indicator_id, 'result_period', $awalBulan2);
                print_r(' setelah updateR0,');
            } else if ($cekR['jumlah'] == 1) {
                $sumNum = $this->imut_m->getSumNumLokal("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);
                $sumDenum = $this->imut_m->getSumDenumLokal("result_indicator_id = " . $result_indicator_id . " and result_period like " . $thnBulan);

                print_r('sebelum updateR0L');
                $data   =   array('result_numerator_value' => $sumNum['jumlah'], 'result_denumerator_value' => $sumDenum['jumlah'], 'result_post_date' => (date("Y-m-d H:i:s")), 'result_period' => $awalBulan2);

                //update hospital_survey_indicator_result_recapitulation
                $this->mcrud->update2('hospital_survey_indicator_result_recapitulation_for_hospital', $data, 'result_indicator_id', $result_indicator_id, 'result_period', $awalBulan2);
                print_r('setelah updateR0L');
            }
        }

        $data['id'] =  $variable_indicator_id;
        $data['result_period'] =  $result_period;
        $data['up'] =  $typeImut;
        $data['title'] =  "Pendataan Imut (Versi Lokal) Tgl " . $result_period;
        $data['record'] = $this->imut_m->inputIndikatorLokal(
            array(
                'variable_indicator_id' => $variable_indicator_id, 'result_period' => $result_period
            )
        ); //bisa1

        $this->template->load('template', $this->folder . '/viewInputMutuLokal', $data);
    }

    function imutLokal($tglP = null)
    {
        if (isset($_POST['submit'])) {
            $tglP  =   $this->input->post('tglP');
        }

        if ($tglP == null) {
            $bulan = (date('m'));
            $tahun = (date('Y'));
        } else {
            $bulan = substr($tglP, 5, 2);
            $tahun = explode('-', $tglP)[0];
        }

        // $data['id'] =  $event_version;
        $bul = array(1 => "Januari", 2 => "Februari", 3 => "Maret", 4 => "April", 5 => "Mei", 6 => "Juni", 7 => "Juli", 8 => "Agustus", 9 => "September", 10 => "Oktober", 11 => "November", 12 => "Desember");
        if ($bulan <= 9) {
            $bl = substr($bulan, 1, 1);
        } else {
            $bl = $bulan;
        }
        $data['title'] =  "Indikator Mutu Harian (Versi Lokal) Bulan " . $bul[$bl] . " " . $tahun;

        $indicator_id_user =  $this->session->userdata('hospital_survey_indicator_lkl'); //_for_hospital
        print_r($indicator_id_user);

        if ($indicator_id_user != null) {

            $data['record'] = $this->imut_m->imutLokal("indicator_category_id in (1, 2) and (MONTH(result_period) = $bulan and YEAR(result_period) = $tahun and indicator_id in ($indicator_id_user))");

            $data['indicator_element11'] = $this->imut_m->imutLokalSg("(indicator_category_id in (1, 2) and indicator_id in ($indicator_id_user)) or (
                indicator_category_id in (1, 2) and (MONTH(result_period) = $bulan and YEAR(result_period) = $tahun) and indicator_id in ($indicator_id_user)
            )");
        } else if ($indicator_id_user == null) {
            $data['record'] = $this->imut_m->imutLokal("indicator_category_id in (1, 2) and (MONTH(result_period) = $bulan and YEAR(result_period) = $tahun)");

            $data['indicator_element11'] = $this->imut_m->imutLokalSg("indicator_category_id in (1, 2) or (indicator_category_id in (1, 2) and (MONTH(result_period) = $bulan and YEAR(result_period) = $tahun))");
        }

        //11
        // $data['record'] = $this->imut_m->imutLokal("indicator_category_id in (1, 2) and (MONTH(result_period) = $bulan and YEAR(result_period) = $tahun)");

        // $data['indicator_element11'] = $this->imut_m->imutLokalSg("indicator_category_id in (1, 2) or (indicator_category_id in (1, 2) and (MONTH(result_period) = $bulan and YEAR(result_period) = $tahun))");

        $temp = $data['indicator_element11'];
        $indicator_element11 = array();
        $indicator_id11 = array();

        for ($i = 0; $i < count($temp); $i++) {
            $indicator_element11[$i] = $temp[$i]->indicator_element;
            $indicator_id11[$i] = $temp[$i]->indicator_id;
        }

        //get last day of month
        $lastDate = cal_days_in_month(CAL_GREGORIAN, $bulan, $tahun);
        if ($indicator_id_user == null) {
            $batas = count($indicator_element11);
        } else {
            $batas = $lastDate;
        }


        //looping tgl menyammping 11
        $record = array();
        for ($c = 1; $c < (count($indicator_element11) + 1); $c++) {

            for ($d = 1; $d <= $batas; $d++) { //(31) $data['sheet']

                // if ($d <= count($indicator_id11)){
                $record[$indicator_element11[$c - 1]][$d]['nilai'] = $this->getNilai($d, $bulan, $tahun, $indicator_element11[$c - 1],  $data);
                if ($record[$indicator_element11[$c - 1]][$d]['nilai'] == null) {
                    $record[$indicator_element11[$c - 1]][$d]['nilai'] = 0;
                }

                $record[$indicator_element11[$c - 1]][$d]['tgl'] = $this->getTgl($d, $bulan, $tahun, $indicator_element11[$c - 1],  $data);
                if ($record[$indicator_element11[$c - 1]][$d]['tgl'] == null) {
                    if ($d <= 9) {
                        $record[$indicator_element11[$c - 1]][$d]['tgl'] = $tahun . "-" . $bulan . "-0" . $d;
                    } else {
                        $record[$indicator_element11[$c - 1]][$d]['tgl'] = $tahun . "-" . $bulan . "-" . $d;
                    }
                }

                // if ($c <= count($indicator_id11)){
                $record[$indicator_element11[$c - 1]][$d]['nama'] = $indicator_element11[($c - 1)];
                $record[$indicator_element11[$c - 1]][$d]['id'] = $indicator_id11[$c - 1];
                // }

            }
        }

        $data['record'] = $record;
        $data['lastDate'] = $lastDate;

        $this->template->load('template', $this->folder . '/viewImutLokal', $data);
    }





    // function tampil($event_version)
    // {
    //     $data['id'] =  $event_version;
    //     $data['title'] =  $this->title;

    //     $data['record'] = $this->pokja_m->listPokja(array('chapter_version' => $event_version)); //bisa1

    //     // $data['record']   =  $this->mcrud->getByID2($this->tables, 'accreditation_transaction_chapter', 'accreditation_transaction_chapter.trans_chapter_id = accreditation_instrument_chapter.chapter_id', "chapter_version", $event_version)->result();

    //     $this->template->load('template', $this->folder . '/view', $data);
    // }

    // function tampilSub($standard_chapter_id)
    // {
    //     $data['id'] =  $standard_chapter_id;
    //     $data['title'] =  "Daftar Sub Bab Standar Akreditasi";
    //     $data['record'] = $this->pokja_m->listSubPokja(($standard_chapter_id)); //bisa1
    //     $this->template->load('template', $this->folder . '/viewSub', $data);
    // }



    function tampilElemen($standard_id)
    {
        $data['id'] =  $standard_id;
        $data['title'] =  "Elemen Penilaian";
        $data['record'] = $this->pokja_m->listElemen(array('instrument_standard_id' => $standard_id)); //bisa1
        $this->template->load('template', $this->folder . '/viewElemen', $data);
    }

    function tampilUpload($transaction_instrument_id)
    {
        $data['title'] =  "Unggah Dokumen";
        $data['record'] = $this->pokja_m->listUpload(array('document_instrument_id' => $transaction_instrument_id)); //bisa1
        $this->template->load('template', $this->folder . '/viewUpload', $data);
    }

    function editElemen()
    {
        if (isset($_POST['submit'])) {
            $transaction_surveyor_fact_and_analysis  =   $this->input->post('transaction_surveyor_fact_and_analysis');
            $transaction_surveyor_recommendation  =   $this->input->post('transaction_surveyor_recommendation');
            $transaction_hospital_score  =   $this->input->post('transaction_hospital_score');

            $transaction_standard_id = $this->input->post('transaction_standard_id'); //ambil kata kunci 

            $id     = $this->input->post('id');
            $data   =   array(
                'transaction_surveyor_fact_and_analysis' => ($transaction_surveyor_fact_and_analysis),
                'transaction_surveyor_recommendation' => ($transaction_surveyor_recommendation),
                'transaction_hospital_score' => ($transaction_hospital_score)
            );

            $this->mcrud->update("accreditation_transaction_ongoing", $data, 'transaction_instrument_id', $id);

            // redirect($this->uri->segment(1));
            redirect(base_url('pokja/tampilElemen/' . $transaction_standard_id));
        } else { //pertama kali
            $data['title'] =  "Edit Elemen Penilaian";
            $id          =  $this->uri->segment(3);

            $data['r'] = $this->pokja_m->listElemen2(array('transaction_instrument_id' => $id)); //bisa1

            $this->template->load('template', $this->folder . '/editElemen', $data);
        }
    }

    // function jenisKegiatan($id)
    // {
    //     if($id=='')
    //     {
    //         return '';
    //     }
    //     else
    //     {
    //         return getField('accreditation_event_category', 'category_name', 'category_id', $id);
    //     }
    // }

    // function level($level)
    // {
    //     if($level==1)
    //     {
    //         return 'admin';
    //     }
    //     elseif($level==2)
    //     {
    //         return 'pihak jurusan';
    //     }
    //     elseif($level==3)
    //     {
    //         return 'pegawai';
    //     }
    //     else
    //     {
    //         return 'mahasiswa';
    //     }
    // }

    function post()
    {
        if (isset($_POST['submit'])) {
            $event_title  =   $this->input->post('event_title');
            $event_version  =   $this->input->post('event_version');
            $event_id_kars     =   $this->input->post('event_id_kars');
            $event_start_date     =   $this->input->post('event_start_date');
            $event_end_date     =   $this->input->post('event_end_date');

            // if($level==2)
            // {
            $data   =   array('event_title' => $event_title, 'event_version' => ($event_version), 'event_id_kars' => $event_id_kars, 'event_start_date' => $event_start_date, 'event_end_date' => $event_end_date);
            // }
            // else
            // {
            //      $data   =   array('username'=>$username,'password'=>md5($password),'level'=>$level);
            // }
            $this->db->insert($this->tables, $data);
            redirect($this->uri->segment(1));
        } else {
            $data['title'] =  $this->title;
            $this->template->load('template', $this->folder . '/post', $data);
        }
    }
    function edit()
    {
        if (isset($_POST['submit'])) {
            $chapter_name  =   $this->input->post('chapter_name');
            $trans_hospital_score  =   $this->input->post('trans_hospital_score');

            $event_version = $this->input->post('event_version'); //ambil kata kunci 

            $id     = $this->input->post('id');
            $data   =   array('trans_hospital_score' => ($trans_hospital_score));

            $this->mcrud->update($this->tables2, $data, 'trans_chapter_id', $id);

            // redirect($this->uri->segment(1));
            redirect(base_url('pokja/tampil/' . $event_version));
        } else { //pertama kali
            $data['title'] =  $this->title;
            $id          =  $this->uri->segment(3);

            $data['r'] = $this->pokja_m->listPokja2(array('chapter_id' => $id)); //bisa1

            // $data['r']   =  $this->mcrud->getByID2($this->tables, 'accreditation_transaction_chapter', 'accreditation_transaction_chapter.trans_chapter_id = accreditation_instrument_chapter.chapter_id', "trans_chapter_id", $id)->row_array();

            $this->template->load('template', $this->folder . '/edit', $data);
        }
    }
    function delete()
    {
        $id     =  $this->uri->segment(3);
        $chekid = $this->db->get_where($this->tables, array($this->pk => $id));
        if ($chekid->num_rows() > 0) {
            $this->mcrud->delete($this->tables,  $this->pk,  $this->uri->segment(3));
        }
        redirect($this->uri->segment(1));
    }

    function profile()
    {
        $id =  $this->session->userdata('event_id');
        if (isset($_POST['submit'])) {
            $username =  $this->input->post('username');
            $password =  $this->input->post('password');
            $data    =  array('username' => $username, 'password' =>  md5($password));
            $this->mcrud->update($this->tables, $data, $this->pk, $id);
            redirect('users/profile');
        } else {
            $data['title'] =  $this->title;
            $data['r']   =  $this->mcrud->getByID($this->tables,  $this->pk, $id)->row_array();
            $this->template->load('template', $this->folder . '/profile', $data);
        }
    }

    function account()
    {
        $id =  $this->session->userdata('keterangan');
        if (isset($_POST['submit'])) {
            $nama           =   $this->input->post('nama');
            $nidn           =   $this->input->post('nidn');
            $nip            =   $this->input->post('nip');
            $tempat_lahir   =   $this->input->post('tempat_lahir');
            $tanggal_lahir  =   $this->input->post('tanggal_lahir');
            $gender         =   $this->input->post('gender');
            $agama          =   $this->input->post('agama');
            $kawin          =   $this->input->post('kawin');
            $alamat         =   $this->input->post('alamat');
            $hp             =   $this->input->post('hp');
            $email          =   $this->input->post('email');
            $data           =   array(
                'nama_lengkap' => $nama,
                'nidn' => $nidn,
                'nip' => $nip,
                'tempat_lahir' => $tempat_lahir,
                'tanggal_lahir' => $tanggal_lahir,
                'gender' => $gender,
                'agama_id' => $agama,
                'status_kawin' => $kawin,
                'alamat' => $alamat, 'hp' => $hp,
                'email' => $email
            );
            $this->mcrud->update('app_dosen', $data, 'dosen_id', $id);
            redirect('users/account');
        } else {
            $data['title'] =  $this->title;
            $data['r']   =  $this->mcrud->getByID('app_dosen',  'dosen_id',  $id)->row_array();
            $this->template->load('template', $this->folder . '/account', $data);
        }
    }
}
