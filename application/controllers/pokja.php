<?php
class pokja extends CI_Controller
{

    var $folder =   "pokja";
    var $tables =   "accreditation_instrument_chapter";
    var $tables2 =   "accreditation_transaction_chapter";
    var $pk     =   "chapter_id";
    var $title  =   "Daftar Bab Standar Akreditasi";
    function __construct()
    {
        parent::__construct();
        $this->load->model('pokja_m');
    }

    // function index()
    // {
    //     $data['title']=  $this->title;
    //     $data['record']=  $this->db->get($this->tables)->result();
    //     $this->template->load('template', $this->folder.'/view',$data);
    // }

    function index()
    {
        $data['title'] =  $this->title;
        $data['record'] =  $this->db->get($this->tables)->result();
        $this->template->load('template', $this->folder . '/view', $data);
    }

    function tampil($event_version)
    {
        $data['id'] =  $event_version;
        $data['title'] =  $this->title;

        $data['record'] = $this->pokja_m->listPokja(array('chapter_version' => $event_version)); //bisa1

        // $data['record']   =  $this->mcrud->getByID2($this->tables, 'accreditation_transaction_chapter', 'accreditation_transaction_chapter.trans_chapter_id = accreditation_instrument_chapter.chapter_id', "chapter_version", $event_version)->result();

        $this->template->load('template', $this->folder . '/view', $data);
    }

    function tampilSub($standard_chapter_id)
    {
        $data['id'] =  $standard_chapter_id;
        $data['title'] =  "Daftar Sub Bab Standar Akreditasi";
        $data['record'] = $this->pokja_m->listSubPokja(($standard_chapter_id)); //bisa1
        $this->template->load('template', $this->folder . '/viewSub', $data);
    }

    function tampilElemen($standard_id )
    {
        $data['id'] =  $standard_id;        
        $data['title'] =  "Elemen Penilaian";
        $data['record'] = $this->pokja_m->listElemen(array('instrument_standard_id' => $standard_id)); //bisa1
        $this->template->load('template', $this->folder . '/viewElemen', $data);
    }

    function tampilUpload($transaction_instrument_id )
    {
        $data['title'] =  "Unggah Dokumen";
        $data['record'] = $this->pokja_m->listUpload(array('document_instrument_id' => $transaction_instrument_id)); //bisa1
        $this->template->load('template', $this->folder . '/viewUpload', $data);
    }

    function editElemen()
    {
        if (isset($_POST['submit'])) {
            $transaction_surveyor_fact_and_analysis  =   $this->input->post('transaction_surveyor_fact_and_analysis');
            $transaction_surveyor_recommendation  =   $this->input->post('transaction_surveyor_recommendation');
            $transaction_hospital_score  =   $this->input->post('transaction_hospital_score');

            $transaction_standard_id = $this->input->post('transaction_standard_id'); //ambil kata kunci 

            $id     = $this->input->post('id');
            $data   =   array(
                    'transaction_surveyor_fact_and_analysis' => ($transaction_surveyor_fact_and_analysis), 
                    'transaction_surveyor_recommendation' => ($transaction_surveyor_recommendation),
                    'transaction_hospital_score' => ($transaction_hospital_score));

            $this->mcrud->update("accreditation_transaction_ongoing", $data, 'transaction_instrument_id', $id);

            // redirect($this->uri->segment(1));
            redirect(base_url('pokja/tampilElemen/' . $transaction_standard_id));
        } else { //pertama kali
            $data['title'] =  "Edit Elemen Penilaian";
            $id          =  $this->uri->segment(3);

            $data['r'] = $this->pokja_m->listElemen2(array('transaction_instrument_id' => $id)); //bisa1

            $this->template->load('template', $this->folder . '/editElemen', $data);
        }
    }

    // function jenisKegiatan($id)
    // {
    //     if($id=='')
    //     {
    //         return '';
    //     }
    //     else
    //     {
    //         return getField('accreditation_event_category', 'category_name', 'category_id', $id);
    //     }
    // }

    // function level($level)
    // {
    //     if($level==1)
    //     {
    //         return 'admin';
    //     }
    //     elseif($level==2)
    //     {
    //         return 'pihak jurusan';
    //     }
    //     elseif($level==3)
    //     {
    //         return 'pegawai';
    //     }
    //     else
    //     {
    //         return 'mahasiswa';
    //     }
    // }

    function post()
    {
        if (isset($_POST['submit'])) {
            $event_title  =   $this->input->post('event_title');
            $event_version  =   $this->input->post('event_version');
            $event_id_kars     =   $this->input->post('event_id_kars');
            $event_start_date     =   $this->input->post('event_start_date');
            $event_end_date     =   $this->input->post('event_end_date');

            // if($level==2)
            // {
            $data   =   array('event_title' => $event_title, 'event_version' => ($event_version), 'event_id_kars' => $event_id_kars, 'event_start_date' => $event_start_date, 'event_end_date' => $event_end_date);
            // }
            // else
            // {
            //      $data   =   array('username'=>$username,'password'=>md5($password),'level'=>$level);
            // }
            $this->db->insert($this->tables, $data);
            redirect($this->uri->segment(1));
        } else {
            $data['title'] =  $this->title;
            $this->template->load('template', $this->folder . '/post', $data);
        }
    }
    function edit()
    {
        if (isset($_POST['submit'])) {
            $chapter_name  =   $this->input->post('chapter_name');
            $trans_hospital_score  =   $this->input->post('trans_hospital_score');

            $event_version = $this->input->post('event_version'); //ambil kata kunci 

            $id     = $this->input->post('id');
            $data   =   array('trans_hospital_score' => ($trans_hospital_score));

            $this->mcrud->update($this->tables2, $data, 'trans_chapter_id', $id);

            // redirect($this->uri->segment(1));
            redirect(base_url('pokja/tampil/' . $event_version));
        } else { //pertama kali
            $data['title'] =  $this->title;
            $id          =  $this->uri->segment(3);

            $data['r'] = $this->pokja_m->listPokja2(array('chapter_id' => $id)); //bisa1

            // $data['r']   =  $this->mcrud->getByID2($this->tables, 'accreditation_transaction_chapter', 'accreditation_transaction_chapter.trans_chapter_id = accreditation_instrument_chapter.chapter_id', "trans_chapter_id", $id)->row_array();

            $this->template->load('template', $this->folder . '/edit', $data);
        }
    }
    function delete()
    {
        $id     =  $this->uri->segment(3);
        $chekid = $this->db->get_where($this->tables, array($this->pk => $id));
        if ($chekid->num_rows() > 0) {
            $this->mcrud->delete($this->tables,  $this->pk,  $this->uri->segment(3));
        }
        redirect($this->uri->segment(1));
    }

    function profile()
    {
        $id =  $this->session->userdata('event_id');
        if (isset($_POST['submit'])) {
            $username =  $this->input->post('username');
            $password =  $this->input->post('password');
            $data    =  array('username' => $username, 'password' =>  md5($password));
            $this->mcrud->update($this->tables, $data, $this->pk, $id);
            redirect('users/profile');
        } else {
            $data['title'] =  $this->title;
            $data['r']   =  $this->mcrud->getByID($this->tables,  $this->pk, $id)->row_array();
            $this->template->load('template', $this->folder . '/profile', $data);
        }
    }

    function account()
    {
        $id =  $this->session->userdata('keterangan');
        if (isset($_POST['submit'])) {
            $nama           =   $this->input->post('nama');
            $nidn           =   $this->input->post('nidn');
            $nip            =   $this->input->post('nip');
            $tempat_lahir   =   $this->input->post('tempat_lahir');
            $tanggal_lahir  =   $this->input->post('tanggal_lahir');
            $gender         =   $this->input->post('gender');
            $agama          =   $this->input->post('agama');
            $kawin          =   $this->input->post('kawin');
            $alamat         =   $this->input->post('alamat');
            $hp             =   $this->input->post('hp');
            $email          =   $this->input->post('email');
            $data           =   array(
                'nama_lengkap' => $nama,
                'nidn' => $nidn,
                'nip' => $nip,
                'tempat_lahir' => $tempat_lahir,
                'tanggal_lahir' => $tanggal_lahir,
                'gender' => $gender,
                'agama_id' => $agama,
                'status_kawin' => $kawin,
                'alamat' => $alamat, 'hp' => $hp,
                'email' => $email
            );
            $this->mcrud->update('app_dosen', $data, 'dosen_id', $id);
            redirect('users/account');
        } else {
            $data['title'] =  $this->title;
            $data['r']   =  $this->mcrud->getByID('app_dosen',  'dosen_id',  $id)->row_array();
            $this->template->load('template', $this->folder . '/account', $data);
        }
    }
}
