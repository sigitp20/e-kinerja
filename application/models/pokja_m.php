<?php

/**
 * Description of mcrud
 * class ini digunakan untuk melakukan manipulasi  data sederhana
 * dengan parameter yang dikirim dari controller.
 * @author nuris akbar
 */
class pokja_m extends CI_Model
{
    public function listPokja($condition = null)
    {
        $this->db->where($condition);
        $this->db->join('accreditation_transaction_chapter', 'accreditation_transaction_chapter.trans_chapter_id = accreditation_instrument_chapter.chapter_id');
        $this->db->select('*');
        $query = $this->db->get('accreditation_instrument_chapter');
        return $query->result();
    }

    public function listPokja2($condition = null)
    {
        $this->db->where($condition);
        $this->db->join('accreditation_transaction_chapter', 'accreditation_transaction_chapter.trans_chapter_id = accreditation_instrument_chapter.chapter_id');
        $this->db->select('*');
        $query = $this->db->get('accreditation_instrument_chapter');
        return $query->row_array();
    }

    

    public function listSubPokja($instrument_chapter_id)
    {
        $query = $this->db->query("select standard_id, standard_version, standard_chapter_id, standard_code, standard_name, count(instrument_standard_id) as jumlah 
        from accreditation_instrument_standard
        join accreditation_instrument on accreditation_instrument_standard.standard_id = accreditation_instrument.instrument_standard_id
        WHERE instrument_chapter_id = " . $instrument_chapter_id . "
        GROUP BY standard_code
        having count(instrument_standard_id)");
        return $query->result();
    }

    public function listElemen($standard_id)
    {
        $this->db->where($standard_id);
        $this->db->join('accreditation_instrument', 'accreditation_instrument.instrument_standard_id = accreditation_instrument_standard.standard_id');
        $this->db->join('accreditation_transaction_ongoing','accreditation_transaction_ongoing.transaction_instrument_id = accreditation_instrument.instrument_id');
        $this->db->select('*');
        $query = $this->db->get('accreditation_instrument_standard');
        return $query->result();
    }

    public function listElemen2($condition = null)
    {
        $this->db->where($condition);
        $this->db->join('accreditation_instrument', 'accreditation_instrument.instrument_standard_id = accreditation_instrument_standard.standard_id');
        $this->db->join('accreditation_transaction_ongoing','accreditation_transaction_ongoing.transaction_instrument_id = accreditation_instrument.instrument_id');
        $this->db->select('*');
        $query = $this->db->get('accreditation_instrument_standard');
        return $query->row_array();
    }

    public function listUpload($standard_id)
    {
        $this->db->where($standard_id);
        // $this->db->join('accreditation_instrument', 'accreditation_instrument.instrument_standard_id = accreditation_hospital_document.standard_id');
        // $this->db->join('accreditation_transaction_ongoing','accreditation_transaction_ongoing.transaction_instrument_id = accreditation_instrument.instrument_id');
        $this->db->select('*');
        $query = $this->db->get('accreditation_hospital_document');
        return $query->result();
    }
}
