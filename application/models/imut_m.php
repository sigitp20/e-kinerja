<?php

/**
 * Description of mcrud
 * class ini digunakan untuk melakukan manipulasi  data sederhana
 * dengan parameter yang dikirim dari controller.
 * @author nuris akbar
 */
class imut_m extends CI_Model
{
    // public function listPokja($condition = null)
    // {
    //     $this->db->where($condition);
    //     $this->db->join('accreditation_transaction_chapter', 'accreditation_transaction_chapter.trans_chapter_id = accreditation_instrument_chapter.chapter_id');
    //     $this->db->select('*');
    //     $query = $this->db->get('accreditation_instrument_chapter');
    //     return $query->result();
    // }

    public function imutManajemen($condition = null)
    {
        $this->db->where($condition);
        $this->db->join('hospital_survey_indicator_result', 'hospital_survey_indicator_result.result_indicator_id = hospital_survey_indicator.indicator_id', 'left');
        $this->db->select('*');
        // $this->db->group_by('indicator_element');
        $this->db->order_by("indicator_element");
        $query = $this->db->get('hospital_survey_indicator');
        return $query->result();
    }

    public function imutLokal($condition = null)
    {        
        $this->db->where($condition);
        $this->db->join('hospital_survey_indicator_result_for_hospital', 'hospital_survey_indicator_result_for_hospital.result_indicator_id = hospital_survey_indicator_for_hospital.indicator_id', 'left');
        $this->db->select('*');
        // $this->db->group_by('indicator_element');
        $this->db->order_by("indicator_element");
        $query = $this->db->get('hospital_survey_indicator_for_hospital');
        return $query->result();
    }

    public function imutLokalSg($condition = null) //sg=single
    {
        $this->db->where($condition);
        $this->db->join('hospital_survey_indicator_result_for_hospital', 'hospital_survey_indicator_result_for_hospital.result_indicator_id = hospital_survey_indicator_for_hospital.indicator_id', 'left');
        $this->db->select('indicator_element, indicator_id');
        $this->db->group_by('indicator_element');
        $this->db->order_by("indicator_element");
        $query = $this->db->get('hospital_survey_indicator_for_hospital');
        return $query->result();
    }

    public function imutNas($condition = null) 
    {
        $this->db->where($condition);
        $this->db->join('hospital_survey_indicator_result_recapitulation', 'hospital_survey_indicator_result_recapitulation.result_indicator_id = hospital_survey_indicator.indicator_id', 'left');
        $this->db->select('*');
        // $this->db->group_by('indicator_element');
        $this->db->order_by("indicator_element");
        $query = $this->db->get('hospital_survey_indicator');
        return $query->result();
    }

    public function imutNasSg($condition = null) //sg=single
    {
        $this->db->where($condition);
        $this->db->join('hospital_survey_indicator_result_recapitulation', 'hospital_survey_indicator_result_recapitulation.result_indicator_id = hospital_survey_indicator.indicator_id', 'left');
        $this->db->select('indicator_element, indicator_id');
        $this->db->group_by('indicator_element');
        $this->db->order_by("indicator_element");
        $query = $this->db->get('hospital_survey_indicator');
        return $query->result();
    }

    public function detailIndikator($indikator_id = null, $tabel)
    {        
        if ($indikator_id != null or $indikator_id != '') {
            $this->db->where($indikator_id);
        }
        $this->db->select('*');
        $query = $this->db->get($tabel); //'hospital_survey_indicator' ; hospital_survey_indicator_for_hospital
        return $query->result();
    }

    public function inputIndikator($variable_indicator_id)
    {
        $this->db->where($variable_indicator_id);
        $this->db->join('hospital_survey_indicator_result', 'hospital_survey_indicator_result.result_indicator_id = hospital_survey_indicator_variable.variable_indicator_id', 'left'); //, 'left' right
        $this->db->select('*');
        $this->db->limit(2);
        $query = $this->db->get('hospital_survey_indicator_variable');
        return $query->result();
    }

    public function inputIndikatorNol($variable_indicator_id) //$variable_indicator_id
    {
        $this->db->where($variable_indicator_id);
        // $this->db->join('hospital_survey_indicator_result', 'hospital_survey_indicator_result.result_indicator_id = hospital_survey_indicator_variable.variable_indicator_id', 'left'); //, 'left' right
        $this->db->select('*');
        $this->db->limit(2);
        $query = $this->db->get('hospital_survey_indicator_variable');
        return $query->result();
    }

    public function inputIndikatorLokal($variable_indicator_id)
    {
        $this->db->where($variable_indicator_id);
        $this->db->join('hospital_survey_indicator_result_for_hospital', 'hospital_survey_indicator_result_for_hospital.result_indicator_id = hospital_survey_indicator_variable_for_hospital.variable_indicator_id', 'left'); //, 'left' right
        $this->db->select('*');
        $this->db->limit(2);
        $query = $this->db->get('hospital_survey_indicator_variable_for_hospital');
        return $query->result();
    }

    public function inputIndikatorLokalNol($variable_indicator_id) //$variable_indicator_id
    {
        $this->db->where($variable_indicator_id);
        // $this->db->join('hospital_survey_indicator_result', 'hospital_survey_indicator_result.result_indicator_id = hospital_survey_indicator_variable.variable_indicator_id', 'left'); //, 'left' right
        $this->db->select('*');
        $this->db->limit(2);
        $query = $this->db->get('hospital_survey_indicator_variable_for_hospital');
        return $query->result();
    }

    public function getSumNum($where)
    {
        $this->db->where($where);
        $this->db->select('sum(result_numerator_value) as jumlah');
        $query = $this->db->get('hospital_survey_indicator_result');
        return $query->row_array();
    }

    public function getSumDenum($where)
    {
        $this->db->where($where);
        $this->db->select('sum(result_denumerator_value) as jumlah');
        $query = $this->db->get('hospital_survey_indicator_result');
        return $query->row_array();
    }

    public function getSumNumLokal($where)
    {
        $this->db->where($where);
        $this->db->select('sum(result_numerator_value) as jumlah');
        $query = $this->db->get('hospital_survey_indicator_result_for_hospital');
        return $query->row_array();
    }

    public function getSumDenumLokal($where)
    {
        $this->db->where($where);
        $this->db->select('sum(result_denumerator_value) as jumlah');
        $query = $this->db->get('hospital_survey_indicator_result_for_hospital');
        return $query->row_array();
    }

    public function cekResult($where)
    {
        $this->db->where($where);
        $this->db->select('count(result_indicator_id) as jumlah');
        $query = $this->db->get('hospital_survey_indicator_result');
        return $query->row_array();
    }

    public function cekResultLokal($where)
    {
        $this->db->where($where);
        $this->db->select('count(result_indicator_id) as jumlah');
        $query = $this->db->get('hospital_survey_indicator_result_for_hospital');
        return $query->row_array();
    }

    // public function deleteRekap($where)
    // {
    //     print_r($where);
    //     $this->db->where($where);
    //     $this->db->select('count(result_indicator_id) as jumlah');
    //     $query = $this->db->get('hospital_survey_indicator_result_recapitulation');
    //     return $query->row_array();
    // }

    public function cekRekap($where)
    {
        $this->db->where($where);
        $this->db->select('count(result_indicator_id) as jumlah');
        $query = $this->db->get('hospital_survey_indicator_result_recapitulation');
        return $query->row_array();
    }

    public function cekRekapLokal($where)
    {
        $this->db->where($where);
        $this->db->select('count(result_indicator_id) as jumlah');
        $query = $this->db->get('hospital_survey_indicator_result_recapitulation_for_hospital');
        return $query->row_array();
    }

    public function get_mutu_tahun($year = null, $result_indicator_id = null)
    {
        if ($year == null) {
            $this->db->where('Year(result_period)', date('Y'));
        } else {
            $this->db->where('Year(result_period)', $year);
        }
        $this->db->where('result_indicator_id', $result_indicator_id);

        $this->db->select('MONTH(result_period) as bulan, 
        result_numerator_value as total'); //(result_numerator_value/result_denumerator_value)*100

        // $this->db->group_by('bulan');
        $this->db->order_by('bulan', 'ASC');
        $query = $this->db->get('hospital_survey_indicator_result_recapitulation');
        return $query->result();
    }

    public function aksesImut($condition = null)
    {        
        $this->db->where($condition);
        // $this->db->join('hospital_survey_indicator_result_for_hospital', 'hospital_survey_indicator_result_for_hospital.result_indicator_id = hospital_survey_indicator_for_hospital.indicator_id', 'left');
        $this->db->select('*');
        // $this->db->group_by('indicator_element');
        $this->db->order_by("username");
        $query = $this->db->get('app_users');
        return $query->result();
    }

    public function aksesImutNas($condition = null)
    {        
        $this->db->where($condition);
        $this->db->join('hospital_survey_indicator', 'hospital_survey_indicator.indicator_id = akses_imut.survey_indicator_id');
        $this->db->join('app_users', 'app_users.id_users = akses_imut.id_users');
        $this->db->select('*'); //survey_indicator_id, akses_imut.id_users as id_users, indicator_element 
        $this->db->group_by('survey_indicator_id');
        $this->db->order_by("survey_indicator_id");
        $query = $this->db->get('akses_imut');
        return $query->result();
    }

    // function updateMutu($tables, $result_numerator_value, $result_denumerator_value, $result_post_date, $result_indicator_id, $result_period){
    //     $query = $this->db->query("update ".$tables." 
    //     set result_numerator_value = ".$result_numerator_value."


    //     where result_indicator_id = ".$result_indicator_id." and result_period = ".$result_period." ;"  );
    //     return $query;
    // }
    // , result_denumerator_value = ".$result_denumerator_value."
    //, result_post_date = ".$result_post_date."  


    // public function listPokja2($condition = null)
    // {
    //     $this->db->where($condition);
    //     $this->db->join('accreditation_transaction_chapter', 'accreditation_transaction_chapter.trans_chapter_id = accreditation_instrument_chapter.chapter_id');
    //     $this->db->select('*');
    //     $query = $this->db->get('accreditation_instrument_chapter');
    //     return $query->row_array();
    // }



    // public function listSubPokja($instrument_chapter_id)
    // {
    //     $query = $this->db->query("select standard_id, standard_version, standard_chapter_id, standard_code, standard_name, count(instrument_standard_id) as jumlah 
    //     from accreditation_instrument_standard
    //     join accreditation_instrument on accreditation_instrument_standard.standard_id = accreditation_instrument.instrument_standard_id
    //     WHERE instrument_chapter_id = " . $instrument_chapter_id . "
    //     GROUP BY standard_code
    //     having count(instrument_standard_id)");
    //     return $query->result();
    // }

    // public function listElemen($standard_id)
    // {
    //     $this->db->where($standard_id);
    //     $this->db->join('accreditation_instrument', 'accreditation_instrument.instrument_standard_id = accreditation_instrument_standard.standard_id');
    //     $this->db->join('accreditation_transaction_ongoing','accreditation_transaction_ongoing.transaction_instrument_id = accreditation_instrument.instrument_id');
    //     $this->db->select('*');
    //     $query = $this->db->get('accreditation_instrument_standard');
    //     return $query->result();
    // }

    // public function listElemen2($condition = null)
    // {
    //     $this->db->where($condition);
    //     $this->db->join('accreditation_instrument', 'accreditation_instrument.instrument_standard_id = accreditation_instrument_standard.standard_id');
    //     $this->db->join('accreditation_transaction_ongoing','accreditation_transaction_ongoing.transaction_instrument_id = accreditation_instrument.instrument_id');
    //     $this->db->select('*');
    //     $query = $this->db->get('accreditation_instrument_standard');
    //     return $query->row_array();
    // }

    // public function listUpload($standard_id)
    // {
    //     $this->db->where($standard_id);
    //     // $this->db->join('accreditation_instrument', 'accreditation_instrument.instrument_standard_id = accreditation_hospital_document.standard_id');
    //     // $this->db->join('accreditation_transaction_ongoing','accreditation_transaction_ongoing.transaction_instrument_id = accreditation_instrument.instrument_id');
    //     $this->db->select('*');
    //     $query = $this->db->get('accreditation_hospital_document');
    //     return $query->result();
    // }
}
