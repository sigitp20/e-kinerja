<h2 style="font-weight: normal;"><?php echo $title; ?></h2>
<div class="push">
  <ol class="breadcrumb">
    <li><i class='fa fa-home'></i> <a href="javascript:void(0)">Home</a></li>
    <li><?php echo anchor($this->uri->segment(1), $title); ?></li>
    <li class="active">Data</li>
  </ol>
</div>
<?php
echo form_open('users/profile');
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Your Account</h3>
  </div>
  <div class="panel-body">
    <table class="table table-bordered">

      <tr>
        <td width="100">Username</td>
        <td> <?php echo inputan('text', 'username', 'col-sm-4', 'Username ..', 1, $r['username'], ''); ?></td>
      </tr>
      <tr>
        <td width="100">Nama</td>
        <td> <?php echo inputan('text', 'nama', 'col-sm-4', 'Nama ..', 1, $r['nama'], ''); ?></td>
      </tr>
      <tr>
        <td width="100">Handphone</td>
        <td> <?php echo inputan('text', 'handphone', 'col-sm-4', 'Handphone ..', 1, $r['handphone'], ''); ?></td>
      </tr>
      <tr>
        <td width="100">Tempat Lahir</td>
        <td> <?php echo inputan('text', 'tempat_lahir', 'col-sm-4', 'Tempat Lahir ..', 1, $r['tempat_lahir'], ''); ?></td>
      </tr>
      <tr>
        <td width="100">Tanggal Lahir</td>
        <td> <?php echo inputan('text', 'tgl_lahir', 'col-sm-4', 'Tanggal Lahir ..', 1, $r['tgl_lahir'], array('id' => 'datepicker2')); ?></td>
      </tr>
      <tr>
        <td>Password</td>
        <td> <?php echo inputan('password', 'password', 'col-sm-3', 'Password ..', 1, '', ''); ?></td>
      </tr>
      <tr>
        <td colspan="2">
          <input type="submit" name="submit" value="simpan" class="btn btn-danger  btn-sm">

        </td>
      </tr>
    </table>
  </div>
</div>
</FORM>