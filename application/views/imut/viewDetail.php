<h2 style="font-weight: normal;"><?php echo $title; ?></h2>
<div class="push">
    <ol class="breadcrumb">
        <li><i class='fa fa-home'></i> <a href="javascript:void(0)">Home</a></li>
         <li><?php echo anchor(base_url() . 'imut/'.$up , "Indikator Mutu Harian"); ?></li>
       <li><?php echo  "Daftar Indikator"; ?></li>
       <!--  <li><?php echo anchor(base_url() . 'pokja/tampilSub/' . $id, $title); ?></li> -->
    </ol>
</div>
<?php
// echo anchor($this->uri->segment(1) . '/post', 'Tambah Data', array('class' => 'btn btn-danger   btn-sm'))
?>
<!-- id="example-datatables" -->
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>No</th>
            <th>Kategori</th>
            <th>Judul Indikator</th>
            <th>Definisi Operasional</th>
            <th>Kriteria Inklusi</th>
            <th>Kriteria Eksklusi</th>
            <th>Sumber Data</th>
            <th>Tipe Indikator</th>
            <th>Area Monitoring</th>
            <th>Frekwensi</th>
            <th>Standar</th>

        </tr>
    </thead>
    <tbody>

        <?php
        $i = 1;
        foreach ($record as $r) {
        ?>
            <tr>

                <?php
                $indicator_category_s = "";
                if ($r->indicator_category_id == '1') {
                    $indicator_category_s = "Klinik";
                } else if ($r->indicator_category_id == '2') {
                    $indicator_category_s = "Manajemen";
                } else if ($r->indicator_category_id == '4') {
                    $indicator_category_s = "Wajib";
                } ?>

                <td><?php echo $i; ?></td>
                <td><?php echo $indicator_category_s; ?></a></td>
                <td><?php echo $r->indicator_element; ?></a></td>
                <td><?php echo $r->indicator_definition; ?></a></td>
                <td><?php echo $r->indicator_criteria_inclusive; ?></a></td>
                <td><?php echo $r->indicator_criteria_exclusive; ?></a></td>
                <td><?php echo $r->indicator_source_of_data; ?></a></td>
                <td><?php echo $r->indicator_type; ?></a></td>
                <td><?php echo $r->indicator_monitoring_area; ?></a></td>
                <td><?php echo $r->indicator_frequency; ?></a></td>
                <td><?php echo $r->indicator_target; ?></a></td>
                
            </tr>
        <?php $i++;
        } ?>


    </tbody>
</table>
<!-- END Datatables -->