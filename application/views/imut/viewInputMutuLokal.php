<h2 style="font-weight: normal;"><?php echo $title; ?></h2>
<div class="push">
    <ol class="breadcrumb">
        <li><i class='fa fa-home'></i> <a href="javascript:void(0)">Home</a></li>
        <li><?php echo anchor(base_url() . 'imut/' . $up, "Indikator Mutu Harian"); ?></li>
        <li><?php echo anchor(base_url() . 'imut/tampilDetail/' . $id . '/' . $up.'/hospital_survey_indicator', "Daftar Indikator"); ?></li>
        
    </ol>
</div>
<?php
echo form_open_multipart(base_url() . 'imut/inputMutuLokal/'.$id.'/'.$result_period.'/'.$up);
// echo "<input type='hidden' name='id' value='".$record[1]->result_indicator_id."'";
// echo anchor($this->uri->segment(1) . '/post', 'Tambah Data', array('class' => 'btn btn-danger   btn-sm'))
?>
<!-- id="example-datatables" -->
<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>No</th>
            <th>Indikator</th>
            <th>Hasil</th>
            <th>Satuan</th>
            <th>Dokumen Pendukung</th>
            <!-- <th>Kriteria Eksklusi</th> -->

        </tr>
    </thead>
    <tbody>

        <?php
        $i = 1;
        foreach ($record as $r) {
        ?>
            <tr>
                <?php
                // $indicator_category_s = "";
                // if ($r->indicator_category_id == '1') {
                //     $indicator_category_s = "Klinik";
                // } else if ($r->indicator_category_id == '2') {
                //     $indicator_category_s = "Manajemen";
                // } else if ($r->indicator_category_id == '4') {
                //     $indicator_category_s = "Wajib";
                // } 

                $result_value = "";
                if ($r->variable_type == "N") {
                    $result_value = $r->result_numerator_value;
                } else if ($r->variable_type == "D") {
                    $result_value = $r->result_denumerator_value;
                }

                ?>

                <td><?php echo $i; ?></td>
                <td><?php echo $r->variable_name; ?></a></td>
                <td><input type="text" name="result_value<?php echo $i; ?>" value="<?php echo $result_value; ?>" /></a></td>
                <td><?php echo $r->variable_unit_name; ?></a></td>
                <td><?php  ?></a></td>

            </tr>
        <?php $i++;
        } ?>
        <tr>
            <td></td>
            <td></td>
            <td colspan="2">
                <input type="submit" name="submit" value="simpan" class="btn btn-danger  btn-sm">
                <input type='hidden' name='id' value='<?=$record[1]->result_indicator_id ?>'>
                <input type='hidden' name='id2' value='<?=$record[1]->result_period ?>'>
                <!-- <?php echo anchor($this->uri->segment(1), 'kembali', array('class' => 'btn btn-danger btn-sm')); ?> -->
            </td>
            <td></td>
        </tr>


    </tbody>
</table>
<!-- END Datatables -->