<h2 style="font-weight: normal;"><?php echo $title; ?></h2>
<div class="push">
    <ol class="breadcrumb">
        <li><i class='fa fa-home'></i> <a href="javascript:void(0)">Home</a></li>
        <li><?php echo anchor($this->uri->segment(1), $title); ?></li>
        <li class="active">Entry Record</li>
    </ol>
</div>
<?php
echo anchor($this->uri->segment(1) . '/post', 'Tambah Data', array('class' => 'btn btn-danger   btn-sm'))
?>
<table id="example-datatables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama File</th>
            <th>Deskripsi Dokumen</th>
            <th>Tanggal Upload</th>
            <th>Diunggah Oleh</th>
        </tr>
    </thead>
    <tbody>

        <?php
        $i = 1;
        foreach ($record as $r) {
        ?>
            <tr>
                <td><?php echo $i; ?></td>

                <?php $name = explode("c4ca4238a0b923820dcc509a6f75849b-", $r->document_file_name) ?>
                <td><?php echo $name[1]; ?></a></td>

                <td width="550" class="text-center"><?php echo $r->document_name; ?></a></td>

                <td><?php echo $r->document_post_date; ?></a></td>
                <td><?php echo $r->document_user_id; ?></a></td>
                <!-- <td><a href="<?php echo base_url() . 'pokja/tampilUpload/' . $r->transaction_instrument_id; ?>"><?php echo "Dokumen (" . $r->transaction_document_amount . ")"; ?></a></td> 
                <td><?php echo $r->transaction_hospital_score; ?></a></td> 
                <td width="80" class="text-center">
                    <div class="btn-group">
                        <a href="<?php echo base_url() . 'pokja/edit/' . $r->transaction_instrument_id; ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-success"> <i class="fa fa-pencil"></i> Edit </a> 
                        <!-- <a href="<?php echo base_url('kegiatan/delete/' . $r->event_id); ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a> 
                </div>
                </td> -->
            </tr>
        <?php $i++;
        } ?>

    </tbody>
</table>
<!-- END Datatables -->