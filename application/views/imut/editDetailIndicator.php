<h2 style="font-weight: normal;"><?php echo $title; ?></h2>
<div class="push">
  <ol class="breadcrumb">
    <li><i class='fa fa-home'></i> <a href="javascript:void(0)">Home</a></li>
    <li><?php echo anchor(base_url() . 'imut/tampilDetailLokal/' . $r['indicator_id'] . '/imutLokal/hospital_survey_indicator_for_hospital', "Daftar Indikator "); ?></li>
    <li><?= $title; ?></li>
    <!-- <li class="active">Entry Record</li> -->
  </ol>
</div>
<script src="<?php echo base_url(); ?>assets/js/1.8.2.min.js"></script>
<script>
  $(document).ready(function() {
    $("#jurusan").hide();
  });
</script>
<script>
  $(document).ready(function() {
    $("#level").change(function() {
      var level = $("#level").val();
      if (level == 2) {
        $("#jurusan").show();
      } else {
        $("#jurusan").hide();
      }
    });
  });
</script>
<?php
echo form_open(base_url() . 'imut/editDetailIndicator/' . $r['indicator_id'].'/imutLokal');
echo "<input type='hidden' name='id' value='$r[indicator_id]'>";
$level = array(1 => 'Admin', 2 => 'Pihak Jurusan', 3 => 'Dosen');
$class      = "class='form-control' id='level'";
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Edit Record</h3>
  </div>
  <div class="panel-body">
    <table class="table table-bordered">

      <tr>
        <td width="150">Kategori</td>
        <td>
          <div class="col-sm-2">
            <?php
            $indicator_category_id = array('1' => 'Klinik', '2' => 'Manajemen', '4' => 'Wajib');
            echo form_dropdown('indicator_category_id', $indicator_category_id, $r['indicator_category_id'], "class='form-control'"); ?>

            <!-- <?php echo inputan('text', 'event_title', 'col-sm-4', 'Nama Kegiatan ..', 1, $r['event_title'], ''); ?> -->

          </div>
        </td>
      </tr>
      <tr>

      <tr>
        <td width="130">Judul Indikator</td>
        <td>


          <?php echo inputan('text', 'indicator_element', 'col-sm-12', 'Judul Indikator ..', 1, $r['indicator_element'], ''); ?>

          <!-- <?php echo buatcombo('event_version', 'event_version', 'col-sm-6', 'keterangan', 'event_version_id', '', array('id' => 'event_version')); ?> -->
          <!-- </div> -->
        </td>

      </tr>

      <tr>
        <td width="150">Defenisi Operasional</td>
        <td>
          <?php echo inputan('text', 'indicator_definition', 'col-sm-12', 'Defenisi Operasional ..', 0, $r['indicator_definition'], ''); ?>
        </td>
      </tr>
      <tr>
        <td width="150">Kriteria Inklusi</td>
        <td>
          <?php echo inputan('text', 'indicator_criteria_inclusive', 'col-sm-12', 'Kriteria Inklusi ..', 0, $r['indicator_criteria_inclusive'], ''); ?>
        </td>
      </tr>
      <tr>
        <td width="150">Kriteria Ekslusi</td>
        <td>
          <?php echo inputan('text', 'indicator_criteria_exclusive', 'col-sm-12', 'Kriteria Ekslusi ..', 0, $r['indicator_criteria_exclusive'], ''); ?>
        </td>
      </tr>
      <tr>
        <td width="150">Sumber Data</td>
        <td>
          <?php echo inputan('text', 'indicator_source_of_data', 'col-sm-12', 'Sumber Data ..', 0, $r['indicator_source_of_data'], ''); ?>
        </td>
      </tr>
      <tr>
        <td width="150">Tipe Indikator</td>
        <td>
          <div class="col-sm-4"><?php
                                $indicator_type = array('Input' => 'Input', 'Proses' => 'Proses', 'Outcome' => 'Outcome', 'Outcome and Process' => 'Outcome and Process');
                                echo form_dropdown('indicator_type', $indicator_type, $r['indicator_type'], "class='form-control'"); ?>

            <!-- <?php echo inputan('text', 'indicator_type', 'col-sm-8', 'Tanggal Selesai ..', 0, $r['event_end_date'], array('id' => 'datepicker2')); ?> -->

          </div>
        </td>
      </tr>
      <tr>
        <td width="150">Area Monitoring</td>
        <td>
          <?php echo inputan('text', 'indicator_monitoring_area', 'col-sm-8', 'Area Monitoring ..', 0, $r['indicator_monitoring_area'], ''); ?>
        </td>
      </tr>
      <tr>
        <td width="150">Frekwensi</td>
        <td>
          <div class="col-sm-4">
            <?php
            $indicator_frequency = array('Y' => 'Tahunan', 'M' => 'Bulanan', 'W' => 'Mingguan', 'D' => 'Harian');
            echo form_dropdown('indicator_frequency', $indicator_frequency, $r['indicator_frequency'], "class='form-control'"); ?>
          </div>
        </td>
      </tr>
      <tr>
        <td width="150">Standar</td>
        <td>
          <?php echo inputan('text', 'indicator_target', 'col-sm-4', 'Standar ..', 0, $r['indicator_target'], ''); ?>
        </td>
      </tr>

      <tr>
        <td></td>
        <td colspan="2">
          <input type="submit" name="submit" value="simpan" class="btn btn-danger  btn-sm">
          <!-- <?php echo anchor($this->uri->segment(1), 'kembali', array('class' => 'btn btn-danger btn-sm')); ?> -->
        </td>
      </tr>

    </table>
  </div>
</div>
</form>