<h2 style="font-weight: normal;"><?php echo $title; ?></h2>
<div class="push">
    <ol class="breadcrumb">
        <li><i class='fa fa-home'></i> <a href="javascript:void(0)">Home</a></li>
        <li><?php echo anchor(base_url('imut/klinik/'), $title); ?></li>
        <!-- <li class="active">Data</li> -->
    </ol>
</div>

<?php
if ($this->session->userdata('level') == 1) {
    $param = "";
} else {
    $param = array('prodi_id' => $this->session->userdata('keterangan'));
}
?>
<div class="col-sm-5">

    <table class="table table-bordered">
        <tr>
            <td align="center">Bulan</td>
            <td width="350">
                <?php
                echo form_open(base_url() . 'imut/klinik');
                echo inputan('text', 'tglP', 'col-sm-12', 'Pilih Bulan ..', 0, '', array('id' => 'datepicker')); ?>
            </td>
            <td align="center">
                <input type="submit" name="submit" value="Cari" class="btn btn-primary  btn-sm">

            </td>
        </tr>
    </table>
</div>

<div class="col-sm-12" style="overflow-x:auto; overflow-y:auto; font-size: '1';">
    <table id="example-datatables" class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th width="5">No</th>
                <th width="120">Indikator</th>
                <?php
                for ($p = 1; $p <= $lastDate; $p++) { //tgl di atas

                    // print_r(count($record));
                    ?>

                    <th><?= $p ?></th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <?php
            $i = 1;
            foreach ($record as $r) {
                ?>

                <tr>
                    <!-- no di samping kiri -->
                    <td width="5"><?= $i ?></td>

                    <td><a href="<?php echo base_url() . 'imut/tampilDetail/' . $r[$i]['id'] . '/klinik/hospital_survey_indicator'; ?>">
                            <?= ($r[$i]['nama']) ?></a></td>

                    <?php for ($p = 1; $p <= $lastDate; $p++) {

                            if ($r[$p]['nilai'] == null) { ?>
                            <td>
                                <a href="<?php echo base_url() . 'imut/inputMutuNol/' . $r[$p]['id'] . '/' . $r[$p]['tgl'] . '/klinik'; ?>">
                                    <?php echo 0; ?>
                                </a>
                            </td>
                        <?php } else { ?>
                            <td>
                                <a href="<?php echo base_url() . 'imut/inputMutu/' . $r[$p]['id'] . '/' . $r[$p]['tgl'] . '/klinik'; ?>">
                                    <?php echo ($r[$p]['nilai']); ?>
                                </a>
                            </td>
                        <?php } ?>

                        <!-- <td>
                                    <a href="<?php echo base_url() . 'imut/inputMutu/' . $r[$p]['id'] . '/' . $r[$p]['tgl'] . '/klinik'; ?>">
                                    <?php
                                            if ($r[$p]['nilai'] == null) {
                                                echo 0;
                                            } else {
                                                echo ($r[$p]['nilai']);
                                            } ?>
                                    </a>
                                </td> -->

                    <?php } ?>
                </tr>
            <?php $i++;
            } ?>
        </tbody>
    </table>
</div>