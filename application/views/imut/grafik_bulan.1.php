  <div id="frame">
  <div id="frame_title">
    <h3 align="left">Grafik Kunjungan PASIEN</h3>
  </div>

   <div class="row data-container" >
    <div class="col-md-2 panel small-panel col-md-offset-5">
      <div class="row">

      <form class="form-vertical" method="GET" action="">
          <fieldset class="fieldset ">
            
              
              <label class="control-label small">Tahun</label>
              <select name="tahun" class="form-control input-sm">
                
                <?php 
	                $year=date('Y');
	                for($i=0;$i<=5;$i++){
	            	    echo '<option value="'.$year.'">'.$year--.'</option>';	
	                }
                
                ?>
              </select>
              
                <div class="form-group">
              <label ></label>
              <button type="submit" class="btn btn-default btn-block" name="submit" value="submit">Cari</button>

            </div>         
        </fieldset>
      </form>
    </div>
  </div>
</div>

<div class="panel-body">

	<canvas id="grafik_rj" width="1000" height="200"></canvas>
	<center><strong>Total Kunjungan :<?php echo $total_rj;?></strong></center>

	<canvas id="grafik_ri" width="1000" height="200"></canvas>
	<center><strong>Total Kunjungan :<?php echo $total_ri;?></strong></center>
</div>
</div>
 <script type="text/javascript" src="<?php echo base_url().'assets/js/Chart.bundle.min.js'?>"></script>
    <script>
 		var ctx = document.getElementById("grafik_rj");
 		var ctx2 = document.getElementById("grafik_ri");
 		var myChart = new Chart(ctx, {
			    type: 'bar',
			     data: {
			        labels: <?php echo $bulan;?>,
			        datasets: [{
			            label: 'Data Kunjungan RJ tahun <?php echo $tahun ;?>',
			            data: <?php echo $rj;?>,
			            backgroundColor:<?php echo $warna;?>,
			            borderColor:<?php echo $border;?> ,
			            borderWidth: 1
			        }]
			    },
					    options: {
			        scales: {
			            yAxes: [{
			                ticks: {
			                    beginAtZero:true
			                }
			            }]
			        }
			    }
			});
 		
 		var myChart = new Chart(ctx2, {
			    type: 'bar',
			    data: {
			        labels: <?php echo $bulan;?>,
			        datasets: [{
			            label: 'Data Kunjungan RI tahun <?php echo $tahun ;?>',
			            data: <?php echo $ri;?>,
			            backgroundColor:<?php echo $warna;?>,
			            borderColor:<?php echo $border;?> ,
			            borderWidth: 1
			        }]
			    },
			    options: {
			        scales: {
			            yAxes: [{
			                ticks: {
			                    beginAtZero:true
			                }
			            }]
			        }
			    }
			});
 		
         
    </script>