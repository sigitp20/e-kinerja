<h2 style="font-weight: normal;"><?php echo $title; ?></h2>
<div class="push">
    <ol class="breadcrumb">
        <li><i class='fa fa-home'></i> <a href="javascript:void(0)">Home</a></li>
        <li><?php echo anchor($this->uri->segment(1), $title); ?></li>
        <li class="active">Entry Record</li>
    </ol>
</div>
<?php
echo anchor($this->uri->segment(1) . '/post', 'Tambah Data', array('class' => 'btn btn-danger   btn-sm'))
?>
<table id="example-datatables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th></th>
            <th>No</th>
            <th>Jenis Kegiatan</th>
            <th width="80">Nama Kegiatan </th>
            <th>Standart Akreditasi</th>
            <th>Kode KARS</th>
            <th>Laporan</th>
            <th>Download</th>
            <th>Tanggal Mulai</th>
            <th>Tanggal Selesai</th>
        </tr>
    </thead>
    <tbody>

        <?php
        $i = 1;
        foreach ($record as $r) {
        ?>

            <tr>
                <td width="80" class="text-center">
                    <div class="btn-group">
                        <a href="<?php echo base_url() . 'kegiatan/edit/' . $r->event_id; ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-success"><i class="fa fa-pencil"></i></a>
                        <a href="<?php echo base_url('kegiatan/delete/' . $r->event_id); ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a>
                    </div>
                </td>
                <td><?php echo $i; ?></td>
                <!-- <td><?php echo $r->event_id; ?></a></td> -->

                <td>
                    <?php echo "Survei Akreditasi"; ?></a></td>


                <td><a href="<?=base_url().'pokja/tampil/'. $r->event_version;?>"> <?php echo $r->event_title; ?></a></td>

                <?php if ($r->event_version == "2012") {
                    $event_version_s = "Versi 2012";
                } else {
                    $event_version_s = "SNARS Edisi 1";
                }
                ?>
                <td><?php echo $event_version_s; ?></td>
                <!-- //tgl_indo($r->last_login) -->
                <td><?php echo $r->event_id_kars; ?></td>
                <td><?php echo "Belum"; ?></td>
                <td><?php echo "Belum"; ?></td>
                <td><?php echo $r->event_start_date; ?></td>
                <td><?php echo $r->event_end_date; ?></td>
                <!-- <td><?php echo users_keterangan($r->level, $r->keterangan) ?></td> -->
            </tr>
        <?php $i++;
        } ?>


    </tbody>
</table>
<!-- END Datatables -->