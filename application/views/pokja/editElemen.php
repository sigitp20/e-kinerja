<h2 style="font-weight: normal;"><?php echo $title; ?></h2>
<div class="push">
  <ol class="breadcrumb">
    <li><i class='fa fa-home'></i> <a href="javascript:void(0)">Home</a></li>
    <li><?php echo anchor(base_url().'kegiatan/', "Daftar Kegiatan"); ?></li>
        <li><?php echo "Daftar Bab Standar Akreditasi"; ?></li>
        <li><?php echo "Daftar Sub Bab Standar Akreditasi"; ?></li>
        <li><?php echo anchor(base_url('pokja/tampilElemen/' . $r['transaction_standard_id']), "Edit Elemen"); ?></li>
    <li><?php echo anchor(base_url('pokja/editElemen/' . $r['transaction_instrument_id']), $title); ?></li>
    <!-- <li class="active">Entry Record</li> -->
  </ol>
</div>
<script src="<?php echo base_url(); ?>assets/js/1.8.2.min.js"></script>
<script>
  $(document).ready(function() {
    $("#jurusan").hide();
  });
</script>
<script>
  $(document).ready(function() {
    $("#level").change(function() {
      var level = $("#level").val();
      if (level == 2) {
        $("#jurusan").show();
      } else {
        $("#jurusan").hide();
      }
    });
  });
</script>
<?php
echo form_open($this->uri->segment(1) . '/editElemen');
echo "<input type='hidden' name='id' value='$r[transaction_instrument_id]'>";
$level = array(1 => 'Admin', 2 => 'Pihak Jurusan', 3 => 'Dosen');
$class      = "class='form-control' id='level'";
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Edit Record</h3>
  </div>
  <div class="panel-body">
    <table class="table table-bordered">

      <tr>
        <td width="150">Elemen Penilaian</td>
        <td>
          <?php echo inputan('text', 'readonly', 'col-sm-12', 'Nama Bab Standar Akreditasi ..', 0, $r['instrument_name'], ''); ?>
        </td>
      </tr>
      <tr>

      <tr>
        <td width="150">Fakta Dan Analisis</td>
        <td>
          <?php echo inputan('text', 'transaction_surveyor_fact_and_analysis', 'col-sm-12', 'Fakta Dan Analisis ..', 0, $r['transaction_surveyor_fact_and_analysis'], ''); ?>
        </td>
      </tr>
      <tr>

      <tr>
        <td width="150">Rekomendasi</td>
        <td>
          <?php echo inputan('text', 'transaction_surveyor_recommendation', 'col-sm-12', 'Rekomendasi ..', 0, $r['transaction_surveyor_recommendation'], ''); ?>
        </td>
      </tr>
      <tr>

      <tr>
        <td width="130">Nilai</td>
        <td>
          <div class="col-sm-2">
            <?php
            $transaction_hospital_score = array('0' => '0', '5' => '5', '10' => '10', 'TDD' => 'TDD');
            echo form_dropdown('transaction_hospital_score', $transaction_hospital_score, $r['transaction_hospital_score'], "class='form-control'"); ?>
          </div>
        </td>

        <!-- <td>
          <!-- <?php echo inputan('text', 'transaction_hospital_score', 'col-sm-4', 'Nilai ..', 1, $r['transaction_hospital_score'], ''); ?> -->
        <?php echo inputan('hidden', 'transaction_standard_id', 'col-sm-4', 'Nama Bab Standar Akreditasi ..', 0, $r['transaction_standard_id'], ''); ?>
        <!-- </td> -->

      </tr>

      <tr>
        <td></td>
        <td colspan="2">
          <input type="submit" name="submit" value="simpan" class="btn btn-danger  btn-sm">
          <?php echo anchor(base_url('pokja/tampilElemen/' . $r['transaction_standard_id']), 'kembali', array('class' => 'btn btn-danger btn-sm')); ?>
        </td>
      </tr>

    </table>
  </div>
</div>
</form>