<h2 style="font-weight: normal;"><?php echo $title; ?></h2>
<div class="push">
    <ol class="breadcrumb">
        <li><i class='fa fa-home'></i> <a href="javascript:void(0)">Home</a></li>
        <li><?php echo anchor(base_url().'kegiatan/', "Daftar Kegiatan"); ?></li>
        <li><?php echo "Daftar Bab Standar Akreditasi"; ?></li>
        <li><?php echo "Daftar Sub Bab Standar Akreditasi"; ?></li>
        <li><?php echo anchor(base_url('pokja/tampilElemen/'.$id), $title); ?></li>
        <!-- <li class="active">Entry Record</li> -->
    </ol>
</div>
<?php
// echo anchor($this->uri->segment(1) . '/post', 'Tambah Data', array('class' => 'btn btn-danger   btn-sm'))
?>
<table id="example-datatables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>No</th>
            <th>Kode Sub Bab Standar Akreditasi</th>
            <th>Elemen Penilaian</th>
            <th>Fakta Dan Analisis</th>
            <th>Rekomendasi</th>
            <th >Dokumen Pendukung</th>
            <th >Skor</th>
            <th >Aksi</th>
            <!-- <th>Jumlah Elemen Penilaian</th> -->
        </tr>
    </thead>
    <tbody>

        <?php
        $i = 1;
        foreach ($record as $r) {
        ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $r->standard_code; ?></a></td>
                <td width="550" class="text-center"><?php echo $r->instrument_name; ?></a></td>  
                <td><?php echo $r->transaction_surveyor_fact_and_analysis; ?></a></td> 
                <td><?php echo $r->transaction_surveyor_recommendation; ?></a></td> 
                <td><a href="<?php echo base_url() . 'pokja/tampilUpload/' . $r->transaction_instrument_id; ?>"><?php echo "Dokumen (".$r-> transaction_document_amount.")"; ?></a></td> 
                <td><?php echo $r->transaction_hospital_score; ?></a></td> 
                <td width="80" class="text-center">
                    <div class="btn-group">
                        <a href="<?php echo base_url() . 'pokja/editElemen/' . $r->transaction_instrument_id; ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-success"> <i class="fa fa-pencil"></i> Edit </a> 
                        <!-- <a href="<?php echo base_url('kegiatan/delete/' . $r->event_id); ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a> -->
                    </div>
                </td>
            </tr>
        <?php $i++;
        } ?>

    </tbody>
</table>
<!-- END Datatables -->