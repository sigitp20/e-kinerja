<h2 style="font-weight: normal;"><?php echo $title; ?></h2>
<div class="push">
    <ol class="breadcrumb">
        <li><i class='fa fa-home'></i> <a href="javascript:void(0)">Home</a></li>
        <li><?php echo anchor(base_url().'kegiatan/', "Daftar Kegiatan"); ?></li>
        <li><?php echo anchor(base_url().'pokja/tampil/'.$record[2]->standard_version,"Daftar Bab Standar Akreditasi"); ?></li>
        <li><?php echo anchor(base_url().'pokja/tampilSub/'.$id, $title); ?></li>
    </ol>
</div>
<?php
// echo anchor($this->uri->segment(1) . '/post', 'Tambah Data', array('class' => 'btn btn-danger   btn-sm'))
?>
<table id="example-datatables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>No</th>
            <th>Kode</th>
            <th >Nama Sub Bab Standar Akreditasi</th>
            <th>Jumlah Elemen Penilaian</th>
        </tr>
    </thead>
    <tbody>

        <?php
        $i = 1;
        foreach ($record as $r) {
        ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><a href="<?php echo base_url() . 'pokja/tampilElemen/' . $r->standard_id; ?>"><?php echo $r->standard_code; ?></a></td>
                <td width="550" class="text-center"><a href="<?php echo base_url() . 'pokja/tampilElemen/' . $r->standard_id; ?>"><?php echo $r->standard_name; ?></a></td>  
                <td><a href="<?php echo base_url() . 'pokja/tampilElemen/' . $r->standard_id; ?>"><?php echo $r->jumlah; ?></a></td> 
                <!-- <td width="80" class="text-center">
                    <div class="btn-group">
                        <a href="<?php echo base_url() . 'pokja/edit/' . $r->chapter_id; ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-success">Verifikasi </a> 
                        <!-- //<i class="fa fa-pencil"></i> -->
                        <!-- <a href="<?php echo base_url('kegiatan/delete/' . $r->event_id); ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a> 
                    </div>
                </td> -->
            </tr>
        <?php $i++;
        } ?>


    </tbody>
</table>
<!-- END Datatables -->