<h2 style="font-weight: normal;"><?php echo $title; ?></h2>
<div class="push">
  <ol class="breadcrumb">
    <li><i class='fa fa-home'></i> <a href="javascript:void(0)">Home</a></li>
    <li><?php echo anchor($this->uri->segment(1), $title); ?></li>
    <li class="active">Entry Record</li>
  </ol>
</div>
<script src="<?php echo base_url(); ?>assets/js/1.8.2.min.js"></script>
<script>
  $(document).ready(function() {
    $("#jurusan").hide();
  });
</script>
<script>
  $(document).ready(function() {
    $("#level").change(function() {
      var level = $("#level").val();
      if (level == 2) {
        $("#jurusan").show();
      } else {
        $("#jurusan").hide();
      }
    });
  });
</script>
<?php
echo form_open($this->uri->segment(1) . '/edit');
echo "<input type='hidden' name='id' value='$r[chapter_id]'>";
$level = array(1 => 'Admin', 2 => 'Pihak Jurusan', 3 => 'Dosen');
$class      = "class='form-control' id='level'";
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Edit Record</h3>
  </div>
  <div class="panel-body">
    <table class="table table-bordered">

      <tr>
        <td width="150">Nama Bab Standar Akreditasi</td>
        <td>
          <?php echo inputan('text', 'chapter_name', 'col-sm-4', 'Nama Bab Standar Akreditasi ..', 1, $r['chapter_name'], ''); ?>
        </td>
      </tr>
      <tr>

      <tr>
        <td width="130">Nilai</td>
        <td>
          <?php echo inputan('text', 'trans_hospital_score', 'col-sm-4', 'Nama Bab Standar Akreditasi ..', 1, $r['trans_hospital_score'], ''); ?>
          <?php echo inputan('hidden', 'event_version', 'col-sm-4', 'Nama Bab Standar Akreditasi ..', 0, $r['chapter_version'], ''); ?>
        </td>

      </tr>

      <tr>
        <td></td>
        <td colspan="2">
          <input type="submit" name="submit" value="simpan" class="btn btn-danger  btn-sm">
          <?php echo anchor(base_url('pokja/tampil/'.$r['chapter_version']), 'kembali', array('class' => 'btn btn-danger btn-sm')); ?>
        </td>
      </tr>

    </table>
  </div>
</div>
</form>