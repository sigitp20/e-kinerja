<h2 style="font-weight: normal;"><?php echo $title; ?></h2>
<div class="push">
  <ol class="breadcrumb">
    <li><i class='fa fa-home'></i> <a href="javascript:void(0)">Home</a></li>
    <li><?php echo anchor($this->uri->segment(1), $title); ?></li>
    <li class="active">Entry Record</li>
  </ol>
</div>
<script src="<?php echo base_url(); ?>assets/js/1.8.2.min.js"></script>
<script>
  $(document).ready(function() {
    $("#jurusan").hide();
  });
</script>
<script>
  $(document).ready(function() {
    $("#level").change(function() {
      var level = $("#level").val();
      if (level == 2) {
        $("#jurusan").show();
      } else {
        $("#jurusan").hide();
      }
    });
  });
</script>
<?php
echo form_open_multipart($this->uri->segment(1) . '/post');
$level = array(1 => 'Admin', 2 => 'Pihak Jurusan', 3 => 'Dosen');
$class      = "class='form-control' id='level'";
?>
<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Entry Record</h3>
  </div>
  <div class="panel-body">
    <table class="table table-bordered">

      <tr>
        <td width="150">Nama Kegiatan</td>
        <td>
          <?php echo inputan('text', 'event_title', 'col-sm-4', 'Nama Kegiatan ..', 1, '', ''); ?>
        </td>
      </tr>
      <tr>

      <tr>
        <td width="130">Standar Akreditasi</td>
        <td>
          <div class="col-sm-2">
            <?php
            $event_version = array('2012' => 'Versi 2012', '2018' => 'SNARS Edisi 1');
            echo form_dropdown('event_version', $event_version, '', "class='form-control'"); ?>

            <!-- <?php echo buatcombo('event_version', 'event_version', 'col-sm-6', 'keterangan', 'event_version_id', '', array('id' => 'event_version')); ?> -->
          </div>
        </td>

      </tr>

      <tr>
        <td width="150">Kode KARS</td>
        <td>
          <?php echo inputan('text', 'event_id_kars', 'col-sm-3', 'Kode KARS ..', 0, '', ''); ?>
        </td>
      </tr>
      <tr>
        <td width="150">Tanggal Mulai</td>
        <td>
          <?php echo inputan('text', 'event_start_date', 'col-sm-4', 'Tanggal Mulai ..', 0, '', array('id' => 'datepicker')); ?>
        </td>
      </tr>
      <tr>
        <td width="150">Tanggal Selesai</td>
        <td>
          <?php echo inputan('text', 'event_end_date', 'col-sm-4', 'Tanggal Selesai ..', 0, '', array('id' => 'datepicker2')); ?>
        </td>
      </tr>
      <tr>
        <td></td>
        <td colspan="2">
          <input type="submit" name="submit" value="simpan" class="btn btn-danger  btn-sm">
          <?php echo anchor($this->uri->segment(1), 'kembali', array('class' => 'btn btn-danger btn-sm')); ?>
        </td>
      </tr>

    </table>
  </div>
</div>
</form>