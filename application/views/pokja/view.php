<h2 style="font-weight: normal;"><?php echo $title; ?></h2>
<div class="push">
    <ol class="breadcrumb">
        <li><i class='fa fa-home'></i> <a href="javascript:void(0)">Home</a></li>
        <li><?php echo anchor(base_url().'kegiatan/', "Daftar Kegiatan"); ?></li>
        <li><?php echo anchor(base_url().'pokja/tampil/'.$id, $title); ?></li>
        <!-- <li class="active">Entry Record</li> -->
    </ol>
</div>
<?php
// echo anchor($this->uri->segment(1) . '/post', 'Tambah Data', array('class' => 'btn btn-danger   btn-sm'))
?>
<table id="example-datatables" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <!-- <th></th> -->
            <th>No</th>
            <th>Nama Bab Standar Akreditasi</th>
            <th width="80">Nilai </th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>

        <?php
        $i = 1;
        foreach ($record as $r) {
        ?>

            <tr>
                <td><?php echo $i; ?></td>
                <td><a href="<?php echo base_url() . 'pokja/tampilSub/'.$r->chapter_id; ?>"><?php echo $r->chapter_name; ?></a></td>
                <td><?php echo $r->trans_hospital_score; ?></a></td>  
                <td width="80" class="text-center">
                    <div class="btn-group">
                        <a href="<?php echo base_url() . 'pokja/edit/' . $r->chapter_id; ?>" data-toggle="tooltip" title="Edit" class="btn btn-xs btn-success">Verifikasi </a> 
                        <!-- //<i class="fa fa-pencil"></i> -->
                        <!-- <a href="<?php echo base_url('kegiatan/delete/' . $r->event_id); ?>" data-toggle="tooltip" title="Delete" class="btn btn-xs btn-danger"><i class="fa fa-times"></i></a> -->
                    </div>
                </td>
            </tr>
        <?php $i++;
        } ?>


    </tbody>
</table>
<!-- END Datatables -->